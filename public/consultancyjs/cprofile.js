var consultancyapp=angular.module('consultancyapp',[]);

consultancyapp.controller('CProfileController',function($scope,$http,$compile){
    $scope.totalReferals='';
    $scope.completedReferals='';
    $scope.reviewedReferals='';
    $scope.pendingReferals='';
    $scope.referalPartners=[];
    $scope.user=[];
    $scope.changeCommision=false;
    $scope.newCommission='';
    $scope.searchKey='';
    $scope.competitors=[];
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
		}); 
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#errorBox'
            }
        });
          
        // Open
        modal.open();
	}
	var showSuccess=function(message)
	{
		$('#successMessage').empty();
		$('#successMessage').html(message);
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#successModal'
            }
        });
          
        // Open
        modal.open();
    }
    $(document).on( 'click', '#pagination a', function(e){

        e.preventDefault();
        var url = $(this).attr('href');
        var filter = $scope.status_filter ? '&filter='+$scope.status_filter : '';
        $.get(url+filter,function(data){
            $scope.referals=data.referals.data;
            $scope.$apply();
            
            var myEl = angular.element(document.querySelector('#pagination'));
            var $anchor = angular.element(data.pagination);
            myEl.html($compile($anchor)($scope));
            
        });
    });
    $scope.filterByStatus=function(){
        if($scope.status_filter == null) return;
        
        $('#filterByStatusLoading').removeAttr('hidden');
        $http({ method: "GET", url: "/consultancy/cprofile", headers:{'X-Requested-With':'XMLHttpRequest'},
          params: {filter:$scope.status_filter}} ).then(function success(response) {
					
                $scope.referals=response.data.referals.data;
          
                
                var myEl = angular.element(document.querySelector('#pagination'));
                var $anchor = angular.element(response.data.pagination);
                myEl.html($compile($anchor)($scope));
                $('#filterByStatusLoading').attr('hidden','true');
					
		},function error(response){
				
			$('#saveCommission').removeAttr('disabled');
            $('#saveCommission i').attr('class','fa fa-check-circle g-mr-5');
            $scope.changeCommision=false;
            
            showErrors(response);
            $('#filterByStatusLoading').attr('hidden','true');
			
            
        });
    }
    $scope.initPagination=function(html){
        $('#pagination').html(html)
    }
    $scope.dashboardInit=function(){
        $('#dashboardMainLoading').removeAttr('hidden');
		$('#dashboardMainBeforeLoading').attr('hidden','true');
		$http({ method: "GET", url: "/consultancy/dashboard"}).then(function success(response) {
            $scope.referalPartners=response.data.referalPartners;
            $scope.user=response.data.user;
            $scope.totalReferals=response.data.totalReferals;
            var data=[response.data.submittedReferals,response.data.seenReferals,response.data.inProgressReferals,response.data.successReferals,response.data.paidReferals,response.data.rejectedReferals];
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ["Submitted", "Seen", "In-Progress", "Success", "Paid", "Rejected"],
                    datasets: [{
                        label: '# of Votes',
                        data: data,
                        backgroundColor: [
                            '#007bff',
                            '#18ba9b',
                            '#ffc107',
                            '#28a745',
                            '#00f337',
                            '#dc3545'
                        ]
                    }]
                },
            });	
			$('#dashboardMainLoading').attr('hidden','true');
			$('#dashboardMainBeforeLoading').removeAttr('hidden');
			
		},function error(response){
			$('#dashboardMainLoading').attr('hidden','true');
			$('#dashboardMainBeforeLoading').removeAttr('hidden');
			showErrors(response);
		});
    }
    
    $scope.saveCommission=function(){
		$('#saveCommission').attr('disabled','disabled');
		$('#saveCommission i').attr('class','fa fa-spin fa-spinner g-mr-5');
		$http({ method: "POST", url: "/consultancy/dashboard/savecommission",
          data: {newCommission:$scope.newCommission}}, ).then(function success(response) {
					
					
					$('#saveCommission').removeAttr('disabled');
                    $('#saveCommission i').attr('class','fa fa-check-circle g-mr-5');
                    $scope.user.commission_rate=response.data;

                    $scope.changeCommission=false;
                    showSuccess('Commission Changed Successfully');
                    
					
		},function error(response){
				
			$('#saveCommission').removeAttr('disabled');
            $('#saveCommission i').attr('class','fa fa-check-circle g-mr-5');
            $scope.changeCommision=false;
            
			showErrors(response);
			
            
        });
	}
    
    $scope.toggleHideCommissionRate=function(){
        $('#hideCommissionRateLoading').removeAttr('hidden');
        $('#hideCommissionRateBeforeLoading').attr('hidden','true');
        
		$http({ method: "POST", url: "/consultancy/dashboard/toggleshowrate",
          data: {newCommission:$scope.newCommission}}, ).then(function success(response) {
					
					
                    $('#hideCommissionRateLoading').attr('hidden','true');
                    $('#hideCommissionRateBeforeLoading').removeAttr('hidden');
                    
                    $scope.user.show_rate=response.data;

                    
					
		},function error(response){
				
            $('#hideCommissionRateLoading').attr('hidden','true');
            $('#hideCommissionRateBeforeLoading').removeAttr('hidden');
            
            
			showErrors(response);
			
            
        });
    }
    $scope.search=function(){
        $('#searchReferal').attr('disabled','disabled');
		$('#searchReferal i').attr('class','fa fa-spin fa-spinner g-mr-5');
		$http({ method: "GET", url: "/consultancy/cprofile",headers:{'X-Requested-With':'XMLHttpRequest'},
          params: {searchKey:$scope.searchKey}}, ).then(function success(response) {
					
					
					$('#searchReferal').removeAttr('disabled');
                    $('#searchReferal i').attr('class','fa fa-arrow-right g-mr-5');
                    $scope.referals=response.data.referals.data;
          
                
                    var myEl = angular.element(document.querySelector('#pagination'));
                    var $anchor = angular.element(response.data.pagination);
                    myEl.html($compile($anchor)($scope));
                   
                    
					
		},function error(response){
				
			$('#searchReferal').removeAttr('disabled');
            $('#searchReferal i').attr('class','fa fa-arrow-right g-mr-5');
            
            
			showErrors(response);
			
            
        });
    }
    $scope.myReferalsInit=function(referals){
        $scope.referals=referals;
    }
    $scope.competitorsInit=function(response){
        $('#competitorsMainLoading').removeAttr('hidden');
		$('#competitorsMainBeforeLoading').attr('hidden','true');
		    $scope.competitors=response;
            for(var i=0;i<$scope.competitors.length;i++)
            {
                var data=[$scope.competitors[i].submittedReferals,$scope.competitors[i].seenReferals,$scope.competitors[i].inProgressReferals,$scope.competitors[i].successReferals,$scope.competitors[i].paidReferals,$scope.competitors[i].rejectedReferals];
                var ctx = document.getElementById("competitor"+$scope.competitors[i].id);
                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["Submitted", "Seen", "In-Progress", "Success", "Paid", "Rejected"],
                        datasets: [{
                            label: '# of Votes',
                            data: data,
                            backgroundColor: [
                                '#007bff',
                                '#18ba9b',
                                '#ffc107',
                                '#28a745',
                                '#00f337',
                                '#dc3545'
                            ]
                        }]
                    },
                });
            }	
			$('#competitorsMainLoading').attr('hidden','true');
			$('#competitorsMainBeforeLoading').removeAttr('hidden');
		
    }

    
    $scope.getStatusColor=function(status){
		if(status=='unfinished')
			return 'bg-warning';
		else if(status=='success')
			return 'bg-success';
		else if(status=='submitted')
            return 'bg-primary';
        else if(status=='rejected')
            return 'bg-danger';
        else if(status=='in-progress')
            return 'bg-warning';
        else if(status=='seen')
            return 'g-bg-teal';
        else if(status=='paid')
            return 'bg-success';
    }

    $scope.myReferals=function(){
        $('#myReferalsTabLink').trigger('click');
    }
});

consultancyapp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);