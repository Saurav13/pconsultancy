var profileapp=angular.module('profileapp',[]);
profileapp.controller('ProfileController',function($scope,$http,$compile){
    
    $scope.user = {};
    $scope.error = {'general':null,'oldPassword':null,'password':null};
    $scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};
    
    $scope.editProfileInit=function(user)
	{
		$scope.user=user;
    }
    
    $scope.changePassword=function(){
        $('#submitchangebtn').attr('disabled','disabled');
        $('#passwordChangeSuccess').attr('hidden','hidden');
        $scope.error = {'general':null,'oldPassword':null,'password':null};
        
        $http({
            method : "POST",
            url : "/consultancy/changepassword",
            data: {'oldPassword':$scope.data.oldPassword,'password':$scope.data.password,'password_confirmation':$scope.data.password_confirmation}
        }).then(function mySuccess(response) {
            $scope.resetForm();
            $scope.hasPassword = true;
            $('#submitchangebtn').removeAttr('disabled');
            $('#passwordChangeSuccess').removeAttr('hidden');
            setTimeout(function(){
                $('#passwordChangeSuccess').attr('hidden','hidden');
            }, 5000);
        }, function myError(xhr) {
            if(xhr.status == 422){
                $.each( xhr.data.errors, function( key, value ) {
                    $scope.error[key] = value[0];
                });
            }
            else{
                $scope.error.general = 'Something Went Wrong. Please try again.';
            }
            $('#submitchangebtn').removeAttr('disabled');
        });
    }

    $scope.resetForm = function(){
        $scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};
        $scope.error = {'general':null,'oldPassword':null,'password':null};
    }
});