var referalApp=angular.module('referalapp',[]);
referalApp.controller('ReferalController',function($scope,$http){
   $scope.referal={};
   $scope.referal_image='';
   var showErrors=function(response){
    $('#errorMessage').empty();
    $('#errorDetails').empty();
    $('#errorMessage').html(response.data.message)
    $.each(response.data.errors, function(index, value) {
        $('#errorDetails').append(value+'<br>');
    
    }); 
    var modal = new Custombox.modal({
            content: {
            effect: 'fadein',
            target: '#errorBox'
            }
        });
        
        // Open
        modal.open();
    }
    var showSuccess=function(message)
    {
        $('#successMessage').empty();
        $('#successMessage').html(message);
        var modal = new Custombox.modal({
            content: {
            effect: 'fadein',
            target: '#successModal'
            }
        });
        
        // Open
        modal.open();
    }
   $scope.referalInit=function(referal){
       $scope.referal=referal;
   } 
   $scope.toggleStatusConfirm=function(status){
        var modal = new Custombox.modal({
            content: {
            effect: 'fadein',
            target: '#'+status+'Referal'
            }
        });
        
        // Open
        modal.open();
   }
   $scope.toggleReferalStatus=function(status){
        $('#'+status+'ReferalBtn').attr('disabled','disabled');
        $('#'+status+'ReferalBtn i').attr('class','fa fa-spin fa-spinner g-mr-5');
        $http({ method: "POST", url: "/consultancy/referal/togglestatus",
        data: {status:status, id:$scope.referal.id}}, ).then(function success(response) {
                    
                    
                    $('#'+status+'ReferalBtn').removeAttr('disabled');
                    $('#'+status+'ReferalBtn i').attr('class','fa fa-check-circle g-mr-5');
                  
                    $scope.referal.status=response.data;
                    showSuccess('Marked Successfully');
                    Custombox.modal.close();
                    
                    
        },function error(response){
                
            $('#'+status+'ReferalBtn').removeAttr('disabled');
            $('#'+status+'ReferalBtn i').attr('class','fa fa-check-circle g-mr-5');
          
            
            showErrors(response);
            
            
        });
   }
   $scope.payCommission=function(){
        var modal = new Custombox.modal({
            content: {
            effect: 'fadein',
            target: '#payCommission'
            }
        });
        
        // Open
        modal.open();
    }
    $scope.submitReceipt=function(){
        $('#referalSubmitBtn').attr('disabled','disabled');
        $('#referalSubmitBtn i').attr('class','fa fa-spin fa-spinner g-mr-5');
		var formData= new FormData();
        formData.append('receipt',$scope.receipt_image);
        formData.append('id',$scope.referal.id);
		$http.post('/consultancy/referal/submitreceipt', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
                    $('#referalSubmitBtn').removeAttr('disabled');
                    $('#referalSubmitBtn i').attr('class','fa fa-check-circle g-mr-5');
                    showSuccess('Submitted Successfully. Please Wait for verification.');
                    $scope.referal.paid_status=response.data;
                    Custombox.modal.close();
		
		},function error(response){
            $('#referalSubmitBtn').removeAttr('disabled');
            $('#referalSubmitBtn i').attr('class','fa fa-check-circle g-mr-5');
          
			showErrors(response);
			
            
        });
    }
   $scope.getStatusColor=function(status){
        if(status=='unfinished')
            return 'bg-warning';
        else if(status=='success')
            return 'bg-success';
        else if(status=='submitted')
            return 'bg-primary';
        else if(status=='rejected')
            return 'bg-danger';
        else if(status=='in-progress')
            return 'bg-warning';
        else if(status=='seen')
            return 'g-bg-teal';
        else if(status=='paid')
            return 'bg-success';
    }

});

referalApp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);