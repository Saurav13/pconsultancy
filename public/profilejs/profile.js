var profileapp=angular.module('profileapp',[]);
profileapp.controller('ProfileController',function($scope,$http,$compile){
	$scope.referalApplication={name:"",email:"",phone:"",details:"",photo:"",consultancy:"",selectedCategory:""};
	$scope.model={'name':false,'about':false,'gender':false,'phone':false,'profession':false,'bank_name':false,'bank_account_number':false};
    $scope.user=[];
	$scope.selectedReferal={};
	$scope.referals=[];
	$scope.selectedCategory="";
	$scope.selectedPartner="";
	$scope.partners=[];
	$scope.branches=[];
	$scope.np={np:3};

	$scope.error = {'general':null,'oldPassword':null,'password':null};
    $scope.hasPassword;
	$scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};
	$scope.profilephoto='';
	
	var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
		}); 
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#errorBox'
            }
        });
          
        // Open
        modal.open();
	}
	
	var showSuccess=function(message)
	{
		$('#successMessage').empty();
		$('#successMessage').html(message);
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#successModal'
            }
        });
          
        // Open
        modal.open();
	}
	$(document).on( 'click', '#referalPagination a', function(e){

        e.preventDefault();
		var url = $(this).attr('href');
		$('#myReferalsLoading').removeAttr('hidden');
		$('#myReferalsBeforeLoading').attr('hidden','true');
        $.get(url+'&filter='+$scope.status_filter,function(data){

            $scope.referals=data.referals.data;
            $scope.$apply();
            
            var myEl = angular.element(document.querySelector('#referalPagination'));
            var $anchor = angular.element(data.pagination);
			myEl.html($compile($anchor)($scope));
			$('#myReferalsLoading').attr('hidden','true');
			$('#myReferalsBeforeLoading').removeAttr('hidden');
            
        });
	});
	$(document).on( 'click', '#commissionPagination a', function(e){

        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url+'&filter='+$scope.status_filter,function(data){

            $scope.paidReferals=data.referals.data;
            $scope.$apply();
            
            var myEl = angular.element(document.querySelector('#commissionPagination'));
            var $anchor = angular.element(data.pagination);
			myEl.html($compile($anchor)($scope));
			$('#myCommissionsLoading').attr('hidden','true');
			$('#myCommissionsBeforeLoading').removeAttr('hidden');
            
        });
	});
	$scope.init=function(categories){
		$scope.categories=categories;
		for(var i=0;i<$scope.categories.length;i++)
		{
			$scope.partners=$scope.partners.concat($scope.categories[i].partners);
		}

		for(var i=0;i<$scope.partners.length;i++)
		{
			$scope.branches=$scope.branches.concat($scope.partners[i].branches);
		}
		
		console.log($scope.categories);
	}
	$scope.getReferals=function(){
		$('#myReferalsLoading').removeAttr('hidden');
		$('#myReferalsBeforeLoading').attr('hidden','true');
		$http({ method: "GET", url: "/user/getreferal"}).then(function success(response) {
			$scope.referals=response.data.referals.data;	
			$scope.referalApplication={};
			
			var myEl = angular.element(document.querySelector('#referalPagination'));
            var $anchor = angular.element(response.data.pagination);
			myEl.html($compile($anchor)($scope));
			
			$('#myReferalsLoading').attr('hidden','true');
			$('#myReferalsBeforeLoading').removeAttr('hidden');
			
		},function error(response){
			$('#myReferalsLoading').attr('hidden','true');
			$('#myReferalsBeforeLoading').removeAttr('hidden');
			showErrors(response);
		});
	}
	$scope.getCommissions=function(){
		$('#myCommissionsLoading').removeAttr('hidden');
		$('#myCommissionsBeforeLoading').attr('hidden','true');
		$http({ method: "GET", url: "/user/getcommission"}).then(function success(response) {
			$scope.paidReferals=response.data.paidReferals.data;	
			$scope.referalApplication={};
			
			var myEl = angular.element(document.querySelector('#commissionPagination'));
            var $anchor = angular.element(response.data.pagination);
			myEl.html($compile($anchor)($scope));

			$('#myCommissionsLoading').attr('hidden','true');
			$('#myCommissionsBeforeLoading').removeAttr('hidden');
			
		},function error(response){
			$('#myCommissionsLoading').attr('hidden','true');
			$('#myCommissionsBeforeLoading').removeAttr('hidden');
			showErrors(response);
		});
	}
	$scope.getStatusColor=function(status){
		if(status=='unfinished')
			return 'bg-warning';
		else if(status=='success')
			return 'bg-success';
		else if(status=='submitted')
            return 'bg-primary';
        else if(status=='rejected')
            return 'bg-danger';
        else if(status=='in-progress')
            return 'bg-warning';
        else if(status=='seen')
			return 'g-bg-teal';
		else if(status=='paid')
            return 'bg-success';
	}
	$scope.referalSave=function(){
		// console.log($scope.refer);
		var formData = new FormData();
		// formData.append('nameasd');
		console.log(formData);
		formData.append('name',$scope.referalApplication.name==null?'':$scope.referalApplication.name);
		formData.append('email',$scope.referalApplication.email==null?'':$scope.referalApplication.email);
		formData.append('details',$scope.referalApplication.visa_type==null?'':$scope.referalApplication.visa_type);
		formData.append('phone',$scope.referalApplication.phone==null?'':$scope.referalApplication.phone);
		formData.append('photo',$scope.referalApplication.photo==null?'':$scope.referalApplication.photo);
		formData.append('consultancy',$scope.referalApplication.consultancy==null?'':$scope.referalApplication.consultancy);
		
		// console.log(formData);
		// for (var key of formData.entries()) {
		// 	console.log(key[0] + ', ' + key[1]);
		// }
		$('#saveReferal').attr('disabled','disabled');
		$('#saveReferal i').attr('class','fa fa-spin fa-spinner g-mr-5');
		$http.post('/user/savereferal', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
					$('#saveReferal').removeAttr('disabled');
					$('#saveReferal i').attr('class','fa fa-check-circle g-mr-5');
					$('#myReferalTabLink').trigger('click');	
					showSuccess('Saved Successfully');
		
		},function error(response){
			$('#saveReferal').removeAttr('disabled');
			$('#saveReferal i').attr('class','fa fa-check-circle g-mr-5');
			showErrors(response);
			
            
        });
    }
    $scope.referalSubmit=function(){
		var formData = new FormData();
		$('#submitReferal').attr('disabled','disabled');
		$('#submitReferal i').attr('class','fa fa-spin fa-spinner g-mr-5');
		// formData.append('nameasd');
		formData.append('name',$scope.referalApplication.name==null?'':$scope.referalApplication.name);
		formData.append('email',$scope.referalApplication.email==null?'':$scope.referalApplication.email);
		formData.append('visa_type',$scope.referalApplication.visa_type==null?'':$scope.referalApplication.visa_type);
		formData.append('phone',$scope.referalApplication.phone==null?'':$scope.referalApplication.phone);
		formData.append('photo',$scope.referalApplication.photo==null?'':$scope.referalApplication.photo);
		formData.append('consultancy',$scope.referalApplication.consultancy==null?'':$scope.referalApplication.consultancy);
		
		// console.log(formData);
		// for (var key of formData.entries()) {
		// 	console.log(key[0] + ', ' + key[1]);
		// }
		$http.post('/user/submitreferal', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
					
					$('#myReferalTabLink').trigger('click');	
					
					$('#submitReferal').removeAttr('disabled');
					$('#submitReferal i').attr('class','fa fa-check-circle g-mr-5');
					showSuccess('Successfully Refered !!!');
		},function error(response){
				
			$('#submitReferal').removeAttr('disabled');
			$('#submitReferal i').attr('class','fa fa-check-circle g-mr-5');
			showErrors(response);
            
        });
	}
	$scope.referalSaveEdit=function(){
		// console.log($scope.refer);
		var formData = new FormData();
		// formData.append('nameasd');
		console.log(formData);
		formData.append('id',$scope.selectedReferal.id);		
		formData.append('name',$scope.selectedReferal.name==null?'':$scope.selectedReferal.name);
		formData.append('email',$scope.selectedReferal.email==null?'':$scope.selectedReferal.email);
		formData.append('details',$scope.selectedReferal.visa_type==null?'':$scope.selectedReferal.visa_type);
		formData.append('phone',$scope.selectedReferal.phone==null?'':$scope.selectedReferal.phone);
		formData.append('photo',$scope.selectedReferal.photo==null?'':$scope.selectedReferal.photo);	
		formData.append('consultancy',$scope.selectedReferal.consultancy==null?'':$scope.selectedReferal.consultancy);
		
		// console.log(formData);
		// for (var key of formData.entries()) {
		// 	console.log(key[0] + ', ' + key[1]);
		// }
		$('#saveReferalEdit').attr('disabled','disabled');
		$('#saveReferalEdit i').attr('class','fa fa-spin fa-spinner g-mr-5');
		$http.post('/user/savereferaledit', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
					  for(var i=0;i<$scope.referals.length;i++)
					  {
						if($scope.referals[i].id==response.data.id)
							{
								$scope.referals[i]=response.data;
							}
					  }
					$('#saveReferalEdit').removeAttr('disabled');
					$('#saveReferalEdit i').attr('class','fa fa-check-circle g-mr-5');
					Custombox.modal.close();
					showSuccess('Saved Successfully');
					

						
		
		},function error(response){
			$('#saveReferalEdit').removeAttr('disabled');
			$('#saveReferalEdit i').attr('class','fa fa-check-circle g-mr-5');
			Custombox.modal.close();
			showErrors(response);
			
            
        });
    }
    $scope.referalSubmitEdit=function(){
		var formData = new FormData();
		$('#submitReferalEdit').attr('disabled','disabled');
		$('#submitReferalEdit i').attr('class','fa fa-spin fa-spinner g-mr-5');
		// formData.append('nameasd');
		formData.append('id',$scope.selectedReferal.id);		
		formData.append('name',$scope.selectedReferal.name==null?'':$scope.selectedReferal.name);
		formData.append('email',$scope.selectedReferal.email==null?'':$scope.selectedReferal.email);
		formData.append('details',$scope.selectedReferal.visa_type==null?'':$scope.selectedReferal.visa_type);
		formData.append('phone',$scope.selectedReferal.phone==null?'':$scope.selectedReferal.phone);
		formData.append('photo',$scope.selectedReferal.photo==null?'':$scope.selectedReferal.photo);
		formData.append('consultancy',$scope.selectedReferal.consultancy==null?'':$scope.selectedReferal.consultancy);
		
		// console.log(formData);
		// for (var key of formData.entries()) {
		// 	console.log(key[0] + ', ' + key[1]);
		// }
		$http.post('/user/submitreferaledit', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
					for(var i=0;i<$scope.referals.length;i++)
					  {
						if($scope.referals[i].id==response.data.id)
							{
								$scope.referals[i]=response.data;
							}
					  }
					
					$('#submitReferalEdit').removeAttr('disabled');
					$('#submitReferalEdit i').attr('class','fa fa-check-circle g-mr-5');
					Custombox.modal.close();
					showSuccess('Successfully refered !!!');
					
		},function error(response){
				
			$('#submitReferalEdit').removeAttr('disabled');
			$('#submitReferalEdit i').attr('class','fa fa-check-circle g-mr-5');
			Custombox.modal.close();
			showErrors(response);
			
            
        });
	}
	
	$scope.uploadProfilePicture=function(){
		$('#uploadProfilePictureLoading').removeAttr('hidden');
		var formData= new FormData();
		formData.append('photo',$scope.profilephoto);
		$http.post('/user/changeprofilepicture', formData, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}}).then(function success(response) {
					$('#profilePicture').attr('src','/profile_images/'+response.data);
				$('#uploadProfilePictureLoading').attr('hidden','true');
	
		
		},function error(response){
			showErrors(response);
			
            
        });
	}
	$scope.viewReferal=function(id){
		$scope.selectedReferal=angular.copy(_.findWhere($scope.referals,{id:id}));
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#viewReferal'
            }
        });
          
        // Open
        modal.open();
	}
	$scope.editReferal=function(id,enabled){
		if(enabled==true){
		$scope.selectedReferal=angular.copy(_.findWhere($scope.referals,{id:id}));
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#editReferal'
            }
        });
	
        // Open
		modal.open();
		}
	}
	$scope.deleteReferalConfirmation=function(id){
		$scope.selectedReferal=angular.copy(_.findWhere($scope.referals,{id:id}));
		var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#deleteReferal'
            }
		});
		modal.open();
	}
	$scope.deleteReferal=function(){
		// console.log('delete');
		$('#deleteReferalBtn').attr('disabled','disabled');
		$('#deleteReferalBtn i').attr('class','fa fa-spin fa-spinner g-mr-5');
		
		
		$http.post('/user/deletereferal',{id:$scope.selectedReferal.id}).then(function success(response) {
					
					
						for(var i=0;i<$scope.referals.length;i++)
						{
						if($scope.referals[i].id==$scope.selectedReferal.id)
							{
								$scope.referals.splice(i,1);
							}
						}
						
					$('#deleteReferalBtn').removeAttr('disabled');
					$('#deleteReferalBtn i').attr('class','fa fa-check-circle g-mr-5');
						Custombox.modal.close();
					showSuccess('Deleted Successfully');
						
		
		},function error(response){
			
			$('#deleteReferalBtn').removeAttr('disabled');
			$('#deleteReferalBtn i').attr('class','fa fa-check-circle g-mr-5');
			Custombox.modal.close();
			
		});
	}
	$scope.editProfileInit=function(user)
	{
		$scope.user=user;
	}
	$scope.edit=function(key)
    {
        $scope.model[key]=$scope.user[key];
    }

    $scope.cancel=function(key)
    {
        $scope.user[key] = $scope.model[key];
        $scope.model[key]=false;
	}
	$scope.save=function(key)
    {
        $('#save'+key).attr('disabled','disabled');
        $('#save'+key+' i').attr('class','fa fa-spinner fa-spin');
        $('#cancel'+key).attr('disabled','disabled');
        $http({
            method : "POST",
            url : "/user/saveinfo",
            data: {'key':key,'value':$scope.user[key]}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            $scope.model[key]=false;
            $('#save'+key).removeAttr('disabled');
            $('#save'+key+' i').attr('class','fa fa-check');
            $('#cancel'+key).removeAttr('disabled');
				
			showSuccess('Saved Successfully');
			
            
            // $('#renameFolder').modal('hide');
        }, function myError(response) {
            $('#save'+key).removeAttr('disabled');
            $('#save'+key+' i').attr('class','fa fa-check');
            $('#cancel'+key).removeAttr('disabled');
			showErrors(response);
			
        });
	}
	$scope.referPerson=function(){
		$('#referNowTabLink	').trigger('click');
	}
	
	$scope.changePassword=function(){
        $('#submitchangebtn').attr('disabled','disabled');
        $('#passwordChangeSuccess').attr('hidden','hidden');
        $scope.error = {'general':null,'oldPassword':null,'password':null};
        
        $http({
            method : "POST",
            url : "/user/profile/changepassword",
            data: {'oldPassword':$scope.data.oldPassword,'password':$scope.data.password,'password_confirmation':$scope.data.password_confirmation}
        }).then(function mySuccess(response) {
            $scope.resetForm();
            $scope.hasPassword = true;
            $('#submitchangebtn').removeAttr('disabled');
            $('#passwordChangeSuccess').removeAttr('hidden');
            setTimeout(function(){
                $('#passwordChangeSuccess').attr('hidden','hidden');
            }, 5000);
        }, function myError(xhr) {
            if(xhr.status == 422){
                $.each( xhr.data.errors, function( key, value ) {
                    $scope.error[key] = value[0];
                });
            }
            else{
                $scope.error.general = 'Something Went Wrong. Please try again.';
            }
            $('#submitchangebtn').removeAttr('disabled');
        });
    }

    $scope.resetForm = function(){
        $scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};
        $scope.error = {'general':null,'oldPassword':null,'password':null};
    }

});


profileapp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
				});
            });
        }
    }
}]);


profileapp.directive("fileread2", [function () {
    return {
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
				scope.profilephoto = changeEvent.target.files[0];				
                scope.uploadProfilePicture();
            });
        }
    }
}]);

profileapp.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});