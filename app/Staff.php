<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ConsultancyResetPasswordNotification;

class Staff extends Authenticatable
{
    use Notifiable;

    protected $guard = 'consultancy_user';
    protected $table = 'staffs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ConsultancyResetPasswordNotification($token));
    }
}