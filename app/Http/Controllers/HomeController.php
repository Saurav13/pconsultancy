<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;
use App\ContactUsMessage;
use App\Referal;
use App\Branch;
use App\Partner;
use App\Partnership;
use Session;
use Redirect;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $inProgressReferals=Referal::where('status','in-progress')->count()+Referal::where('status','submitted')->count()+Referal::where('status','seen')->count();
        $successReferals=Referal::where('status','success')->count()+Referal::where('status','paid')->count(); 

        $totalReferals=Referal::where('status','!=','unfinished')->count();

        if($totalReferals != 0){
            $inProgressPercent=intval(($inProgressReferals/$totalReferals)*100);
            $successPercent=intval(($successReferals/$totalReferals)*100);
        }
        else{
            $inProgressPercent = 0;
            $successPercent = 0;
        }
        $consultancies = Partner::all();

        return view('frontend.landing',compact('testimonials','successPercent','inProgressPercent','consultancies'));
    }
    public function about(Request $request)
    {
        $testimonials=Testimonial::all();
        
        return view('frontend.howitworks',compact('testimonials'));
    }

    public function contactPage(){
        return view('frontend.contact');
    }

    public function becomePartner(){
        return view('frontend.becomepartner');
    }

    public function partnershipSubmission(Request $request){
        // $request->validate([
        //     'buisness_name'=>'required',            
           
        // ]);
        $partnership= new Partnership;
        $partnership->buisness_name=$request->buisness_name;
        $partnership->locations=$request->locations;
        $partnership->contact_name=$request->contact_name;
        $partnership->contact_position=$request->contact_position;
        $partnership->tel_no=$request->tel_no;
        $partnership->mobile_no=$request->mobile_no;
        $partnership->partner_type=$request->partner_type;
        $partnership->gym_1=$request->gym_1;
        $partnership->gym_2=$request->gym_2;
        $partnership->cons_1=$request->cons_1;
        $partnership->cons_2=$request->cons_2;
        $partnership->tut_1=$request->tut_1;
        $partnership->real_1=$request->real_1;
        $partnership->save();
        Session::flash('message','Thank you for your Registration. A team member from Get Linked will contact you soon.');
        return Redirect::to('/become-a-partner');

    }
    public function contactUs(Request $request)
    {
        
        $request->validate([
            'name'=>'required',            
            'email' => 'required',
            'message' => 'required'
        ]);
        $contact= new ContactUsMessage;
        $contact->name=$request->name;
        $contact->email=$request->email;
        $contact->message=$request->message;
        $contact->save();

        $request->session()->flash('success','We will respond back soon.');
        
        return Redirect::to('/#contact-section');
    }
    public function notifications()
    {
        if(Auth::guard('consultancy_user')->check() )
        {
            $notifications=Auth::guard('consultancy_user')->user()->branch->notifications()->orderBy('id','desc')->paginate(10);
        }
        elseif(Auth::check())
        {
            $notifications=Auth::user()->notifications()->orderBy('id','desc')->paginate(10);
        }
        else{
            abort();
        }
        return view('frontend.notifications',compact('notifications'));
    }
    public function partner($alias)
    {
        $partner=Partner::where('alias',$alias)->with('branches')->firstOrFail();

        return view('frontend.partner',compact('partner'));
    }
}
