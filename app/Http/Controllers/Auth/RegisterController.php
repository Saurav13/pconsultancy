<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;
use Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\ConfirmUserEmail;
use Illuminate\Support\MessageBag;
use Auth;
use Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:consultancy_user');
        $this->middleware(function ($request,$next) {
            // if(!Session::has('url.intended'))
    	    	Session::put('url.intended',route('user.profile'));
    	    return $next($request);
        });
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6',
           
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => Str::random(60),
        ]);
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    { 
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->messages()->messages();
            if ($request->expectsJson()) {
                return response()->json($errors, 422);
            }

            return redirect()->route('register',array('next' => Session::get('url.intended')))
                ->withErrors($errors)->withInput();
        }
        event(new Registered($user = $this->create($request->all())));

        Mail::to($user->email)->send(new ConfirmUserEmail($user));

        $validator = Validator::make($user->toArray(), [
            'verified' => 'accepted'
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('email', 'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $user->email )).'" style="color:#4545c8">Resend</a> verification email?)');
            return redirect()->route('login',array('next' => Session::get('url.intended')))->withErrors($validator)->withInput();
        }
        return redirect()->route('login',array('next' => Session::get('url.intended')))->withInput();
    }

    public function resendConfirmation(Request $request){

        $email = $request->get('email');
        $user = User::where('email',$email)->first();
        if(!$user)
            abort(404,'User not found');

        $validator = Validator::make($user->toArray(), [
            'verified' => 'accepted'
        ]);

        if ($validator->fails()) {
            Mail::to($email)->send(new ConfirmUserEmail($user));
            $validator->errors()->add('email', 'Email Sent. Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('email' => $user['email'] )).'" style="color:#4545c8">Resend</a> verification email?)');
            return redirect('user/login?next='.Session::get('classurl.intended'))->withErrors($validator)->withInput();
        }
    }
    
    public function setEmail(Request $request){

        $socialUser = Session::get('no_email_user');
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users'
        ]);

        if ($validator->fails()) {
            return view('auth.getemail')->withErrors($validator)->with('user',$socialUser);
        }

        if(!$socialUser) return redirect('user/login?next='.Session::get('url.intended'));

        $socialUser->email = $request->email;

        if($socialUser->photo){
            $path = $socialUser->photo;
            $fileName = time().rand(111,999).'.'.'jpg';
            Image::make($path)->save(public_path('profile_images/' . $fileName));             
            $socialUser->photo= $fileName;
        }
        $socialUser->confirmation_code = Str::random(60);
        $socialUser->save();

        Session::forget('no_email_user');

        Mail::to($request->email)->send(new ConfirmUserEmail($socialUser));

        $validator = Validator::make($socialUser->toArray(), [
            'verified' => 'accepted'
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('email', 'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $socialUser->email )).'" style="color:#4545c8">Resend</a> verification email?)');
            return redirect()->route('login',array('next' => Session::get('url.intended')))->withErrors($validator)->withInput();
        }
        
    }

    public function confirmEmail($token){
        
        $user = User::where('confirmation_code','=',$token)->first();
        if(!$user){
            $errors = new MessageBag;
            $errors->add('email', 'The code is invalid');
            return redirect('user/login?next='.Session::get('classurl.intended'))->withErrors($errors)->withInput();
        }
        
        if($user->verified == 1){
            $errors = new MessageBag;
            $errors->add('email', 'Your account is already activated.');
            return redirect('user/login?next='.Session::get('classurl.intended'))->withErrors($errors)->withInput();
        }

        $user->verified = 1;
        $user->save();
        
        Auth::loginUsingId($user->id,true);

        return redirect(Session::get('url.intended'));
	        // if (!Subscriber::where('email', '=', $user->email)->exists()) {
            //     $subscriber = new Subscriber();
            //     $subscriber->email = $user->email;
            //     $subscriber->token = Str::random(60);
            //     $subscriber->save();
            // }
    }

    public function redirectTo(){
        return Session::get('url.intended');
    }
}
