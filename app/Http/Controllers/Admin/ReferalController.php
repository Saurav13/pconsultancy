<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Referal;
use App\Notification;
use Redirect;

class ReferalController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $referals=Referal::where('status','success')->orderBy('paid_status','desc')->paginate(10);
        return view('admin.referals.index',compact('referals'));
    }
    public function verifyReceipt(Request $request)
    {

        Referal::where('id',$request->id)->update(['paid_status'=>'verified']);
        $referal = Referal::findOrFail($request->id);
        Notification::add('Your commission payment of '.$referal->name.' has been accepted ','App\Branch',$referal->branch->id,'/cprofile/referal?referal_id='.$referal->id);

        $request->session()->flash('success', 'Verified Successfully.'); 
        return Redirect::to('/admin/referals');
    }

    public function rejectReceipt(Request $request)
    {
        
        
        Referal::where('id',$request->id)->update(['paid_status'=>'rebound']);
        $referal = Referal::findOrFail($request->id);

        Notification::add('Your commission payment of '.$referal->name.' has been rejected ','App\Branch',$referal->branch->id,'/cprofile/referal?referal_id='.$referal->id);
           
        $request->session()->flash('success', 'Rejected Successfully.'); 
        return Redirect::to('/admin/referals');
    }

    public function markAsPaid(Request $request)
    {
        Referal::where('id',$request->id)->update(['status'=>'paid']);
   
        $referal = Referal::findOrFail($request->id);

        Notification::add('Commission is paid to you for your referal of '.$referal->name,'App\User',$referal->user->id,'/profile');
        
        $request->session()->flash('success', 'Mark as Paid Successfully.'); 
        return Redirect::to('/admin/referals');
    }
}
