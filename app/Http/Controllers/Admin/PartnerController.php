<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Category;
use Session;

class PartnerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::orderBy('updated_at','desc')->paginate(10);
        $categories = Category::orderBy('title')->get();

        return view('admin.partners.index',compact('partners','categories'));
    }

    public function store(Request $request){
        $request->validate([
            'category' => 'required|exists:categories,id',
            'name' => 'required|string|max:191',
            'logo' => 'required|image',
            'description' => 'required'
        ]);
        
        $partner = new Partner;
        $partner->name = $request->name;
        $partner->category_id = $request->category;
        $partner->description = $request->description;

        $image = $request->logo;
        $filename = time(). '.' . $image->getClientOriginalExtension();
        $location = public_path('partner_logos/');
        $image->move($location,$filename);
        $partner->logo = $filename;
        $partner->save();

        Session::flash('success', 'Partner created.');
        return redirect()->back();
    }

    public function edit($id){
        $partner = Partner::findOrFail($id);
        $categories = Category::orderBy('title')->get();

        return view('admin.partners.edit',compact('partner','categories'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'category' => 'required|exists:categories,id',
            'name' => 'required|string|max:191',
            'logo' => 'nullable|image',
            'description' => 'required',
        ]);

        $partner = Partner::findOrFail($id);
        $partner->name = $request->name;
        $partner->category_id = $request->category;
        $partner->description = $request->description;

        if($request->hasFile('logo')){
            if($partner->logo)
                unlink(public_path('partner_logos/'.$partner->logo));

            $image = $request->logo;
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $location = public_path('partner_logos/');
            $image->move($location,$filename);
            $partner->logo = $filename;
        }

        $partner->save();
        
        Session::flash('success', 'Partner updated.');
        return redirect()->route('partners.index');     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $partner = Partner::findOrFail($id);

        if($partner->logo)
            unlink(public_path('partner_logos/'.$partner->logo));

        $partner->delete();
        
        Session::flash('success', 'Partner deleted.');
        return redirect()->back();        
    }
}
