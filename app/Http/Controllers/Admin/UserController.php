<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $users = User::where('verified',1)->orderBy('id','desc')->paginate(20);
        return view('admin.users.index',compact('users'));
    }
}
