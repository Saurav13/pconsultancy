<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Branch;
use App\Mail\NewConsultancyAdded;
use Session;
use Mail;

class BranchController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($partner_id)
    {
        $partner = Partner::findOrFail($partner_id);

        return view('admin.branches.index',compact('partner'));
    }

    public function store(Request $request, $partner_id){
        $request->validate([
            'name' => 'required|string|max:191',
            'location' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:staffs',
            'password' => 'required|string|min:6',
            
        ]);

        $partner = Partner::findOrFail($partner_id);

        $branch = $partner->branches()->create($request->all());

        $agent = $branch->staffs()->create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        Mail::to($request->email)->send(new NewConsultancyAdded($agent,$request->password));

        Session::flash('success', 'Branch created.');
        return redirect()->back();
    }

    public function edit($partner_id, $id){
        $branch = Branch::where('partner_id',$partner_id)->findOrFail($id);

        return view('admin.branches.edit',compact('branch'));
    }

    public function update(Request $request,$partner_id, $id)
    {
        $request->validate([
            'name' => 'required|string|max:191',
            'location' => 'required|string|max:191',
        ]);

        $branch = Branch::where('partner_id',$partner_id)->findOrFail($id);
        $branch->name = $request->name;
        $branch->location = $request->location;
        $branch->lat = $request->lat;
        $branch->long = $request->long;
        $branch->contact_email = $request->contact_email;
        $branch->contact_number = $request->contact_number;
        $branch->office_hours = $request->office_hours;
        $branch->save();
        
        Session::flash('success', 'Branch updated.');
        return redirect()->route('branches.index',$partner_id);
    }
    
    /*
    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:6',
        ]);

        $partner = Partner::findOrFail($request->id);
        $partner->password = bcrypt($data['password']);
        $partner->save();

        // Mail::to($partner->email)->send(new PartnerPasswordChanged($partner,$request->password));

        Session::flash('success', 'Partner password Updated.');
        return redirect()->route('partners.index',array('page'=>$request->page));
    }
    */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($partner_id, $id,Request $request)
    {
        $branch = Branch::where('partner_id',$partner_id)->findOrFail($id);

        $branch->delete();
        
        Session::flash('success', 'Branch deleted.');
        return redirect()->back();        
    }
}
