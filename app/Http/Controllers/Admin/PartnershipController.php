<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\NoEmptyContent;
use Mail;
use App\Partnership;
use Auth;
class PartnershipController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index(){
        $contact_messages = Partnership::orderBy('created_at','desc')->paginate(10);
        return view('admin.partnerships.index')->with('contacts',$contact_messages)->with('state','all');
    }

    public function unseenMsg(){
        $contact_messages = Partnership::where('seen','0')->orderBy('created_at','desc')->paginate(10);
        return view('admin.partnerships.index')->with('contacts',$contact_messages)->with('state','unseen');
    }

    public function show($id)
    {
        $Contact = Partnership::find($id);

        if(!$Contact)
            abort(404);

        if(!$Contact->seen){
            $Contact->seen = 1;
            $Contact->save();
        }
        
        return view('admin.partnerships.show')->with('contact',$Contact);
    }

    public function destroy($id,Request $request)
    {
        $Contact = Partnership::find($id);
        if(!$Contact)
            abort(404);
        $Contact->delete();

        $request->session()->flash('success', 'Message deleted');
        return redirect()->route('contact-us-messages.index');
    }


    public function getUnseenMsgCount(Request $request){
        
        if ($request->ajax()){
            $mno = Partnership::where('seen','0')->count();
            return response()->json(['mno' => $mno]);
        }
        abort(404);  
    }

    public function getUnseenMsg(Request $request){
        if ($request->ajax()){
            $messages = Partnership::where('seen','0')->latest()->limit(5)->get();
            $mno = Partnership::where('seen','0')->count();
            $view = view('admin.partials.pnotis')->with('messages',$messages)->with('mno',$mno)->render();
            
            return response()->json(['html' => $view, 'mno' => $mno]);
        }
        abort(404);
    }
}
