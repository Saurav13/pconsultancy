<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Redirect;

class NotificationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function getUnseenMsgCount(Request $request){
        
        if ($request->ajax()){
            $mno = Notification::where('notifiable_type','App\AdminUser')->where('seen','0')->count();
            return response()->json(['mno' => $mno]);
        }
        abort(404);  
    }

    public function getUnseenMsg(Request $request){
        if ($request->ajax()){
            $messages = Notification::where('notifiable_type','App\AdminUser')->where('seen','0')->latest()->limit(5)->get();
            $mno = Notification::where('notifiable_type','App\AdminUser')->where('seen','0')->count();
            $view = view('admin.partials.nnotis')->with('messages',$messages)->with('mno',$mno)->render();
            
            return response()->json(['html' => $view, 'mno' => $mno]);
        }
        abort(404);
    }

    public function viewNotification(Request $request)
    {
        $n= Notification::findOrFail($request->id);
        $n->seen=1;
        $n->save();
        return Redirect::to($n->link);
    }
}
