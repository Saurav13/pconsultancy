<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Branch;

class AdminController extends Controller
{ 
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
        // $this->middleware(function ($request, $next) {

        //     if (Auth::user()->roles()->where('title', '=', 'Dashboard')->exists()){
        //         return $next($request);
        //     }
        //     else
        //         abort(403);
        // });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Branch::all();
        $consultancies = [];
        foreach($all as $consultancy){
            $refers = $consultancy->referal_list;

            $c[0] = $consultancy->full_name;
            $c[1] = $refers->count();
            $c[2] = $refers->where('status','paid')->count();
            $c[3] = $refers->where('status','!=','paid')->where('status','!=','rejected')->count();
            $c[4] = $refers->where('status','rejected')->count();

            $consultancies []= $c;
        }
        if(count($consultancies)>0)
            array_unshift($consultancies,['Company', 'Total Refers', 'Successfull Refers','Pending Refers','Failed Refers']);

        return view('admin.dashboard',compact('consultancies'));
    }
}
