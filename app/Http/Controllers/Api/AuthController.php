<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Mail\ConfirmUserEmail;
use Carbon\Carbon;
use App\User;
use Auth;
use Mail;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'confirmation_code' => Str::random(60),
        ]);
        $user->save();

        Mail::to($request->email)->send(new ConfirmUserEmail($user));

        return response()->json([
            'message' => 'Account Successfully Created!. Please Check your email to verify your account.'
        ], 201);
    }
  
    public function resendConfirmation(Request $request){

        $email = $request->get('email');
        $user = User::where('email',$email)->first();
        if(!$user)
            return response()->json([
                'message' => 'User not found.'
            ], 404);

        if(!$user->verified){
            Mail::to($email)->send(new ConfirmUserEmail($user));
            return response()->json([
                'message' => 'Email Sent!. Please Check your email to verify your account.'
            ], 200);
        }else{
            return response()->json([
                'message' => 'Your Account has already been verified!. Please login.'
            ], 200);
        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'These credentials do not match our records.'
            ], 401);

        if(!$request->user()->verified){
            return response()->json([
                'message' => 'Your account is not verified yet!'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        // if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
<<<<<<< HEAD
            'user' => $user->only(['id','name','email','photo']),
=======
          	'user' => $user->only(['id','name','email','photo']),
>>>>>>> saurav-branch
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
