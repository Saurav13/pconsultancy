<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Consultancy;
use App\Referal;
use Auth;
use Image;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function index(Request $request)
    {
        $consultancies=Consultancy::select('id','name','commission_rate','logo')->get();
        $bankDetailsNotSet=false;
        $user =User::findOrFail($request->user()->id);
        if(Referal::where('user_id',$request->user()->id)->where('status','paid')->count()>0 && ($user->bank_name == null || $user->bank_account_number ==null ))
            $bankDetailsNotSet=true;

        return response()->json([
            'consultancies' => $consultancies,
            'bankDetailsNotSet' => $bankDetailsNotSet
        ], 200);
    }

    public function getReferal(Request $request)
    {
        
        $referals=Referal::where('user_id',$request->user()->id)->where('status','!=','paid')->orderBy('updated_at','desc')->paginate(6);
        $pagination=$referals->links('frontend.partials.pagination');

        return response()->json(['referals'=>$referals,'pagination'=>html_entity_decode($pagination)],200);
    }
    
    public function submitReferal(Request $request)
    {
        // dd($request->consultancy);
        $request->validate([
            'name' => 'required' ,
            'photo' => 'nullable|mimes:jpeg,jpg,png,gif,svg|max:10000',
            'phone'=>'required',
            'consultancy'=>'required'
        ]);
        $referal=new Referal;
        $referal->name=$request->name;
        $referal->email=$request->email;
        $referal->visa_type=$request->visa_type;
        $referal->phone=$request->phone;
        
        if($request->file('photo'))
        {

            $file=$request->file('photo');
            $destinationPath = public_path().'/'.'referal_photos';
            
            
            $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
            Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
            $referal->photo=$filename;
           
        }  

        $referal->consultancy_id=$request->consultancy;
        $referal->user_id=$request->user()->id;
        $referal->status='submitted';
        $referal->submitted_at=Carbon::now();
        $referal->save();

        return 'success';
    }

    public function submitEditedReferal(Request $request)
    {
        // dd($request->consultancy);
            $request->validate([
                'name' => 'required' ,
                'phone'=>'required',
                'consultancy'=>'required'
                
            ]);
        
        $referal=Referal::findOrFail($request->id);
        if($referal->photo==null || $request->file('photo') ){
            $request->validate([
                'photo' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10000',
            ]);
            $file=$request->file('photo');
            $destinationPath = public_path().'/'.'referal_photos';
            
            
            $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
            Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
            $referal->photo=$filename;
        }
        $referal->name=$request->name;
        $referal->email=$request->email;
        $referal->visa_type=$request->visa_type;
        $referal->phone=$request->phone;
        $referal->consultancy_id=$request->consultancy;
        $referal->user_id=$request->user()->id;
        $referal->status='submitted';
        $referal->submitted_at=Carbon::now();        
        $referal->save();

        return response()->json($referal,200);
    }

    public function saveReferal(Request $request)
    {
        // return $request->referal;
        // c    dd($request->all());

        $request->validate([
            'name' => 'required' ,
            'photo' => 'nullable|mimes:jpeg,jpg,png,gif,svg|max:10000'
            
        ]);
        $referal=new Referal;
        $referal->name=$request->name;
        $referal->email=$request->email;
        $referal->visa_type=$request->visa_type;
        $referal->phone=$request->phone;
        $referal->status='unfinished';
        if($request->file('photo'))
        {

            $file=$request->file('photo');
            $destinationPath = public_path().'/'.'referal_photos';
            
            
            $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
            Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
            $referal->photo=$filename;
           
        }  


        $referal->user_id=$request->user()->id;
        $referal->save();
        return 'success';
        
    }

    public function saveEditedReferal(Request $request)
    {
        // return $request->referal;
        // dd($request->all());
        $request->validate([
            'name' => 'required' ,
            'photo' => 'nullable|mimes:jpeg,jpg,png,gif,svg|max:10000'
            
        ]);
        $referal=Referal::findOrFail($request->id);
        $referal->name=$request->name;
        $referal->email=$request->email;
        $referal->visa_type=$request->visa_type;
        $referal->phone=$request->phone;
        $referal->status='unfinished';
        if($request->file('photo'))
        {

            $file=$request->file('photo');
            $destinationPath = public_path().'/'.'referal_photos';
            
            
            $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
            Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
            if($referal->photo)
            {
                unlink($destinationPath.'/'.$referal->photo);
            }
            $referal->photo=$filename;
           
        }  
        $referal->save();
        return response()->json($referal,200);
        
    }

    public function changeProfilePicture(Request $request)
    {
        $request->validate([
            
            'photo' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10000'
            
        ]);
       

        $file=$request->file('photo');
        $destinationPath = public_path().'/'.'profile_images';
        
        
        $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
        Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
        if(User::findOrFail($request->user()->id)->photo)
        {
            unlink($destinationPath.'/'.User::findOrFail($request->user()->id)->photo);
        }
        User::where('id',$request->user()->id)->update(['photo'=>$filename]);
          
        return $filename;
    }

    public function deleteReferal(Request $request)
    {
        // dd($request->all());
        $referal=Referal::findOrFail($request->id);
        if($referal->photo)
        {
            unlink(public_path().'/'.'referal_photos'.'/'.$referal->photo);
        }
        Referal::where('id',$request->id)->delete();
        return 'success';
    }

    public function saveInfo(Request $request)
    {
        $request->validate([
            'value' => 'required' ,
            
        ]);
        User::where('id',$request->user()->id)->update([$request->key=>$request->value]);

        return User::findOrFail($request->user()->id);
    }

    public function changePassword(Request $request)
    {
        $user = User::find($request->user()->id);
        
        if(!$user->password){
            $request->validate([
                'password' => 'required|min:6|confirmed',
            ]);
        }
        else{
            $request->validate([
                'oldPassword' => 'required',
                'password' => 'required|min:6|confirmed',
            ]);
        }

        if(!$user->password){
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['message'=>'Password Changed'], 200);
        }
        else{
            if ($request->attempt(['email' => $request->user()->email, 'password' => $request->oldPassword]))
            {
                $user->password = bcrypt($request->password);
                $user->save();

                return response()->json(['message'=>'Password Changed'], 200);
            }
            else{
                return response()->json(['errors'=>['oldPassword'=>[0 =>'Incorrect password']]], 422);
            }
        }
    }

    public function getCommission(Request $request)
    {
        $paidReferals=Referal::where('user_id',$request->user()->id)->where('status','paid')->paginate(6);
        $pagination=$paidReferals->links('frontend.partials.pagination');
       
        return response()->json(['paidReferals'=>$paidReferals,'pagination'=>html_entity_decode($pagination)],200);
    }  
}
