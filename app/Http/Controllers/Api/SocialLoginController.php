<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Two\InvalidStateException;
use Carbon\Carbon;
use App\User;
use Auth;
use Socialite;

class SocialLoginController extends Controller
{

    public function handleProviderCallback($provider,Request $request)
    {
         // 1 check if the user exists in our database with facebook_id
        // 2 if not create a new user
        // 3 login this user into our application
        if (!$request->has('code') || $request->has('denied')) {
            return response()->json([
                'message' => 'Something went wrong.'
            ], 401);
        }

        try
        {
            $socialUser = Socialite::driver($provider)->user(); 
        }
        catch(InvalidStateException $e){
            return response()->json([
                'message' => 'Something went wrong.'
            ], 401);
        }
        catch (Exception $e)
        {
            return response()->json([
                'message' => 'Something went wrong.'
            ], 401);
        }

        if(!$socialUser->getEmail()){
            return response()->json([
                'message' => "Email address not received!"
            ], 406);
        }

        $authUser = $this->findOrCreateUser($socialUser, $provider);
        
        if(!$authUser->photo){
            $path = $socialUser->avatar_original;
            $fileName = time().rand(111,999).'.'.'jpg';
            Image::make($path)->save(public_path('profile_images/' . $fileName));             
            $authUser->photo= $fileName;
            $authUser->save();
        }

        $tokenResult = $authUser->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
<<<<<<< HEAD
            'user' => $authUser->only(['id','name','email','photo']),
=======
          	'user' => $authUser,
>>>>>>> saurav-branch
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    private function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        if(User::where('email', $user->email)->exists()){
            $u = User::where('email', $user->email)->first();
            if(!$u->provider){
                $u->provider_id = $user->id;
                $u->provider = $provider;
            }
            
            if(!$u->verified){
                $u->verified = 1;
                $u->save();
            }

            return $u;
        }
        else{
            if($user->getEmail()){
                $u = new User;
                $u->provider = $provider;
                $u->provider_id = $user->getId();
                $u->name = $user->getName();
                $u->email = $user->getEmail();
	    
                $u->verified = 1;
                $u->save();
            }

            return $u;
        }
    }
}
