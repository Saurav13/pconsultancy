<?php

namespace App\Http\Controllers\Frontend\Consultancy\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/consultancy/cprofile' ;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:consultancy_user')->except('logout');
        $this->middleware(function ($request,$next) {
            // if(!Session::has('url.intended'))
    	    	Session::put('url.intended',route('consultancy.profile'));
    	    return $next($request);
        });
    }

    public function showLoginForm()
    {
        return view('frontend.consultancy.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('consultancy_user');
    }

    public function logout(Request $request)
    {
        
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('consultancy_login');
    }
}
