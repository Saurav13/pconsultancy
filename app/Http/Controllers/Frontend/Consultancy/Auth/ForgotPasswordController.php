<?php

namespace App\Http\Controllers\Frontend\Consultancy\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:consultancy_user');
        $this->middleware('guest');

        $this->middleware(function ($request,$next) {
            // if(!Session::has('url.intended'))
    	    	Session::put('url.intended',route('consultancy.profile'));
    	    return $next($request);
        });
    }

    public function showLinkRequestForm()
    {
        return view('frontend.consultancy.auth.passwords.email');
    }

    public function broker()
    {
        return Password::broker('consultency_agents');
    }
}
