<?php

namespace App\Http\Controllers\Frontend\Consultancy\Auth;

use App\Consultancy;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\NewConsultancyAdded;
use Session;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:consultancy_agents',
            'password' => 'required|string|min:6',
            'logo' => 'required|image',
            'description' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {
        $user = new Consultancy;
        $user->name = $data['name'];
        $user->description = $data['description'];

        $image = $data['logo'];
        $filename = time(). '.' . $image->getClientOriginalExtension();
        $location = public_path('consultancy_logos/');
        $image->move($location,$filename);
        $user->logo = $filename;
        $user->save(); 

        $agent = $user->agents()->create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        Mail::to($data['email'])->send(new NewConsultancyAdded($agent,$data['password']));
    }

    public function register(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required|string|max:191',
        //     'email' => 'required|string|email|max:191|unique:consultancies',
        //     'password' => 'required|string|min:6',
        //     'logo' => 'required|image',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()->route('consultancies.index')->withErrors($validator,'add')->withInput();
        // }
        $this->validator($request->all())->validate();

        $this->create($request->all());

        Session::flash('success','Consultancy Added');

        return redirect()->route('consultancies.index');
    }
}
