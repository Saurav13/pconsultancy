<?php

namespace App\Http\Controllers\Frontend\Consultancy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Referal;
use Auth;
use App\User;
use App\Branch;
use App\Staff;
use Image;
use App\Notification;

class ConsultancyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:consultancy_user');
    }
    
    public function index(Request $request)
    {
        $is_ajax=$request->ajax();
        if( (!isset($request->filter) && (!isset($request->searchKey)||$request->searchKey=='') )|| $request->filter=='all')
            $referals=Referal::with('user')->where('branch_id',Auth::user()->branch_id)->orderBy('submitted_at','desc')->paginate(6);
        else if(isset($request->searchKey))
        {
            $referals=Referal::with('user')->where('branch_id',Auth::user()->branch_id)->where('name','LIKE','%'.$request->searchKey.'%')->orderBy('submitted_at','desc')->paginate(6);
           
        }
        else
            $referals=Referal::with('user')->where('branch_id',Auth::user()->branch_id)->orderBy('submitted_at','desc')->where('status',$request->filter)->paginate(6);
            
        // dd($request->ajax());
        $pagination=$referals->links('frontend.partials.pagination');
        // dd(html_entity_decode($pagination));
        // $pagination->render();
        
        if($is_ajax)
        {
            return response()->json(['referals'=>$referals,'pagination'=>html_entity_decode($pagination)],200);
        }
        $competitors=$this->getCompetitors();
        return view('frontend.consultancy.cprofile',compact('referals','competitors'));
    }

    public function referal(Request $request)
    {
        
        $referal=Referal::findOrFail($request->referal_id);
        if($referal->status=='submitted')
        {
            $referal->status='seen';
            $referal->save();
        }
        $referal->user=$referal->user;
        return view('frontend.referal',compact('referal'));
    }

    public function toggleStatus(Request $request)
    {
        $referal=Referal::findOrFail($request->id);
        $referal->status=$request->status;
        $referal->save();
        if($request->status=='success')
        {
            Notification::add('Your referral of '.$referal->name.' is successful','App\User',$referal->user->id,'/profile');
            Notification::add('New referal successful. ','App\AdminUser',1,'/admin/referals');

        }
      

        return $referal->status;
    }

    public function submitReceipt(Request $request)
    {
        $request->validate([
            'id'=>'required',
            'receipt' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx,docm,txt,svg|max:10000'
            
        ]);
        $referal=Referal::findOrFail($request->id);
        $file=$request->file('receipt');
        $destinationPath = public_path().'/'.'receipt_images';
        
        
        $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
        Image::make($file)->save($destinationPath.'/'.$filename);
        $referal->receipt=$filename;
        $referal->paid_status='unverified';
        $referal->save();
      
        Notification::add('New commission payment received from '.$referal->branch->name. ' for '.$referal->user->name,'App\AdminUser',1,'/admin/referals');

      

        return $referal->paid_status;

    }

    public function dashboard()
    {
        $referals=Referal::where('branch_id',Auth::user()->branch_id)->get();
        
        $referalPartners=User::whereIn('id',$referals->pluck('user_id'))->get();
        $referalPartners=$referalPartners->map(function($item,$key) use($referals) {
            $item->count=$referals->where('user_id',$item->id)->count();
            return $item;
        });

        $submittedReferals=$referals->where('status','submitted')->count();
        $seenReferals=$referals->where('status','seen')->count();
        $inProgressReferals=$referals->where('status','in-progress')->count();
        $successReferals=$referals->where('status','success')->count();
        $paidReferals=$referals->where('status','paid')->count();
        $rejectedReferals=$referals->where('status','rejected')->count();
        $totalReferals=$referals->count();
        
        

        
        return response()->json(['referalPartners'=>$referalPartners,
                                    'user'=>Branch::findOrFail(Auth::user()->branch_id),
                                    'submittedReferals'=>$submittedReferals,
                                    'seenReferals'=>$seenReferals,
                                    'inProgressReferals'=>$inProgressReferals,
                                    'successReferals'=>$successReferals,
                                    'paidReferals'=>$paidReferals,
                                    'rejectedReferals'=>$rejectedReferals,
                                    'totalReferals'=>$totalReferals,
                                    
                                ],200);
    }

    private function getCompetitors()
    {
        $consultancies=Branch::all();
        $competitors=[];
        foreach($consultancies as $c)
        {
            $comp=[];
            $comp['name']=$c->name;
            $comp['id']=$c->id;
            $comp['submittedReferals']=Referal::where('branch_id',$c->id)->where('status','submitted')->count();
            $comp['seenReferals']=Referal::where('branch_id',$c->id)->where('status','seen')->count();
            $comp['inProgressReferals']=Referal::where('branch_id',$c->id)->where('status','in-progress')->count();
            $comp['successReferals']=Referal::where('branch_id',$c->id)->where('status','success')->count();
            $comp['paidReferals']=Referal::where('branch_id',$c->id)->where('status','paid')->count();
            $comp['rejectedReferals']=Referal::where('branch_id',$c->id)->where('status','rejected')->count(); 
            $competitors[]=$comp;
        }
        return $competitors;
    }

    public function saveCommission(Request $request)
    {
        $request->validate([
            'newCommission' => 'required|integer',
           
        ]);
        Branch::where('id',Auth::user()->branch_id)->update(['commission_rate'=>$request->newCommission]);
        return $request->newCommission;
    }

    public function toggleShowRate(Request $request)
    {
        $consultancy=Consultancy::findOrFail(Auth::user()->consultancy_id);
        if($consultancy->show_rate=='hide')
        {
            $consultancy->show_rate='show';
        }
        else{
            $consultancy->show_rate='hide';
            
        }
        $consultancy->save();
        return $consultancy->show_rate;
    }

    public function changePassword(Request $request)
    {
        $user = Staff::find(Auth::guard('consultancy_user')->user()->id);
        
        if(!$user->password){
            $request->validate([
                'password' => 'required|min:6|confirmed',
            ]);
        }
        else{
            $request->validate([
                'oldPassword' => 'required',
                'password' => 'required|min:6|confirmed',
            ]);
        }

        if(!$user->password){
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['success'=>'Password Changed'], 200);
        }
        else{
            if (Auth::attempt(['email' => Auth::guard('consultancy_user')->user()->email, 'password' => $request->oldPassword]))
            {
                $user->password = bcrypt($request->password);
                $user->save();

                return response()->json(['success'=>'Password Changed'], 200);
            }
            else{
                return response()->json(['errors'=>['oldPassword'=>[0 =>'Incorrect password']]], 422);
            }
        }
    }
    
    public function editProfile(Request $request)
    {
        return view('frontend.consultancy.editprofile');
    }
}