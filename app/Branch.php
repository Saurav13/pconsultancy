<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name', 'location','lat','long','contact_email','contact_number','office_hours'];

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function staffs()
    {
        return $this->hasMany('App\Staff');
    }

    public function referal_list()
    {
        return $this->hasMany('App\Referal');
    }

    public function getFullNameAttribute(){
        return $this->partner->name.' ('.$this->attributes['name'].')';
    }

    public function getEmailAttribute(){
        return $this->staffs()->where('role','Admin')->first()->email;
    }
    
    public function notifications()
    {
        return $this->morphMany('App\Notification', 'notifiable');
    }
}
