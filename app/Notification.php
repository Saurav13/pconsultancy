<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mail\NotificationEmail;
use Mail;

class Notification extends Model
{
    public function notifiable()
    {
        return $this->morphTo();
    }
    public static function add($message,$notifiable_type,$notifiable_id,$link)
    {
        $n= new Notification;
        $n->message=$message;
        $n->notifiable_type=$notifiable_type;
        $n->notifiable_id=$notifiable_id;
        $n->link=$link;
        $n->save();

        try{
            $user = $notifiable_type::find($notifiable_id);
            $name = $notifiable_type == 'App\Branch' ? $user->full_name : $user->name;
            Mail::to($user->email)->send(new NotificationEmail($name,$message,$link));
        }
        catch(\Exception $e){

        }
    }
}
