<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user,$content,$link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$content,$link)
    {
        $this->user = $user;
        $this->content = $content;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Notification')->view('emails.notification')->with('user',$this->user)->with('content',$this->content)->with('link',$this->link);
    }
}
