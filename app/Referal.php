<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Consultancy;

class Referal extends Model
{
    public $appends=['consultancy_name'];
    public function getConsultancyNameAttribute()
    {
        if($this->consultancy_id!=null)
        {
            return Consultancy::findOrFail($this->consultancy_id)->name;
        }
        else
            return '';
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
}
