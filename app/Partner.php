<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Partner extends Model
{
    use Sluggable;

    protected $fillable = [
        'name', 'logo','description'
    ];

    public function sluggable()
    {
        return [
            'alias' => [
                'source' => ['name']
            ]
        ];
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function branches()
    {
        return $this->hasMany('App\Branch');
    }
}
