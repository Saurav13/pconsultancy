<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partnerships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('buisness_name');
            $table->text('locations')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_position')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('partner_type');
            $table->string('gym_1')->nullable();
            $table->string('gym_2')->nullable();
            $table->string('cons_1')->nullable();
            $table->string('cons_2')->nullable();
            $table->string('tut_1')->nullable();
            $table->string('real_1')->nullable();
            $table->boolean('seen')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partnerships');
    }
}
