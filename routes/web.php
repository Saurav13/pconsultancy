<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/how-it-works', 'HomeController@about');
Route::get('/become-a-partner', 'HomeController@becomePartner');
Route::post('/partnership-submission','HomeController@partnershipSubmission');
Route::get('/partner/{alias}','HomeController@partner');


Route::get('/profile','Frontend\UserController@index')->name('user.profile');
Route::get('/cprofile/referal','Frontend\Consultancy\ConsultancyController@referal');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@index');

Route::post('/subscribe','HomeController@subscribe')->name('subscribe');
Route::get('unsubscribe/{token}','HomeController@unsubscribe')->name('unsubscribe');
Route::post('unsubscribe/{token}','HomeController@unsubscribed')->name('unsubscribed');

Route::get('/contact-us', 'HomeController@contactPage')->name('contact-us');
Route::post('/contact-us','HomeController@contactUs');

Route::get('/notifications','HomeController@notifications');

Route::prefix('consultancy')->group(function(){
    Route::get('/login','Frontend\Consultancy\Auth\LoginController@showLoginForm')->name('consultancy_login');
	Route::post('/login','Frontend\Consultancy\Auth\LoginController@login');
	Route::post('/logout','Frontend\Consultancy\Auth\LoginController@logout')->name('consultancy_logout');
	Route::post('/password/email','Frontend\Consultancy\Auth\ForgotPasswordController@sendResetLinkEmail')->name('consultancy_password.email');
	Route::post('/password/reset','Frontend\Consultancy\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Frontend\Consultancy\Auth\ForgotPasswordController@showLinkRequestForm')->name('consultancy_password.request');
	Route::get('/password/reset/{token}','Frontend\Consultancy\Auth\ResetPasswordController@showResetForm')->name('consultancy_password.reset');
    
    // Route::get('profile','Frontend\Consultancy\ProfileController@index');
    // Route::post('profile/changePassword','Frontend\Consultancy\ProfileController@changePassword')->name('consultancy.changePassword');
    // Route::post('profile/changeName','Frontend\Consultancy\ProfileController@updateName')->name('consultancy.updateName');

    Route::get('/cprofile','Frontend\Consultancy\ConsultancyController@index')->name('consultancy.profile');
    Route::get('/editprofile','Frontend\Consultancy\ConsultancyController@editProfile');
    Route::post('changepassword','Frontend\Consultancy\ConsultancyController@changePassword')->name('consultancy.changePassword');
   
    Route::get('/cprofile/referal','Frontend\Consultancy\ConsultancyController@referal');  
    Route::post('/referal/togglestatus','Frontend\Consultancy\ConsultancyController@toggleStatus');  
    Route::post('/referal/submitreceipt','Frontend\Consultancy\ConsultancyController@submitReceipt');  
          
    Route::get('/dashboard','Frontend\Consultancy\ConsultancyController@dashboard');
    Route::post('/dashboard/savecommission','Frontend\Consultancy\ConsultancyController@saveCommission');
    Route::post('/dashboard/toggleshowrate','Frontend\Consultancy\ConsultancyController@toggleShowRate');
    
    
    
});

Route::prefix('user')->group(function(){
    Auth::routes();
    Route::get('/register/getEmail',function(){
        return redirect('user/login');
    });

    Route::get('/editprofile','Frontend\UserController@editProfile');
    
    
    Route::post('/register/getEmail','Auth\RegisterController@setEmail')->name('set_email');
    Route::get('confirm/resend','Auth\RegisterController@resendConfirmation')->name('resend_confirmation');
    Route::get('confirm/{token}','Auth\RegisterController@confirmEmail')->name('confirmEmail');

    // Route::get('/profile','Frontend\UserController@index')->name('user.profile');

    Route::post('/changeprofilepicture','Frontend\UserController@changeProfilePicture');
    Route::get('/getreferal','Frontend\UserController@getReferal');    
    Route::post('/submitreferal','Frontend\UserController@submitReferal');
    Route::post('/savereferal','Frontend\UserController@saveReferal');
    Route::post('/submitreferaledit','Frontend\UserController@submitEditedReferal');
    Route::post('/savereferaledit','Frontend\UserController@saveEditedReferal');
    Route::post('/deletereferal','Frontend\UserController@deleteReferal');
    Route::post('/saveinfo','Frontend\UserController@saveInfo');
    Route::get('/getcommission','Frontend\UserController@getCommission');    
    Route::post('profile/changepassword','Frontend\UserController@changePassword')->name('user.changePassword');
});
Route::get('signin/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('signin/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

    Route::resource('categories', 'Admin\CategoryController',['except' => ['create','edit','show']]);

    Route::resource('partners', 'Admin\PartnerController',['except' => ['create','show']]);

    Route::resource('partners/{partner_id}/branches', 'Admin\BranchController',['except' => ['create','show']]);

    Route::get('users','Admin\UserController@index')->name('users.index');

    Route::get('referals','Admin\ReferalController@index')->name('referals.index');
    Route::post('referals/verifyreceipt','Admin\ReferalController@verifyReceipt');
    Route::post('referals/rejectreceipt','Admin\ReferalController@rejectReceipt');
    Route::post('referals/markaspaid','Admin\ReferalController@markAsPaid');
    

    // Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    // Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    // Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);

    // Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    // Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    // Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    // Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    // Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');

    Route::get('partnerships/unseen','Admin\PartnershipController@unseenMsg')->name('partnerships.unseen');    
    Route::resource('partnerships', 'Admin\PartnershipController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenPartnershipsCount','Admin\PartnershipController@getUnseenMsgCount')->name('getUnseenPartnershipCount');
    Route::get('getUnseenPartnershipsMsg','Admin\PartnershipController@getUnseenMsg')->name('getUnseenPartnership');

    Route::get('getUnseenNotificationCount','Admin\NotificationController@getUnseenMsgCount')->name('getUnseenNotificationCount');
    Route::get('getUnseenNotificationMsg','Admin\NotificationController@getUnseenMsg')->name('getUnseenNotification');
    Route::get('/notifications/view','Admin\NotificationController@viewNotification');
    Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');
    // Route::post('test','Admin\AdminController@test')->name('test');

    Route::get('/','Admin\AdminController@index');
});

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    $ext = explode(".",$img);
    $ext = end($ext);
    $source = str_replace('*','/',$source);
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('optimize');

