<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::get('signin/{provider}/callback', 'Api\SocialLoginController@handleProviderCallback');
Route::get('resend', 'Api\AuthController@resendConfirmation');
Route::post('/password/reset', 'Api\PasswordResetController@sendResetLinkEmail');

Route::group(['middleware' => 'auth:api'],function(){
    Route::get('logout', 'Api\AuthController@logout');

    Route::get('user', 'Api\UserController@user');
    Route::get('/editprofile','Frontend\UserController@editProfile');
    Route::post('/changeprofilepicture','Api\UserController@changeProfilePicture');
    Route::get('/getreferal','Api\UserController@getReferal');  
    Route::get('/getconsultancies','Api\UserController@index');    
    Route::post('/submitreferal','Api\UserController@submitReferal');
    Route::post('/savereferal','Api\UserController@saveReferal');
    Route::post('/submitreferaledit','Api\UserController@submitEditedReferal');
    Route::post('/savereferaledit','Api\UserController@saveEditedReferal');
    Route::post('/deletereferal','Api\UserController@deleteReferal');
    Route::post('/saveinfo','Api\UserController@saveInfo');
    Route::get('/getcommission','Api\UserController@getCommission');    
    Route::post('/changepassword','Api\UserController@changePassword');
});
    