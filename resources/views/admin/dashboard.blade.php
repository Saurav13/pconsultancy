@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Dashboard</h2>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Partner Chart</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            @if(count($consultancies)>0)
                                <div id="column-chart"></div>
                            @else
                                <p class="card-text">No Partners Yet</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
@stop

@section('js')
    <script src="https://www.google.com/jsapi" type="text/javascript"></script>

    <script src="{{asset('admin-assets/app-assets/js/scripts/charts/google/bar/column.js') }}" type="text/javascript"></script>

    <script>
        google.load('visualization', '1.0', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(drawColumn);

        // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
        function drawColumn() {
            
            // Create the data table.
            var data = google.visualization.arrayToDataTable(<?php echo json_encode($consultancies) ?>);


            // Set chart options
            var options_column = {
                height: 400,
                fontSize: 12,
                colors:['#2494be','#37BC9B','#F6B75A','#e84a5f'],
                chartArea: {
                    left: '5%',
                    width: '100%',
                    height: 350
                },
                vAxis: {
                    gridlines:{
                        color: '#e9e9e9',
                        count: 10
                    },
                    minValue: 0
                },
                legend: {
                    position: 'top',
                    alignment: 'center',
                    textStyle: {
                        fontSize: 12
                    }
                }
            };

            // Instantiate and draw our chart, passing in some options.
            var bar = new google.visualization.ColumnChart(document.getElementById('column-chart'));
            bar.draw(data, options_column);

        }


        // Resize chart
        // ------------------------------

        $(function () {

            // Resize chart on menu width change and window resize
            $(window).on('resize', resize);
            $(".menu-toggle").on('click', resize);

            // Resize function
            function resize() {
                drawColumn();
            }
        });
</script>
@endsection