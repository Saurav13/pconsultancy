@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary mr-1 mb-1 " href="{{ route('branches.index',$branch->partner->id) }}" title="Back">Back</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Partner Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('branches.update',[$branch->partner->id,$branch->id]) }}">
                        
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH"/>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text"  name="name" value="{{ $branch->name }}" placeholder="Name" required>

                            @if ($errors->has('name'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input class="form-control{{ $errors->has('location') ? ' border-danger' : '' }}" id="location" type="text"  value="{{ $branch->location }}" name="location" value="{{ $branch->location }}" placeholder="Location" required>

                            @if ($errors->has('location'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('location') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                                <label for="">Contact Email</label>
                                <input class="form-control{{ $errors->has('contact_email') ? ' border-danger' : '' }}" id="contact_eemail" type="text" value="{{ $branch->contact_email }}" name="contact_email" value="{{ old('contact_email') }}" placeholder="Contact Email" required>

                                @if ($errors->has('contact_email'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Contact Number</label>
                                <input class="form-control{{ $errors->has('contact_number') ? ' border-danger' : '' }}" id="contact_number" type="text"  value="{{ $branch->contact_number }}" name="contact_number" value="{{ old('contact_number') }}" placeholder="Contact Number" required>

                                @if ($errors->has('contact_number'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Latitude</label>
                                <input class="form-control{{ $errors->has('lat') ? ' border-danger' : '' }}" id="lat" type="text"  name="lat" value="{{ $branch->lat }}" placeholder="Latitude" required>

                                @if ($errors->has('lat'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('lat') }}</strong>
                                    </div>
                                @endif
                            </div>
                             <div class="form-group">
                                <label for="">Longitude</label>
                                <input class="form-control{{ $errors->has('long') ? ' border-danger' : '' }}" id="long" type="text"  name="long" value="{{ $branch->long }}" placeholder="Longitude" required>

                                @if ($errors->has('long'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('long') }}</strong>
                                    </div>
                                @endif
                            </div>
                             <div class="form-group">
                                <label for="">Office hours</label>
                                <input class="form-control{{ $errors->has('office_hours') ? ' border-danger' : '' }}" id="office_hours" type="text"  name="office_hours" value="{{ $branch->office_hours }}" placeholder="Office Hours" required>

                                @if ($errors->has('office_hours'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('office_hours') }}</strong>
                                    </div>
                                @endif
                            </div>
                            

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-warning">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
  
@endsection