@extends('layouts.admin')

@section('body')
   
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">{{ $partner->name }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary mr-1 mb-1 " href="{{ route('partners.index') }}" title="Back">Back</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><a data-action="collapse">Add New Branch</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddBranchForm" action="{{ route('branches.store',$partner->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Branch</h4>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text"  name="name" value="{{ old('name') }}" placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="location">Location</label>
                                <input class="form-control{{ $errors->has('location') ? ' border-danger' : '' }}" id="location" type="text"  name="location" value="{{ old('location') }}" placeholder="Location" required>

                                @if ($errors->has('location'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Contact Email</label>
                                <input class="form-control{{ $errors->has('contact_email') ? ' border-danger' : '' }}" id="contact_eemail" type="text"  name="contact_email" value="{{ old('contact_email') }}" placeholder="Contact Email" required>

                                @if ($errors->has('contact_email'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Contact Number</label>
                                <input class="form-control{{ $errors->has('contact_number') ? ' border-danger' : '' }}" id="contact_number" type="text"  name="contact_number" value="{{ old('contact_number') }}" placeholder="Contact Number" required>

                                @if ($errors->has('contact_number'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Latitude</label>
                                <input class="form-control{{ $errors->has('lat') ? ' border-danger' : '' }}" id="lat" type="text"  name="lat" value="{{ old('lat') }}" placeholder="Latitude" required>

                                @if ($errors->has('lat'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('lat') }}</strong>
                                    </div>
                                @endif
                            </div>
                             <div class="form-group">
                                <label for="">Longitude</label>
                                <input class="form-control{{ $errors->has('long') ? ' border-danger' : '' }}" id="long" type="text"  name="long" value="{{ old('long') }}" placeholder="Longitude" required>

                                @if ($errors->has('long'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('long') }}</strong>
                                    </div>
                                @endif
                            </div>
                             <div class="form-group">
                                <label for="">Office hours</label>
                                <input class="form-control{{ $errors->has('office_hours') ? ' border-danger' : '' }}" id="office_hours" type="text"  name="office_hours" value="{{ old('office_hours') }}" placeholder="Office Hours" required>

                                @if ($errors->has('office_hours'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('office_hours') }}</strong>
                                    </div>
                                @endif
                            </div>
                            

                            <div class="form-group">
                                <label for="userinput5">Login Email</label>
                                <input class="form-control{{ $errors->has('email') ? ' border-danger' : '' }}" type="email" placeholder="Email"  name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="input-group">   
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control{{ $errors->has('password') ? ' border-danger' : '' }}" id="password" name="password" rel="gp" data-size="10" data-character-set="a-z,A-Z,0-9" required>
                                    <span class="input-group-btn"><button type="button" class="btn btn-primary getNewPass" style="margin-top:27px"><span class="icon-reload"></span></button></span>
                                </div>
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Branches</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Email</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($partner->branches as $branch)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $branch->name }}</td>
                                    <td>{{ $branch->location }}</td>
                                    <td>{{ $branch->staffs()->where('role','Admin')->first()->email }}</td>
                                    <td>
                                        <a href="{{ route('branches.edit',[$partner->id,$branch->id]) }}" class="btn btn-outline-warning" title="Update Details"><i class="icon-edit"></i></a>
                                        {{-- <button type="button" class="btn btn-outline-warning" onclick="editPassword({{ $branch->id }})" title="Update Password"><i class="icon-key"></i></button> --}}
                                        
                                        <form action="{{ route('branches.destroy',[$partner->id,$branch->id]) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE"/>
                                            <button id='deleteBranch{{ $branch->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade text-xs-left" id="edit" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form class="form" id="branchEditForm" method="POST" action="">
                                <div class="modal-body">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h4 class="form-section">Branch Details Update</h4>
                                        
                                        <input type="text" name="id" value='' id="branchId" hidden/>
                                        <div class="form-group">
                                            <h5 id="branchName"></h5>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Description" id="branchDescription" rows="18" name="description"></textarea>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                                </div>
                            </form>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
@endsection

@section('js')
   <script>
       // Generate a password string
       function randString(id){
           var dataSet = $(id).attr('data-character-set').split(',');  
           var possible = '';
           if($.inArray('a-z', dataSet) >= 0){
               possible += 'abcdefghijklmnopqrstuvwxyz';
           }
           if($.inArray('A-Z', dataSet) >= 0){
               possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
           }
           if($.inArray('0-9', dataSet) >= 0){
               possible += '0123456789';
           }
           if($.inArray('#', dataSet) >= 0){
               possible += '![]{}()%&*$#^<>~@|';
           }
           var text = '';
           for(var i=0; i < $(id).attr('data-size'); i++) {
               text += possible.charAt(Math.floor(Math.random() * possible.length));
           }
           return text;
       }

       // Create a new password
       $(".getNewPass").click(function(){
           var field = $(this).closest('div').find('input[rel="gp"]');
           field.val(randString(field));
       });

       function edit(branch){
           $('#edit').modal('show');
           $('#branchId').val(branch.id);
           $('#branchName').text(branch.name);
           $('#branchDescription').val(branch.description);
       }

       // function editPassword(id){
       //     $('#editPassword').modal('show');
       //     $('#branchId2').val(id);

       // }

       

   </script>
@endsection