@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary mr-1 mb-1 " href="{{ route('partners.index') }}" title="Back">Back</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Partner Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('partners.update',$partner->id) }}"  enctype="multipart/form-data">
                        
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH"/>

                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control{{ $errors->has('category') ? ' border-danger' : '' }}" id="category"  name="category" required>
                                <option value="" selected disabled>Choose a category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $partner->category_id == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('category'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text"  name="name" value="{{ $partner->name }}" placeholder="Name" required>

                            @if ($errors->has('name'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="userinput5">Logo (Optional)</label>
                            <input class="form-control{{ $errors->has('logo') ? ' border-danger' : '' }}" type="file" placeholder="Logo"  name="logo">

                            @if ($errors->has('logo'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description" rows="5" name="description" required>{{ $partner->description }}</textarea>

                            @if ($errors->has('description'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-warning">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
  
@endsection