@extends('layouts.admin')

@section('body')
   
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><a data-action="collapse">Add New Partner</a></h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" id="AddPartnerForm" action="{{ route('partners.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> About Partner</h4>

                            <div class="form-group">
                                <label for="category">Category</label>
                                <select class="form-control{{ $errors->has('category') ? ' border-danger' : '' }}" id="category"  name="category" required>
                                    <option value="" selected disabled>Choose a category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category') == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('category'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" id="name" type="text"  name="name" value="{{ old('name') }}" placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="userinput5">Logo</label>
                                <input class="form-control{{ $errors->has('logo') ? ' border-danger' : '' }}" type="file" placeholder="Logo"  name="logo" required>

                                @if ($errors->has('logo'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" placeholder="Description" rows="5" name="description" required></textarea>

                                @if ($errors->has('description'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Partners</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Logo</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($partners as $partner)
                                <tr>
                                    <td>{{ $loop->iteration + (($partners->currentPage()-1) * $partners->perPage()) }}</td>
                                    <td>{{ $partner->name }}</td>
                                    <td>
                                            {{-- <div class="tag tag-info" style="margin:2px">{{ $role->title }}</div> --}}
                                        <img src="/partner_logos/{{ $partner->logo }}" style="height:50px;width:50px"/>
                                    </td>
                                    <td>{{ $partner->category->title }}</td>
                                    <td title="{{ $partner->description }}">{{ str_limit($partner->description, $limit = 150, $end = '...') }}</td>
                                    <td>
                                        <a class="btn btn-outline-warning" href="{{ route('partners.edit',$partner->id) }}" title="Update Details"><i class="icon-edit"></i></a>
                                        <a href="{{ route('branches.index',$partner->id) }}" class="btn btn-outline-success" title="Add Branches"><i class="icon-sitemap"></i></a>
                                        
                                        <form action="{{ route('partners.destroy',$partner->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE"/>
                                            <button id='deletePartner{{ $partner->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $partners->links('admin.partials.pagination') }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
@endsection