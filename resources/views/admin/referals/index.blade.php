@extends('layouts.admin')

@section('body')
   <style>
       .table th, .table td {
     padding: 1rem 1rem; 
}
       </style>
   <div class="content-header row">
   </div>
   <div class="content-body">
       
       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Successful referals</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>Referal Id</th>
                                   <th>Partner</th>
                                   <th>Refered By:</th>
                                   <th>Referal Status</th>
                                   <th>Paid Status</th>
                                   <th>Receipt</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($referals as $referal)
                               <tr>
                                   <td>{{ $referal->id }}</td>
                                   <td>{{ $referal->branch->full_name }}</td>
                                   
                                   <td>
                                           {{-- <div class="tag tag-info" style="margin:2px">{{ $role->title }}</div> --}}
                                       
                                        <table>
                                            <tr> 
                                                <td>
                                                        @if($referal->user->photo!=null)
                                                            <img src="/profile_images/{{ $referal->user->photo }}" style="height:50px;width:50px;border-radius:50%;"/>
                                                        @endif
                                                </td>
                                                <td>
                                                    <p>Name: {{$referal->user->name}}</p>
                                                    <p>Email: {{$referal->user->email}}</p>
                                                    <p>Bank: {{$referal->user->bank_name}}</p>
                                                    <p>AC no: {{$referal->user->bank_account_number}}</p>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                       
                                    </td>
                                    <td>{{$referal->status}}</td>
                                    <td>{{$referal->paid_status}}</td>
                                    <td>
                                        @if($referal->receipt!=null)
                                            <img src="/receipt_images/{{ $referal->receipt }}" style="height:50px;width:50px;"/>
                                        @endif
                                    </td>
                                    
                                   <td>
                                       @if($referal->paid_status=='unverified' || $referal->paid_status=='rebound')
                                   <a href="#!" id="verifyReferalBtn{{$referal->id}}" verifyId="{{$referal->id}}" class="btn btn-success" title="Verify Payment" data-toggle="modal" data-target="#verifyConfirmation"><i class="icon-check" style="color:white"></i></a>
                                   <a href="#!" id="rejectReferalBtn{{$referal->id}}" verifyId="{{$referal->id}}" class="btn btn-danger" title="Reject Verification" data-toggle="modal" data-target="#rejectConfirmation"><i class="icon-times" style="color:white"></i></a>
                                       
                                   @endif
                                       @if($referal->receipt!=null)
                                        <a href="/receipt_images/{{$referal->receipt}}" target="_blank" id="viewReceipt" class="btn btn-primary" title="Download Receipt"><i class="icon-download"></i></a>
                                       @endif
                                       @if($referal->status=='success')
                                        <a href="#!" id="markAsPaidBtn{{$referal->id}}" verifyId="{{$referal->id}}"  class="btn btn-primary" title="Mark as Paid" data-toggle="modal" data-target="#markAsPaidConfirmation"><i class="icon-check"></i></a>
                                       @endif
                                       {{-- <form action="{{ route('referals.destroy',$referal->id) }}" method="POST" style="display:inline">
                                           {{ csrf_field() }}
                                           <input type="hidden" name="page" value="{{$referals->currentPage()}}"/>
                                           <button id='deletereferal' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                       </form> --}}
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $referals->links('admin.partials.pagination') }}
                           </nav>
                       </div>
                   </div>
               </div>
               <div class="modal fade text-xs-left" id="verifyConfirmation" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-body">
                                  <h4>Is it verified??</h4>
                                  <p>Consultancy will be notified about this verification.</p>

                                </div>
                                <div class="modal-footer">
                                    <form class="form" id="consultancyEditForm" method="POST" action="/admin/referals/verifyreceipt">
                                        {{csrf_field()}}
                                        <input type="text" hidden id="verify_id" name="id" value="0"/>
                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-primary">Verify</button>
                                    </form> 
                                </div>
                                                              
                        </div>
                    </div>
                </div>
                <div class="modal fade text-xs-left" id="markAsPaidConfirmation" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-body">
                                  <h4>Is it Paid??</h4>
                                  <p>User will be notified about this verification. Status of referal will be set to 'Paid'</p>

                                </div>
                                <div class="modal-footer">
                                    <form class="form" id="consultancyEditForm" method="POST" action="/admin/referals/markaspaid">
                                        {{csrf_field()}}
                                        <input type="text" hidden id="markAsPaid_id" name="id" value="0"/>
                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-primary">Mark as Paid</button>
                                    </form> 
                                </div>
                                                              
                        </div>
                    </div>
                </div>
                <div class="modal fade text-xs-left" id="rejectConfirmation" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" >
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                    <div class="modal-body">
                                      <h4>Are you sure??</h4>
                                      <p>Consultancy will be notified about this rejection.</p>
    
                                    </div>
                                    <div class="modal-footer">
                                        <form class="form" id="rejectform" method="POST" action="/admin/referals/rejectreceipt">
                                            {{csrf_field()}}
                                            <input type="text" hidden id="reject_id" name="id" value="0"/>
                                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-danger">Reject</button>
                                        </form> 
                                    </div>
                                                                  
                            </div>
                        </div>
                    </div>
               
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   
       <script>
           $(document).ready(function(){
                $('[id*="verifyReferalBtn"]').click(function(){
                    console.log($(this).attr('verifyId'));
                    $('#verify_id').val($(this).attr('verifyId'));
                });
                $('[id*="rejectReferalBtn"]').click(function(){
                    console.log($(this).attr('verifyId'));
                    $('#reject_id').val($(this).attr('verifyId'));
                });
                $('[id*="markAsPaidBtn"]').click(function(){
                    console.log($(this).attr('verifyId'));
                    $('#markAsPaid_id').val($(this).attr('verifyId'));
                });
           });
        </script>

   
@endsection