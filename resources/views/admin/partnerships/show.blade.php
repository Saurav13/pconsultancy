 @extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Message #{{ $contact->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('contact-us-messages.index') }}">See all messages</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                        <ul class="px-0 list-unstyled">
                            <li><strong>Buisness Name      : </strong>{{ $contact->buisness_name }}</li>
                            <li><strong>Locations      : </strong>{!! $contact->locations !!}</li>

                            <li><strong>Contact Name      : </strong>{{ $contact->contact_name }}</li>

                            <li><strong>Contact Position     : </strong>{{ $contact->contact_position }}</li>                                
                            <li><strong>Tel No. : </strong>{{ $contact->tel_no }}</li><br>
                            <li><strong>Mobile No. : </strong>{{ $contact->mobile_no }}</li><br>
                            

                            <li><span class="text-muted">Received Date :</span> {{ date('M j, Y', strtotime($contact->created_at)) }}</li>
                        </ul>
                    </div>
                </div>
                <!--/ Invoice Company Details -->

                <!-- Invoice Footer -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-xs-center">
                                <strong>Partnership Type : </strong>{{ $contact->partner_type }}
                            </div><br>
                            <p class="lead">Agreed Commission Structure:</p>
                            @if($contact->partner_type=='Gym')
                            <p>1 Month membership (per referral): {{ $contact->gym_1 }}</p>
                            <p>6 Month membership (per referral): {{ $contact->gym_2 }}</p>
                            @elseif($contact->partner_type=='Cons')
                            <p>VET course (per referral): {{ $contact->cons_1 }}</p>
                            <p>University/College Course (per referral): {{ $contact->cons_2 }}</p>
                            @elseif($contact->partner_type=='Tut')
                            <p>Tuitions/PTE/IELTS/PY: (per referral): {{ $contact->tut_1 }}</p>
                            @elseif($contact->partner_type=='Real')
                            <p>Percentage (per referral): {{ $contact->real_1 }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <!--/ Invoice Footer -->

            </div>
        </section>
    </div>

@endsection
