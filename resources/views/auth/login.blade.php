@extends('layouts.app')

@section('body')
<style>
    [class*="input-group-"] {
    min-width: 38px;
    background-color: transparent;
    border-color: #ccc;
    transition: border-color .15s ease-in-out 0s;
}
.input-group-prepend {
    margin-right: -1px;
}
.input-group-append, .input-group-prepend {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}
    </style>
<section class="u-bg-overlay g-bg-pos-top-center g-bg-img-hero g-bg-black-opacity-0_3--after g-py-100" style="background-attachment: fixed;  background-repeat: no-repeat; background-image: url(/assets/img/banner-images/login.jpg);">
    <div class="container u-bg-overlay__inner">
      <div class="row justify-content-center text-center mb-5">
       
      </div>

      <div class="row justify-content-center align-items-center no-gutters">
        <div class="col-lg-5 g-rounded-left-5--lg-up" style="background:whitesmoke;">
          <div class="g-pa-50">
            <!-- Form -->
            <form class="g-py-15" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
              <h2 class="h3 g-color-grey mb-4">Login</h2>
              <div class="mb-4">
                <div class="input-group">
                 
                  <input class="form-control g-color-grey g-brd-grey g-bg-transparent g-placeholder-grey g-pl-5 g-pr-15 g-py-13" type="email" name="email" placeholder="Username" value="{{ old('email') }}">
                 
                </div>
                @if ($errors->has('email'))
              
                  <div class="help-block">
                      <strong>{!! $errors->first('email') !!}</strong>
                  </div>
                @endif
              </div>

              <div class="g-mb-40">
                <div class="input-group rounded">
                
                  <input class="form-control g-color-grey g-brd-grey g-bg-transparent g-placeholder-grey g-pl-5 g-pr-15 g-py-13" type="password" name="password" placeholder="Password">
                  
                </div>
                @if ($errors->has('password'))
                
                  <div class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </div>
                @endif
              </div>

              <div class="mb-1">
                <label class="form-check-inline u-check g-color-grey g-font-size-12 g-pl-25 mb-2">
                  <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                  <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                    <i class="fa" data-check-icon=""></i>
                  </div>
                  Keep signed in
                </label>
                
                <span class="pull-right">
                  <a class="d-inline-block g-color-grey g-font-size-12 mb-2" href="{{ route('password.request') }}">Forgot password?</a>
                </span>
              </div>

              <div class="g-mb-45">
                <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13" style="background:orange" type="submit">Login</button>
              </div>

              <div class="row" style="margin: 1rem;">
                  <div class="col"><hr></div>
                  <div class="col-auto"></div>
                  <div class="col"><hr></div>
              </div>
              <a class="btn btn-block u-btn-facebook rounded text-uppercase g-py-13 g-mb-15" href="/signin/facebook" >
                <i class="mr-3 fa fa-facebook"></i>
                <span class="g-hidden-xs-down">Login with</span> Facebook
              </a>
              <a class="btn btn-block u-btn-twitter rounded text-uppercase g-py-13"  style="background:#dd5144" href="/signin/google">
                <i class="mr-3 fa fa-google"></i>
                <span class="g-hidden-xs-down">Login with</span> Google
              </a>
            </form>
            <!-- End Form -->
          </div>
        </div>

        <div class="col-lg-5 g-bg-white g-rounded-right-5--lg-up">
          <div class="g-pa-50">
            <!-- Form -->
            <form class="g-py-15"  method="POST" action="{{ route('register') }}">
              <h2 class="h3 g-color-black mb-4">Signup</h2>
              <p class="mb-4">Please enter your basic information.<br>(Your data is secure with us)</p>
              
              
              {{ csrf_field() }}
              <div class="mb-4">
                <div class="input-group rounded">
                 
                  <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-5 g-pr-15 g-py-13" type="text" placeholder="Your name" name="name" required>
                </div>
               
              </div>

              <div class="mb-4">
                  <div class="input-group rounded">
                   
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-5 g-pr-15 g-py-13" type="test" placeholder="Company(Optional)">
                  </div>
                </div>

              <div class="mb-4">
                <div class="input-group rounded">
                  <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-5 g-pr-15 g-py-13" type="email" placeholder="Your Email" name="email" required >
                </div>
              </div>

              

              <div class="mb-4">
                <div class="input-group rounded">
                  
                  <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-5 g-pr-15 g-py-13" type="password" placeholder="Password" name="password" required>
                </div>
              </div>

              <div class="mb-1">
                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-2">
                  <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                  <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                    <i class="fa" data-check-icon=""></i>
                  </div>
                  I accept the <a href="#!">Terms and Conditions</a>
                </label>
              </div>

              <div class="mb-3" style="opacity:0">
                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-2">
                  <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                  <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                    <i class="fa" data-check-icon=""></i>
                  </div>
                  Subscribe to our newsletter
                </label>
              </div>

              <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13" type="submit">Sign Up</button>
            </form>
            

            @if ($errors->has('email'))
              
              <div class="help-block" style="opacity:0">
                  <strong>{!! $errors->first('email') !!}</strong>
              </div>
            @endif

            @if ($errors->has('password'))
          
              <div class="help-block" style="opacity:0">
                  <strong>{!! $errors->first('password') !!}</strong>
              </div>
            @endif
            <!-- End Form -->
          </div>
        </div>
      </div>
    </div>
  </section>
{{-- <section class="container g-py-30">
    <div class="row justify-content-center">
        <div class="col-sm-8 col-lg-5">
            <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                <header class="text-center mb-4">
                    <h2 class="h2 g-color-black g-font-weight-600">User Login</h2>
                </header>
    
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? 'has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required autofocus>
                        
                        @if ($errors->has('email'))
                            <div class="help-block">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </div>
                        @endif
                    </div>
        
                    <div class="g-mb-35">
                        <div class="row justify-content-between">
                            <div class="col align-self-center">
                                <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Password:</label>
                            </div>
                            <div class="col align-self-center text-right">
                                <a class="d-inline-block g-font-size-12 mb-2" href="{{ route('password.request') }}">Forgot password?</a>
                            </div>
                        </div>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 mb-3 {{ $errors->has('password') ? 'has-error' : '' }}" type="password" placeholder="Password" name="password" required>

                        @if ($errors->has('password'))
                            <div class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                        <div class="row justify-content-between">
                            <div class="col-8 align-self-center">
                                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-0">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </div>
                                    Keep signed in
                                </label>
                            </div>
                            <div class="col-4 align-self-center text-right">
                                <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Login</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- End Form -->
    
                <footer class="text-center">
                    <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Or, Log in with:
                        <a class="g-font-weight-600" href="/signin/facebook" style="font-size: 15px;" title="Facebook"><i class="fa fa-facebook-square"></i></a>
                        <a class="g-font-weight-600" href="/signin/google" style="font-size: 15px;" title="Google"><i class="fa fa-google-plus"></i></a>
                    </p>
                    <hr>
                    <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Don't have an account? <a class="g-font-weight-600" href="{{ route('register') }}">signup</a>
                    </p>
                </footer>
            </div>
        </div>
    </div>
</section> --}}
@endsection
