@extends('layouts.app')

@section('body')
<section class="container g-py-30">
    <div class="row justify-content-center">
        <div class="col-sm-8 col-lg-5">
            <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                <header class="text-center mb-4">
                    <h2 class="h2 g-color-black g-font-weight-600">Enter Your Email</h2>
                </header>
    
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{ route('set_email') }}">
                    {{ csrf_field() }}

                    <div class="photo-container" style="margin-bottom: 10px;    text-align: center;">
                        <img src="{{ $user['photo'] }}" style="height:  123px;width:  123px;"/>
                    </div>

                    <div class="mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? 'has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required autofocus>
                        
                        @if ($errors->has('email'))
                            <div class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                </form>
                <!-- End Form -->
            </div>
        </div>
    </div>
</section>
@endsection
