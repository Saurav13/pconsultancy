@extends('layouts.app')

@section('body')
<section class="container g-py-30">
    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-9 col-lg-6">
            <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
            <header class="text-center mb-4">
                <h2 class="h2 g-color-black g-font-weight-600">User Signup</h2>
            </header>

            <!-- Form -->
            <form class="g-py-15" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="mb-4">
                    <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Name</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="John" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="mb-4">
                    <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? ' has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Password:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('password') ? ' has-error' : '' }}" type="password" placeholder="********" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="row justify-content-between mb-5">
                    {{-- <div class="col-8 align-self-center">
                        <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="accept" {{ old('accept') ? 'checked' : '' }}>
                            <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                <i class="fa" data-check-icon=""></i>
                            </div>
                            I accept the <a href="#!">Terms and Conditions</a>
                        </label>
                        
                        @if ($errors->has('accept'))
                            <div class="help-block">
                                <strong>{{ $errors->first('accept') }}</strong>
                            </div>
                        @endif
                    </div> --}}
                    <div class="col-4 align-self-center text-right">
                        <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Signup</button>
                    </div>
                </div>
            </form>
            <!-- End Form -->

            <footer class="text-center">
                <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Or, Log in with:
                    <a class="g-font-weight-600" href="/signin/facebook" style="font-size: 15px;" title="Facebook"><i class="fa fa-facebook-square"></i></a>
                    <a class="g-font-weight-600" href="/signin/google" style="font-size: 15px;" title="Google"><i class="fa fa-google-plus"></i></a>
                </p>
                <hr>
                <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Already have an account? <a class="g-font-weight-600" href="{{ route('login') }}">signin</a>
                </p>
            </footer>
            </div>
        </div>
    </div>
</section>
@endsection
