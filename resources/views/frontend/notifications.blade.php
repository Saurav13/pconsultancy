@extends('layouts.app')

@section('body')
    <div class="container g-my-20">
        <h3>Notifications: </h3>
            <ul class="list-unstyled">
                @forelse($notifications as $n)
                <li class="g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1">
                    <div class="media">
                   <div class="media-body">
                   <a href="{{$n->link}}">
                   <p class="m-0">{{$n->message}}</p>
                        <span class="g-font-size-12 g-color-gray">{{$n->created_at->diffForHumans()}}</span>
                   </a>
                    </div>
                    </div>
                </li>
                @empty
                <p style="font-weight:600;color:brown;">You currently do not have any notifications</p>
                @endforelse
                <br>
               {{$notifications->links('frontend.partials.pagination')}}
            </ul>
    </div>
@endsection