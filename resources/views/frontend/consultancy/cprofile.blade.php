@extends('layouts.app')

@section('body')
{{-- <div class="g-min-height-400 g-flex-centered g-bg-img-hero g-bg-pos-top-center g-bg-size-cover g-pt-80" style="background-image: url(main-assets/assets/img-temp/1920x800/img2.jpg);">
    <div class="container g-pos-rel g-z-index-1">
      <h1 class="g-color-white g-font-size-60--lg mb-0">Highly Productive Consultants</h1>
    </div>
  </div> --}}
<style>
    .count_box{
        border: 3px solid;
        border-radius: 23px;
    }
    .chart_container{
        padding: 1rem 1rem;
        margin-bottom:1rem;
    }
    
</style>
  <hr style="margin:0rem">
  
<section class="g-py-30" ng-cloak ng-app="consultancyapp" ng-controller="CProfileController" ng-init="dashboardInit()">
    <div class="container">
        <ul class="nav u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-primary g-mb-20" role="tablist" data-target="nav-1-1-default-hor-left-underline" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#overviewTab" role="tab" ng-click="dashboardInit()">Overview</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="myReferalsTabLink" data-toggle="tab" href="#myReferalsTab" role="tab">My Referrals</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#competitorsTab" role="tab" >Insights</a>
            </li>
        </ul>
<!-- End Nav tabs -->

<!-- Tab panes -->
        <div id="nav-1-1-default-hor-left-underline" class="tab-content">
            <div class="tab-pane fade show active" id="overviewTab" role="tabpanel">
                <div class="container">
                    <div class="row">
                      <!-- Profile Sidebar -->
                        
                        <div class="col-lg-3 g-mb-50 g-mb-0--lg" style="border-right: 1px solid #b6b3bd;">
                        <!-- User Image -->   
                            <div class="u-block-hover g-pos-rel" style="margin:2rem;">
                                <figure>
                                    @if(Auth::guard('consultancy_user')->user()->branch->partner->logo == null)
                                    <img class="img-fluid u-block-hover__main--zoom-v1 profile_picture" id="profilePicture" src="/main-assets/assets/img-temp/400x450/img5.jpg" alt="Image Description">
                                    @else
                                    <img class="img-fluid u-block-hover__main--zoom-v1 profile_picture" id="profilePicture" src="/partner_logos/{{Auth::guard('consultancy_user')->user()->branch->partner->logo}}" alt="Image Description">
                                    @endif
                                </figure>
                                <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                                    <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                        <!-- Figure Social Icons -->
                                        <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                        
                                        <li class="list-inline-item align-middle g-mx-7">
                                            <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!" id="uploadProfilePictureTrigger">
                                            <i class="fa fa-edit"></i>
                                            
                                            </a>
                                        </li>
                                        <input type="file" hidden fileread="profilephoto" ng-model="profilephoto" id="uploadProfilePicture" onchange="angular.element(this).scope().uploadProfilePicture()"/>
                        
                                        </ul>
                                        <p class="text-center">
                                            <img hidden id="uploadProfilePictureLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 3rem; min-width:3rem;"/>
                                        </p> 
                                        <!-- End Figure Social Icons -->
                                    </div>
                                </figcaption>
                                
                            </div>
                            <p class="text-center"><strong>{{Auth::guard('consultancy_user')->user()->full_name}}</strong></p>
                            <div class="count_box text-center g-brd-primary g-pa-10">
                                <p style="margin-bottom:0;"><strong>Commission:</strong></p>
                                
                                <div ng-show="user.commission_rate!=null" class="g-font-size-25 g-font-weight-300 g-mb-7">Rs. @{{user.commission_rate}}</div>
                                <p class="g-color-gray-dark-v4 g-mb-5"><input  ng-model="newCommission" type="number" min="0" ng-show="changeCommission" /></p>
                                <p class="g-color-gray-dark-v4 g-mb-5"><button class="btn btn-primary" ng-show="!changeCommission && user.commission_rate==null" ng-click="changeCommission=true;">Set</button></p>
                                
                                <p class="g-color-gray-dark-v4 g-mb-5"><button class="btn btn-primary" ng-show="!changeCommission && user.commission_rate!=null" ng-click="changeCommission=true;">Change</button></p>
                                <p class="g-color-gray-dark-v4 g-mb-5">
                                    <button class="btn btn-success" ng-show="changeCommission"  ng-click="saveCommission()" id="saveCommission">Save <i class="fa fa-check-circle g-mr-5"></i></button>
                                    <button class="btn btn-danger" ng-show="changeCommission" ng-click="changeCommission=false;">Cancel <i class="fa fa-times-circle g-mr-5"></i></button>
                                </p>
                                <label class="u-check g-pl-25">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" ng-checked="user.show_rate=='hide'" ng-click="toggleHideCommissionRate()">
                                    <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                      <i class="fa" data-check-icon=""></i>
                                    </div>
                                    Hide Commission Rate<br> 
                                </label>
                                <br>
                                <img src="/loading.gif" id="hideCommissionRateLoading" hidden style="height:2rem; width:2rem"/>
                                
                                <div id="hideCommissionRateBeforeLoading">
                                <br>
                                </div>
                                
                                
                            </div>
                        </div>
                       
                        <div class="col-lg-9 g-mb-50 g-mb-0--lg" >
                            <p class="text-center">
                                <img id="dashboardMainLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 5rem; width:5rem;"/>
                            </p>
                            <div id="dashboardMainBeforeLoading" hidden>
                                <p><strong>My Referral Stats:</strong></p>
                                <button type="button" class="btn btn-primary pull-right" ng-click="myReferals()">View Referals</button>
                                <p style="font-weight:600;color:brown;" ng-hide="totalReferals>0">You currently do no have any referrals.</p>
                                
                                <div class="chart_container" >
                                    <canvas id="myChart" style="max-height:20rem; max-width:45rem"></canvas>   
                                </div>  
                                <p><strong>My Beneficiaries:</strong></p>
                                <p style="font-weight:600;color:brown;" ng-hide="referalPartners.length>0">You currently do no have beneficiaries.</p>
                                
                                <ul class="row list-unstyled">
                                    <li class="col-md-6 g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1" ng-repeat="user in referalPartners">
                                        <div class="media">
                                            <div class="d-flex g-mt-2 g-mr-15">
                                                <img class="g-width-40 g-height-40 rounded-circle" ng-if="user.photo==null" src="/main-assets/assets/img-temp/100x100/img7.jpg" alt="Default Image">
                                                <img class="g-width-40 g-height-40 rounded-circle" ng-if="user.photo!=null" ng-src="/profile_images/@{{user.photo}}" alt="@{{user.name}}">
                                                
                                            </div>
                                            <div class="media-body">
                                            <p class="m-0"><strong>@{{user.name}}</strong></p>
                                                <span class="g-font-size-12 g-color-gray">(@{{user.count+' refers'}})</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                                                                           <br>
                                                                                           <br>
                                                                                           <br>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade " id="myReferalsTab" role="tabpanel" ng-init="myReferalsInit({{json_encode($referals->items())}})" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <select class="custom-select mb-3 " ng-change="filterByStatus()" ng-model="status_filter" id="inlineFormCustomSelectPref">
                                <option selected="" value="" disabled>Filter By Status</option>
                                <option value="all">All</option>                                
                                <option value="submitted">Submitted</option>
                                <option value="seen">Seen</option>
                                <option value="rejected">Rejected</option>
                                <option value="in-progress">In-Progress</option>
                                <option value="success">Success</option>
                                <option value="paid">Paid</option>
                                
                                
                                
                            </select>
                            <img hidden id="filterByStatusLoading"  src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                               
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="text" class="form-control rounded-0 form-control-md" ng-model="searchKey" placeholder="Search by name">
                                <div class="input-group-append">
                                    <button class="btn btn-md u-btn-cyan rounded-0" id="searchReferal" type="button" ng-click="search()" style="height:3rem">Go <i class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <p style="font-weight:600;color:brown;" ng-hide="referals.length>0">No referral found.</p>
                        
                    </div>
                    <hr style="margin:1rem;">
                    <div class="row">
                        <div class="col-lg-4 g-mb-30" ng-repeat="refers in referals">
                            <!-- Figure -->
                            <div class="card">
                                <figure class="u-shadow-v19 g-bg-white g-rounded-4 g-pa-20">
                                    <div class="d-flex justify-content-start">
                                    <!-- Figure Image -->
                                        <img class="g-width-90 g-height-90 rounded-circle g-mr-15" ng-if="refers.photo==null" src="/main-assets/assets/img-temp/125x125/img1.jpg" alt="Image Description">
                                        <img class="g-width-90 g-height-90 rounded-circle g-mr-15" ng-if="refers.photo!=null" ng-src="/referal_photos/@{{refers.photo}}" alt="Image Description">
                                    <!-- Figure Image -->
                            
                                        <div class="d-block">
                                            <!-- Figure Info -->
                                            <div class="g-mb-5">
                                            <h4 class="h5 g-mb-1">@{{refers.name}}</h4>
                                            </div>
                                            <!-- End Figure Info -->
                                        <a href="{{URL::to('/consultancy/cprofile/referal?referal_id=')}}@{{refers.id}}" class="btn btn-primary btn-sm">View Details</a>
                                        </div>
                                    </div>
                            
                                    <hr class="g-brd-gray-light-v4 g-my-5">
                            
                                    <div class="g-py-10">
                                    <!-- Figure Contacts -->
                                    <ul class="list-unstyled g-color-gray-dark-v5 g-font-size-13 g-mb-0">
                                        <li class="g-mb-10">
                                        <i class="g-pos-rel g-mt-1 mr-2 icon-communication-062 u-line-icon-pro"></i>
                                        <a href="#!">@{{refers.email}}</a>
                                        </li>
                                        <li>
                                        <i class="g-pos-rel g-top-1 mr-2 icon-electronics-005 u-line-icon-pro"></i>
                                        <em class="g-font-style-normal">@{{refers.phone}}</em>
                                        </li>
                                        <li>
                                            <br>
                                        <span>Status: &nbsp;</span> <span class="u-label g-rounded-20 g-px-15 g-mr-10 g-mb-15 @{{getStatusColor(refers.status)}}">@{{refers.status}}</span> 
                                        </li>
                                    </ul>
                                    <!-- End Figure Contacts -->
                                    </div>
                            
                                    <hr class="g-brd-gray-light-v4 g-my-5">
                            
                                    <!-- Figure Title -->
                                    <div class="d-flex justify-content-between align-items-center mb-4">
                                        <h4 class="h6 mb-0">Refered By:</h4>
                                    </div>
                                    <!-- End Figure Title -->
                            
                                    <div class="d-flex justify-content-start">
                                    <!-- Figure Image -->
                                        <img class="g-width-50 g-height-50 rounded-circle g-mr-15" ng-if="refers.user.photo==null" src="/main-assets/assets/img-temp/100x100/img11.jpg" alt="Image Description">
                                        <img class="g-width-50 g-height-50 rounded-circle g-mr-15" ng-if="refers.user.photo!=null" ng-src="/profile_images/@{{refers.user.photo}}" alt="@{{refers.user.name}}">
                                    
                                        <!-- Figure Image -->
                            
                                        <div class="d-block">
                                            <!-- Figure Info -->
                                            <div class="g-mb-5">
                                            <h3 class="h6 g-font-weight-600 mb-1">
                                                <a class="g-color-black g-color-primary--hover" href="#!">@{{refers.user.name}}</a>
                                            </h3>
                                            <em class="g-color-gray-dark-v4 g-font-style-normal g-font-size-12">Date: </em>
                                            <a class="g-font-size-12" href="#!">@{{refers.submitted_at.replace(' ','T') | date: 'MMM d, y h:mm a'}} </a>
                                            </div>
                                            <!-- End Figure Info -->
                                        </div>
                                    </div>
                                </figure>
                            </div>
                            <!-- End Figure -->
                        </div>
                    </div>
                <div id="pagination">{{$referals->links('frontend.partials.pagination')}}</div>
                </div>
                    <!-- End Team Block -->
            </div>

            <div class="tab-pane fade" id="competitorsTab" role="tabpanel" ng-init="competitorsInit({{json_encode($competitors)}})" >
                <p class="text-center">
                    <img id="competitorsMainLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 5rem; width:5rem;"/>
                </p>
                <div class="row" id="competitorsMainBeforeLoading" hidden>
                    @foreach($competitors as $c)
                    <div class="col-md-4">
                        <p><strong>{{$c['name']}}:</strong></p>
                        <div class="chart_container" >
                            <canvas id="competitor{{$c['id']}}" style="max-height:100rem" width="100%"></canvas>   
                        </div> 
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div id="errorBox" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="text-center">
            <h4><strong>Something went wrong.</strong></h4>
            <p class="description" style="color:red;" id="errorMessage"></p>
            <p class="description" style="color:red;" id="errorDetails"></p>
                <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
    <div id="successModal" class="text-center g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        <div class="icon-box">
            <i class="fa fa-check" style="font-size:57px"></i>
        </div>
        <div class="container">
            <div class="text-center">
            <h4><strong id="successMessage">Success!!!</strong></h4>
               <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
</section>
               <br><br><br> <br><br><br> <br><br><br> <br><br><br>
@endsection


@section('js')
    <script src="/consultancyjs/cprofile.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    
    <script>
        $(document).ready(function(){
            
        });
    </script>
@endsection