@extends('layouts.app')

@section('body')

<style>
    .profile_picture{
        border-radius: 50%;
        height: 10rem;
        width: 10rem;
        margin: 1rem 4rem;
    }
    .profile_container{
        background: #f1f1f1;
        padding: 2rem;
    }
    .view_referal_photo{
        height: 8rem;
        margin: 1rem 2rem;
    }
    .icon-box{
        color: #1fca2e;
        border-radius: 50%;
        z-index: 9;
        border: 5px solid #fff;
        padding: 15px;
    }
    .disabled {
    cursor: not-allowed;
    }
    .sp-list-group-item{
        color:#28a745;
    }
    .sp-list-group-item.active {
        z-index: 2;
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }

</style>

<hr style="margin:0rem">
<section class="g-mb-100 g-mt-20" ng-app="profileapp" ng-cloak  ng-controller="ProfileController">
    <div class="container">
      <div class="row">
        <!-- Profile Sidebar -->
        <div class="col-lg-3 g-mb-50 g-mb-0--lg">
          <!-- User Image -->   
          <div class="u-block-hover g-pos-rel">
            <figure>
                <img class="img-fluid u-block-hover__main--zoom-v1 profile_picture" id="profilePicture" src="/partner_logos/{{ Auth::guard('consultancy_user')->user()->branch->partner->logo }}" alt="Image Description">

            </figure>
          </div>
          
          <p class="text-center"><b>{{ Auth::guard('consultancy_user')->user()->branch->full_name }}</b></p>
          <!-- User Image -->


        </div>
        <!-- End Profile Sidebar -->

        <!-- Profile Content -->
        <div class="col-lg-9">
            <div id="editProfileTab" class="" role="" ng-init="editProfileInit({{ Auth::guard('consultancy_user')->user() }})">
               
                <ul class="nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20" role="tablist" data-target="nav-1-1-default-hor-left-underline" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20">
                    
                    <li class="nav-item">
                        <a class="nav-link g-py-10 active" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--2" role="tab" aria-selected="false">Security Settings</a>
                    </li>
                </ul>

                <div id="nav-1-1-default-hor-left-underline" class="tab-content">
                
                    <!-- Security Settings -->
                    <div class="tab-pane fade active show" id="nav-1-1-default-hor-left-underline--2" role="tabpanel">
                    <h2 class="h4 g-font-weight-300">Security Settings</h2>
                    <p class="g-mb-25">Reset your password</p>
        
                    <form ng-submit="changePassword()">
                        <p style="text-align:  center;color: red;" ng-show="error.general.length > 0">@{{ error.general }}</p>
                        <p style="text-align:  center;color: green;" id="passwordChangeSuccess" hidden>Password was Successfully Changed.</p>
                        <!-- Current Password -->
                        <div class="form-group row g-mb-25">
                          <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Current password</label>
                          <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                              <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.oldPassword" placeholder="Current password">
                              <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                              </div>
                            </div>
                            <p style="text-align:  center;color: red;" ng-show="error.oldPassword.length > 0">@{{ error.oldPassword }}</p>
                          </div>
                        </div>
                        <!-- End Current Password -->
        
                        <!-- New Password -->
                        <div class="form-group row g-mb-25">
                          <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">New password</label>
                          <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                              <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.password" placeholder="New password">
                              <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                              </div>
                            </div>
                            <p style="text-align:  center;color: red;" ng-show="error.password.length > 0">@{{ error.password }}</p>
                          </div>
                        </div>
                        <!-- End New Password -->
        
                        <!-- Verify Password -->
                        <div class="form-group row g-mb-25">
                          <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Verify password</label>
                          <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                              <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.password_confirmation" placeholder="Verify password">
                              <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Verify Password -->
        
                        <hr class="g-brd-gray-light-v4 g-my-25">
        
                        <div class="text-sm-right">
                          <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#!" ng-click="resetForm()">Cancel</a>
                          <button type="submit" class="btn u-btn-primary rounded-0 g-py-12 g-px-25" id="submitchangebtn">Save Changes</button>
                        </div>
                    </form>
                    </div>
                    <!-- End Security Settings -->
        
                
                    <!-- Notification Settings -->
                    <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--4" role="tabpanel">
                    <h2 class="h4 g-font-weight-300">Manage your Notifications</h2>
                    <p class="g-mb-25">Below are the notifications you may manage.</p>
        
                    <form class="ng-pristine ng-valid">
                        <!-- Email Notification -->
                        <div class="form-group">
                        <label class="d-flex align-items-center justify-content-between">
                            <span>Email notification</span>
                            <div class="u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="emailNotification" type="checkbox" checked="">
                            <div class="u-check-icon-radio-v7">
                                <i class="d-inline-block"></i>
                            </div>
                            </div>
                        </label>
                        </div>
                        <!-- End Email Notification -->
        
                        <hr class="g-brd-gray-light-v4 g-my-20">
        
                        <!-- Comments Notification -->
                        <div class="form-group">
                        <label class="d-flex align-items-center justify-content-between">
                            <span>Send me email notification when a user comments on my blog</span>
                            <div class="u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="commentNotification" type="checkbox">
                            <div class="u-check-icon-radio-v7">
                                <i class="d-inline-block"></i>
                            </div>
                            </div>
                        </label>
                        </div>
                        <!-- End Comments Notification -->
        
                        <hr class="g-brd-gray-light-v4 g-my-20">
        
                        <!-- Update Notification -->
                        <div class="form-group">
                        <label class="d-flex align-items-center justify-content-between">
                            <span>Send me email notification for the latest update</span>
                            <div class="u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="updateNotification" type="checkbox" checked="">
                            <div class="u-check-icon-radio-v7">
                                <i class="d-inline-block"></i>
                            </div>
                            </div>
                        </label>
                        </div>
                        <!-- End Update Notification -->
        
                        <hr class="g-brd-gray-light-v4 g-my-25">
        
                        <!-- Message Notification -->
                        <div class="form-group">
                        <label class="d-flex align-items-center justify-content-between">
                            <span>Send me email notification when a user sends me message</span>
                            <div class="u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="messageNotification" type="checkbox" checked="">
                            <div class="u-check-icon-radio-v7">
                                <i class="d-inline-block"></i>
                            </div>
                            </div>
                        </label>
                        </div>
                        <!-- End Message Notification -->
        
                        <hr class="g-brd-gray-light-v4 g-my-25">
        
                        <!-- Newsletter Notification -->
                        <div class="form-group">
                        <label class="d-flex align-items-center justify-content-between">
                            <span>Receive our monthly newsletter</span>
                            <div class="u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="newsletterNotification" type="checkbox">
                            <div class="u-check-icon-radio-v7">
                                <i class="d-inline-block"></i>
                            </div>
                            </div>
                        </label>
                        </div>
                        <!-- End Newsletter Notification -->
        
                        <hr class="g-brd-gray-light-v4 g-my-25">
        
                        <div class="text-sm-right">
                        <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#!">Cancel</a>
                        <a class="btn u-btn-primary rounded-0 g-py-12 g-px-25" href="#!">Save Changes</a>
                        </div>
                    </form>
                    </div>
                    <!-- End Notification Settings -->
                </div>
               
            </div>
        </div>
        <!-- End Profile Content -->
      </div>
    </div>
    

    <div id="errorBox" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="text-center">
            <h4><strong>Something went wrong.</strong></h4>
            <p class="description" style="color:red;" id="errorMessage"></p>
            <p class="description" style="color:red;" id="errorDetails"></p>
                <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
    <div id="successModal" class="text-center g-bg-white g-overflow-y-auto g-pa-20" style="display: none; width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        <div class="icon-box">
            <i class="fa fa-check" style="font-size:57px"></i>
        </div>
        <div class="container">
            <div class="text-center">
            <h4><strong id="successMessage">Success!!!</strong></h4>
               <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
</section>

@stop

@section('js')
    <script src="/consultancyjs/editprofile.js"></script>
@stop