@extends('layouts.app')

@section('body')
<section class="container g-py-30">
    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-9 col-lg-6">
            <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                <header class="text-center mb-4">
                    <h2 class="h2 g-color-black g-font-weight-600">Partner Reset Password</h2>
                </header>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{ route('consultancy_password.email') }}">
                    {{ csrf_field() }}

                    <div class="mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 {{ $errors->has('email') ? ' has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        </div>

                    <div class="mb-5">
                        <div class="align-self-center text-center">
                            <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Send Password Reset Link</button>
                        </div>
                    </div>
                </form>
                <!-- End Form -->
            </div>
        </div>
    </div>
</section>
@endsection
