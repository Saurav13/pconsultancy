@extends('layouts.app')

@section('body')
<section class="container g-py-30">
    <div class="row justify-content-center">
        <div class="col-sm-8 col-lg-5">
            <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                <header class="text-center mb-4">
                <h2 class="h2 g-color-black g-font-weight-600">Partner Login</h2>
                </header>
    
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{ route('consultancy_login') }}">
                    {{ csrf_field() }}
                    <div class="mb-4">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Email:</label>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15  {{ $errors->has('email') ? 'has-error' : '' }}" type="email" placeholder="johndoe@gmail.com" name="email" value="{{ old('email') }}" required autofocus>
                        
                        @if ($errors->has('email'))
                            <div class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
        
                    <div class="g-mb-35">
                        <div class="row justify-content-between">
                            <div class="col align-self-center">
                                <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Password:</label>
                            </div>
                            <div class="col align-self-center text-right">
                                <a class="d-inline-block g-font-size-12 mb-2" href="{{ route('consultancy_password.request') }}">Forgot password?</a>
                            </div>
                        </div>
                        <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 mb-3 {{ $errors->has('password') ? 'has-error' : '' }}" type="password" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <div class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif

                        <div class="row justify-content-between">
                            <div class="col-8 align-self-center">
                                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-0">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </div>
                                    Keep signed in
                                </label>
                            </div>
                            <div class="col-4 align-self-center text-right">
                                <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Login</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- End Form -->
    
                <footer class="text-center">
                    <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Don't have an account? <a class="g-font-weight-600" href="/become-a-partner">Know how to get one.</a>
                    </p>
                </footer>
            </div>
        </div>
    </div>
</section>
@endsection
