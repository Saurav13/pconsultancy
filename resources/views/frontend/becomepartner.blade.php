@extends('layouts.app')

@section('body')
<div class="g-brd-y g-brd-gray-light-v4">
        <div class="container g-pt-30 g-pb-100">
            <div class="g-mb-60">
                <h2 class="h1 mb-3">Become a partner?</h2>
                <p class="g-line-height-2">Please fill up the following form.<br>A member of our staff will contact you soon.</p>
            </div>

            <form method="POST" action="/partnership-submission">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12 g-mb-35">
                                <label class="g-color-main g-font-weight-500 mb-3">Name of Buisness</label>
                                <input name="buisness_name" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="Buisness Name">
                            </div>
                            <div class="col-sm-12 g-mb-35">

                                <div class="mb-4">
                                    <label class="g-color-main g-font-weight-500 mb-3">Location/branch</label>
                                    <textarea name="locations" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-secondary rounded g-py-13 g-px-15" rows="8" placeholder="Give us location and details of branch"></textarea>
                                </div>
                            </div>
                            <label class="g-color-main g-font-weight-500 mb-3">Contact Person Details</label>
                            
                            <div class="col-sm-12 g-mb-35">
                                <label  class="g-color-main g-font-weight-500 mb-3">Name</label>
                                <input name="contact_name" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="Contact person name">
                            </div>
                             <div class="col-sm-12 g-mb-35">
                                <label class="g-color-main g-font-weight-500 mb-3">Position</label>
                                <input name="contact_position" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="Position">
                            </div>
                            <div class="col-sm-6 g-mb-35">
                                <label class="g-color-main g-font-weight-500 mb-3">Tel No:</label>
                                <input name="tel_no" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="tel" placeholder="+ (01) 222 33 44">
                            </div>
                            <div class="col-sm-6 g-mb-35">
                                <label class="g-color-main g-font-weight-500 mb-3">Mobile No:</label>
                                <input name="mobile_no"class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="tel" placeholder="+ (01) 222 33 44">
                            </div>
                            <label class="g-color-main g-font-weight-500 mb-3">Agreed Commission structure</label>
                            <div class="col-sm-12 g-mb-35">
                                <select class="form-control" name="partner_type" id="selectType">
                                    <option value="Gym">Gym</option>
                                    <option value="Cons">Consultancy</option>
                                    <option value="Tut">Tuitions/PTE/IELTS/PY</option>
                                    <option value="Real">Real State</option>
                                    
                                </select>
                            </div>
                            <div class="col-sm-12">

                                <div id="gymstructure">
                                    <div class="col-md-12 g-mb-35">
                                        <label class="g-color-main g-font-weight-500 mb-3">1 Month membership (per referral)</label>
                                        <input name="gym_1" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                    <div class="col-md-12 g-mb-35" >
                                        <label class="g-color-main g-font-weight-500 mb-3">6 Month membership (per referral)</label>
                                        <input name="gym_2" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">

                                <div id="consultancystructure" hidden>
                                    <div class="col-md-12 g-mb-35">
                                        <label class="g-color-main g-font-weight-500 mb-3">VET course (per referral)</label>
                                        <input name="cons_1" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                    <div class="col-md-12 g-mb-35">
                                        <label class="g-color-main g-font-weight-500 mb-3">University/College Course (per referral)</label>
                                        <input name="cons_2" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                </div>
                            </div>
                      
                            <div class="col-sm-12">
                                <div class="col-sm-12 g-mb-35">
                                <div id="tutionstructure" hidden>
                                    <div class="col-md-12 g-mb-35">
                                        <label class="g-color-main g-font-weight-500 mb-3">  Tuitions/PTE/IELTS/PY:  (per referral)</label>
                                        <input name="tut_1" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div id="realstatestructure" hidden>
                                    <div class="col-md-12 g-mb-35">
                                        <label class="g-color-main g-font-weight-500 mb-3">  Percentage  (per referral)</label>
                                        <input name="real_1" class="form-control g-color-main g-brd-gray-light-v4 g-brd-primary--focus g-bg-white rounded g-py-13 g-px-15" type="text" placeholder="johndoe@gmail.com">
                                    </div>
                                    
                                </div>
                            </div>
                            


                           
                        </div>
                    </div>

                    <div class="col-md-12">
                    

                        <div class="text-right">
                            <button class="btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase rounded-3 g-py-12 g-px-35" type="submit" role="button">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-dismissible fade show g-bg-teal g-color-white rounded-0" role="alert" style="position:fixed;bottom:5px; right:5px;">
        <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>

        <div class="media">
        <span class="d-flex g-mr-10 g-mt-5">
            <i class="icon-check g-font-size-25"></i>
        </span>
        <span class="media-body align-self-center">
           {{Session::get('message')}}
        </span>
        </div>
    </div>
    @endif
@endsection


@section('js')
    <script>
        $(document).ready(function(){
            var hideall=function(){
                $('#gymstructure').attr('hidden','true');
                $('#consultancystructure').attr('hidden','true');
                $('#tutionstructure').attr('hidden','true');
                $('#realstatestructure').attr('hidden','true');

            }
            $('#selectType').on('change',function(){
                console.log('asd')
                if($('#selectType').val()=='Gym')
                {
                    hideall();
                    $('#gymstructure').removeAttr('hidden');
                }
                if($('#selectType').val()=='Cons')
                {
                    hideall();
                    $('#consultancystructure').removeAttr('hidden');
                }if($('#selectType').val()=='Tut')
                {
                    hideall();
                    $('#tutionstructure').removeAttr('hidden');
                }if($('#selectType').val()=='Real')
                {
                    hideall();
                    $('#realstatestructure').removeAttr('hidden');
                }
            })
        })
    </script>
@endsection