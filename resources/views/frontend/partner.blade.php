@extends('layouts.app')

@section('body')
<section class="g-mt-40">
    <div class="row justify-content-center g-mb-70">
        <div class="col-lg-9">
            <!-- Heading -->
            <div class="text-center">
                @if($partner->logo)
                <img style="height:6rem; margin-bottom:3rem" src="/partner_logos/{{$partner->logo}}" alt="Image Description">
                @else
                <img style="height:6rem; margin-bottom:3rem" src="/main-assets/assets/img-temp/200x100/img1.png" alt="Image Description">                    
                @endif
                <h2 class="h1 g-color-black g-font-weight-700 text-uppercase mb-4">{{$partner->name}}</h2>
                <div class="d-inline-block g-width-70 g-height-2 g-bg-black mb-4"></div>
                <p class="g-font-size-18 mb-0">{{$partner->description}}
                </p>
            <br>
                <br>
                <p class="g-font-size-22 mb-0">We are present at different locations:</p>
                <br>
                <ul class="nav justify-content-center u-nav-v1-1 u-nav-primary u-nav-rounded-3 g-mb-20" role="tablist" data-target="nav-1-1-accordion-primary-hor-center" data-tabs-mobile-type="accordion" data-btn-classes="btn btn-md btn-block u-btn-outline-primary g-mb-20">
                    @foreach($partner->branches as $branch)
                        <li class="nav-item">
                          <a class="nav-link {{$loop->index==0?'active':''}}" data-toggle="tab" href="#branchTab{{$branch->id}}" role="tab">{{$branch->name}}</a>
                        </li>
                    @endforeach
                    
                </ul>
                      <!-- End Nav tabs -->
                      
                      <!-- Tab panes -->

                <div id="nav-1-1-accordion-primary-hor-center" class="tab-content">
                    @foreach($partner->branches as $branch)
                        <div class="tab-pane fade show {{ $loop->iteration == 1 ? 'active' : '' }}" id="branchTab{{$branch->id}}" role="tabpanel">
                            <div class="row no-gutters">
                                <div class="col-lg-7">
                                    <iframe width="100%"  height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?q={{$branch->partner->name}}&ll={{$branch->lat}},{{$branch->long}}&t=&z=17&ie=UTF8&iwloc=&output=embed"></iframe>
                                    
                                </div>
                        
                                <div class="col-lg-5 g-bg-white" style="border: 2px solid #2285aa;">
                                    <div class="g-pa-50 g-pa-70--md">
                                        <h2 class="h2 g-color-black">{{$branch->name}}</h2>
                            
                                        <!-- Contact Details -->
                                        <ul class="list-unstyled g-color-black g-font-weight-300 g-font-size-14 mb-0">
                                            <li class="g-my-30">
                                                <div class="media">
                                                    <i class=" g-color-black g-font-size-23 mt-2 mr-4 icon-hotel-restaurant-235 u-line-icon-pro"></i>
                                                    <div class="media-body g-mt-8">
                                                    {{$branch->location}}
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="g-my-30">
                                                <div class="media">
                                                    <i class="g-color-black g-font-size-23 mt-2 mr-4 icon-communication-062 u-line-icon-pro"></i>
                                                    <div class="media-body g-mt-8">
                                                    {{$branch->contact_email}}
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="g-my-30">
                                                <div class="media">
                                                    <i class="g-color-black g-font-size-23 mt-2 mr-4 icon-communication-033 u-line-icon-pro"></i>
                                                    <div class="media-body g-mt-8">
                                                    {{$branch->contact_number}}
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media">
                                                    <i class="d-flex g-color-black g-font-size-20 mt-1 mr-4 icon-education-097 u-line-icon-pro"></i>
                                                    <div class="media-body">
                                                    <h2 class="g-color-black g-font-size-default g-font-weight-600 text-uppercase mb-3">Office hours:</h2>
                                                    <p>{{$branch->office_hours}}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- End Contact Details -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End Heading -->
        </div>
    </div>
</section>
@endsection