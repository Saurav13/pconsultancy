<style>
    .profile_picture_mini{
        border-radius: 50%;
        height: 2.5rem;
        width: 2.5rem;
        margin-top: -0.5rem;
    }

    html, body {
      height: 100%;
    }

    #wrap {
      min-height: 100%;
      margin-bottom: -50px;
    }

    #main {
      overflow:auto;
      margin-bottom: -50px;
    }

    .footer {
      position: relative;
      /* margin-top: -150px; 
      height: 150px; */
      /* clear:both; */
      /* padding-top:20px; */
    } 
       
    body:before {
        content: "";
        height: 100%;
        float: left;
        width: 0;
        /* margin-top: -32767px; */
        /* thank you Erik J - negate effect of float*/
      } 

  </style>
  
<body id="home-section">
  
    <div id="wrap">
    <main id="main">
      <header id="js-header" class="u-header u-header--static--lg u-header--show-hide--lg u-header--change-appearance--lg" data-header-fix-moment="500" data-header-fix-effect="slide">
        <div class="u-header__section u-header__section--light g-transition-0_3 g-bg-white g-py-3" data-header-fix-moment-exclude="g-bg-white g-py-10" data-header-fix-moment-classes="g-bg-white-opacity-0_7 u-shadow-v18 g-py-0">
          <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
            <div class="container">
              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                <span class="hamburger hamburger--slider">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
                </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
              <!-- Logo -->
                @if(Auth::guard('consultancy_user')->check())
                  <a href="/consultancy/cprofile" class="navbar-brand">
              @else
                    <a href="/" class="navbar-brand">

                @endif
                <img src="/assets/img/logo.png" style="height:2rem;" alt="Image Description">
              </a>
              <!-- End Logo -->
  
              <!-- Navigation -->
              <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10" id="navBar">
                <ul class="navbar-nav ml-auto text-uppercase g-font-weight-600 u-main-nav-v5 u-sub-menu-v1">
                  <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                    @if(Auth::guard('consultancy_user')->check())
                    <a href="/consultancy/cprofile" class="nav-link">Dashboard
                  
                    </a>
                    @elseif(Auth::check())
                    <a href="/profile" class="nav-link">Home
                  
                    </a>
                    @else
                    <a href="/" class="nav-link">Home
                  
                    </a>
                    @endif
                  </li>
                  <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                    <a href="/how-it-works" class="nav-link">How it works
                  
                    </a>
                  
                  </li>
                  <li class="nav-item g-mx-20--lg g-mb-5 g-mb-0--lg">
                      <a href="/become-a-partner" class="nav-link">Become a partner</a>
                  </li>
                  @if(Auth::check() || Auth::guard('consultancy_user')->check() )

                     

                      <li class="nav-item g-ml-15--lg dropdown">
                          <a href="#!" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            @if(Auth::check() && Auth::user()->photo!=null)  
                          <img class="img-fluid profile_picture_mini"  src="/profile_images/{{Auth::user()->photo}}" alt="Image Description">
                            @elseif(Auth::guard('consultancy_user')->check()&& Auth::guard('consultancy_user')->user()->branch->partner->logo!=null)
                          <img class="img-fluid profile_picture_mini"  src="/partner_logos/{{Auth::guard('consultancy_user')->user()->branch->partner->logo}}" alt="Image Description">
                            @else
                            <img class="img-fluid profile_picture_mini"  src="/main-assets/assets/img-temp/400x450/img5.jpg" alt="Image Description">
                            @endif                                                        
                          </a>
                          {{-- <a href="#" class="dropdown-toggle nav-link p-0" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a> --}}

                          <ul class="dropdown-menu list-unstyled g-text-transform-none g-brd-top g-brd-top-2 g-mt-7 g-mt-7--lg--scrolling animated" style="padding: 0.5rem 1rem; min-width:10rem;">
                            <li class="dropdown-item">
                              <a class="nav-link g-px-0" style="color:#555555" href="/{{Auth::guard('consultancy_user')->check()? 'consultancy/cprofile':'profile'}}">{{Auth::guard('consultancy_user')->check()? 'Dashboard':'My Profile'}}</a>
                            </li>
                            
                            <li class="dropdown-item">
                              <a class="nav-link g-px-0" style="color:#555555" href="/{{ Auth::guard('consultancy_user')->check() ? 'consultancy' : 'user' }}/editprofile">Edit Profile</a>
                            </li>
                            <li class="dropdown-item">
                                  <a class="nav-link g-px-0" style="color:#555555"  href="{{Auth::guard('consultancy_user')->check()? route('consultancy_logout'): route('logout') }}"
                                      onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                      Logout
                                  </a>

                                  <form id="logout-form" action="{{Auth::guard('consultancy_user')->check() ? route('consultancy_logout'):route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                  </form>
                              </li>
                          </ul>
                      </li>
                      <li class="nav-item g-ml-15--lg">
                          <a href="/notifications"><i class="fa fa-bell" style="padding-top: 6px;    padding-right: 8px;font-size: 15px;"></i>
                          @if(Auth::check())
                            @if(Auth::user()->notifications->count()>0)
                              <span style="    position: absolute; top: -24px; right: 13px;color: red; font-size: 37px;">.</span>
                            @endif
                          @endif
                          @if(Auth::guard('consultancy_user')->check())
                            @if(Auth::guard('consultancy_user')->user()->branch->notifications->count()>0)
                              <span style="    position: absolute; top: -24px; right: 13px;color: red; font-size: 37px;">.</span>
                            @endif
                          @endif
                          </a>
                      </li>
                  @endif

                </ul>
              </div>
              <!-- End Navigation -->
            </div>
          </nav>
        </div>
      </header>