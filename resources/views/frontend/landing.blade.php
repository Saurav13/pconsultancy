<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Get Linked</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/logo_ico.png">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans%7CMerriweather%7COpen+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-line-pro/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/animate.css">
  <link rel="stylesheet" href="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-globals.css">
    <!-- CSS Template -->
    <link rel="stylesheet" href="/assets/css/styles.op-consulting.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/layers.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/navigation.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/main-assets/assets/css/custom.css">
  </head>
<style>
  .has-error{
        border: 1px solid #DA4453 !important;
      }
  </style>
  <body id="home-section">
    <main>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance u-header--change-logo"
              data-header-fix-moment="0">
        <div class="u-header__section g-bg-black-opacity-0_2 g-transition-0_3 g-py-7 g-py-10--md"
             data-header-fix-moment-exclude="g-bg-black-opacity-0_5 g-py-10--md"
             data-header-fix-moment-classes="u-shadow-v19 g-bg-white g-py-10--md">
          <nav class="navbar navbar-expand-lg g-py-0">
            <div class="container g-pos-rel">
              <!-- Logo -->
              <a href="/" style="padding-top:1.2rem;" class="js-go-to navbar-brand u-header__logo"
                 data-type="static">
                <img class="u-header__logo-img u-header__logo-img--main g-width-90" src="/assets/img/logo.png" style="height:2rem;" alt="Image description">
                <img class="u-header__logo-img g-width-90" src="/assets/img/logo-dark.png" alt="Image description" style="height:2rem;">
              </a>
              <!-- End Logo -->

              <!-- Navigation -->
              <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar">
                <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-letter-spacing-1 g-font-size-12 g-pt-20 g-pt-0--lg ml-auto">
                  <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg">
                    <a href="/" class="nav-link p-0">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="/how-it-works" class="nav-link p-0">How it works</a>
                  </li>
                  
                  <li class="nav-item g-ml-15--lg">
                    <a href="/become-a-partner" class="nav-link p-0">Become a partner</a>
                  </li>
                  @if(!Auth::guard('consultancy_user')->check() && !Auth::check())
                      <li class="nav-item g-ml-15--lg"><a class="nav-link p-0" href="{{ route('login') }}">Sign In</a></li>
                      
                  @elseif(Auth::check())
                      <li class="nav-item g-ml-15--lg dropdown">
                          <a href="#" class="dropdown-toggle nav-link p-0" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <ul class="dropdown-menu list-unstyled g-text-transform-none g-brd-top g-brd-top-2 g-min-width-200 g-mt-7 g-mt-7--lg--scrolling animated">
                            <li class="dropdown-item">
                              <a class="nav-link g-px-0" style="color:#555555" href="/profile">My Profile</a>
                            </li> 
                            <li class="dropdown-item">
                                  <a class="nav-link g-px-0" style="color:#555555"  href="{{ route('logout') }}"
                                      onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                      Logout
                                  </a>

                                  <form id="logout-form" action="{{route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                  </form>
                              </li>
                          </ul>
                      </li>
                  @endif
                  @if(Auth::check() || Auth::guard('consultancy_user')->check() )

                  <li class="nav-item g-ml-15--lg">
                      <a href="/notifications"><i class="fa fa-bell" style="font-size: 15px;"></i></a>
                      @if(Auth::check())
                        @if(Auth::user()->notifications->count()>0);
                          <span style="    position: absolute; top: -24px; right: 7px;color: red; font-size: 37px;">.</span>
                        @endif
                      @endif
                      @if(Auth::guard('consultancy_user')->check())
                        @if(Auth::guard('consultancy_user')->user()->branch->notifications->count()>0);
                          <span style="    position: absolute; top: -24px; right: 7px;color: red; font-size: 37px;">.</span>
                        @endif
                      @endif
                    </li>
                  @endif
                </ul>
                
                @if(!Auth::guard('consultancy_user')->check() && !Auth::check())
                  <div class="d-inline-block g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg g-ml-10">
                    <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="{{ route('consultancy_login') }}" ><b>Partner Area</b></a>
                  </div>
                @elseif(Auth::guard('consultancy_user')->check())
                  <div class="nav-item g-ml-15--lg dropdown">
                    <a id="nav-link--cousuntancydrop" class="dropdown-toggle g-font-size-12 text-uppercase nav-link g-py-7 g-px-0" data-toggle="dropdown" href="#!" aria-haspopup="true" aria-expanded="false" >
                        {{Auth::guard('consultancy_user')->user()->consultancy->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu list-unstyled g-text-transform-none g-brd-top g-brd-top-2 g-min-width-200 g-mt-7 g-mt-7--lg--scrolling animated" >
                      <li class="dropdown-item">
                        <a class="nav-link g-px-0" style="color:#555555" href="/consultancy/cprofile">Dashboard</a>
                      </li>  
                      <li class="dropdown-item">
                            <a class="nav-link g-px-0" style="color:#555555" href="{{ route('consultancy_logout') }}"
                                onclick="event.preventDefault();
                                        document.getElementById('logout-form-consultancy').submit();">
                                Logout
                            </a>

                            <form id="logout-form-consultancy" action="{{ route('consultancy_logout')}}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                  </div>
                
                @endif
              </div>
              <!-- End Navigation -->

              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
            </div>
          </nav>
        </div>
      </header>
      <!-- End Header -->


      <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options="{direction: ;reverse;, settings_mode_oneelement_max_offset: ;150;}">
        <div class="divimage dzsparallaxer--target w-100 g-bg-cover g-bg-black-opacity-0_7--after" style="height: 120%; background-attachment: fixed; background-repeat: no-repeat;
        background-size: cover; background-image: url(/assets/img/banner-images/banner.jpg); transform: translate3d(0px, -112.858px, 0px);"></div>
  
        <div class="container g-pt-100">
          <div class="row" style="      margin-top: 1rem;  display: flex;justify-content: center;">
            <div class="col-md-6 col-lg-6 g-mb-100">
              <!-- Content Info -->
              <div>
                {{-- <h1 class="g-color-white g-font-weight-600 g-font-size-50 mb-3"> <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>Get Linked <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i></h1> --}}
                <div class="g-mt-40 mb-5">
                  <h2 class="mb-4 g-color-white">What is Get Linked?</h2>
                  <p class="g-line-height-2 g-color-white" style="    font-size: 17px; text-align: justify;">Get Linked Solutions is a referral application, both mobile and web with its sole purpose to bring businesses closer to their customer through the power of internet and network. There has been a lot of commission up for grabs in a modern business model with society being ever more sophisticated and tailored to the advantage of the customer. Get Linked Solutions is a platform which offers the referral commission firsthand forming a win-win situation to the business, which spends a large portion of their budget on marketing and the customers who always trust the business they have been recommended by their close somebody. 
                  </p>
                </div>
                {{-- <ul class="row list-inline g-pb-10 mb-0">
                    <li class="col-md-4 list-inline-item g-mx-0">
                      <!-- Icon Blocks -->
                      <div class="u-block-hover g-px-10">
                        <div class="g-mb-5">
                          <span  style="color:white;" class="u-icon-v1 g-width-85 g-height-55 g-font-size-40 rounded-circle">
                            <i class="icon-education-024 u-line-icon-pro"></i>
                          </span>
                        </div>
                        <h3  style="font-size: 23px; color: white;" class="g-font-weight-600 mb-3">
                          <span class="g-font-weight-700" >01.</span>
                          Sign Up
                        </h3>
                        <p class="g-color-white">Sign up with basic information.</p>
                      </div>
                      <!-- End Icon Blocks -->
                    </li>
        
                    <li class="col-md-4 list-inline-item g-mx-0">
                      <!-- Icon Blocks -->
                      <div class="u-block-hover g-px-10">
                        <div class="g-mb-5">
                          <span style="color: white" class="u-icon-v1 g-width-85 g-height-55 g-font-size-40 rounded-circle">
                            <i class="icon-education-073 u-line-icon-pro"></i>
                          </span>
                        </div>
                        <h3 style="font-size: 23px; color: white" class="g-font-weight-600 mb-3">
                          <span class="g-font-weight-700">02.</span>
                         Refer
                        </h3>
                        <p class="g-color-white">Refer your friends/family if they require consultation to study abroad.</p>
                      </div>
                      <!-- End Icon Blocks -->
                    </li>
        
                    <li class="col-md-4 list-inline-item g-mx-0">
                      <!-- Icon Blocks -->
                      <div class="u-block-hover g-px-10">
                        <div class="g-mb-5">
                          <span style="color: white;" class="u-icon-v1 g-width-85 g-height-55 g-font-size-40 rounded-circle">
                            <i class="icon-communication-180 u-line-icon-pro"></i>
                          </span>
                        </div>
                        <h3 style="font-size: 23px; color: white;" class="g-font-weight-600 mb-3">
                          <span class="g-font-weight-700">03.</span>
                          Earn
                        </h3>
                        <p class="g-color-white">Earn a commission of Rs.10,000 if your referral agrees to apply their visa process from one of our partnered consultancies</p>
                      </div>
                      <!-- End Icon Blocks -->
                    </li>
                </ul> --}}
              </div>
              <a class="btn u-btn-primary g-font-weight-500 g-font-size-12 text-uppercase g-px-25 g-py-13 mr-3" href="/how-it-works">
                Know More..
                <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
              </a>
              <!-- End Content Info -->
            </div>
  
          <div class="col-md-6 col-lg-6 g-mb-100" style="display:flex; justify-content:center;opacity:{{Auth::guest() && Auth::guard('consultancy_user')->guest()?'1':'0'}}">
              <!-- Join Form -->
              <form class="g-bg-white rounded g-px-30 g-py-40" style="min-width:70%"  method="POST" action="{{ route('register') }}">
                  {{ csrf_field() }}
                <div class="text-center">
                  <h2 class="h3 g-font-weight-600 g-mb-35">Become our member!!</h2>
                </div>
  
                <input class="form-control rounded g-px-20 g-py-12 mb-3 {{ $errors->has('name') ? ' has-error' : '' }}" type="text" name="name" placeholder="Your Name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
                <input class="form-control rounded g-px-20 g-py-12 mb-3" type="email" placeholder="Your Email" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
                <input class="form-control rounded g-px-20 g-py-12 mb-3 {{ $errors->has('password') ? ' has-error' : '' }}" type="password" placeholder="Your Password" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
  
                <div class="g-mb-35">
                  <button class="btn btn-block u-btn-primary g-font-weight-500 g-font-size-12 text-uppercase g-px-25 g-py-13" type="submit">Sign Up</button>
                </div>
                <div class="g-mb-35">
                  <a class="btn btn-block u-btn-primary g-font-weight-500 g-font-size-12 text-uppercase g-px-25 g-py-13" href="/user/login" style="background:#e79948">Sign In</a>
                </div>
  
                <div class="text-center mb-3">
                  <h3 class="h5">Join with</h3>
                </div>
  
                
                  <div class="row no-gutters">
                    <div class="col">
                      <a class="btn btn-block u-btn-facebook g-font-weight-500 g-font-size-12 text-uppercase g-rounded-left-3 g-rounded-right-0 g-px-25 g-py-13" href="/signin/facebook">
                        <i class="mr-2 fa fa-facebook"></i>
                        Facebook
                      </a>
                    </div>
                    <div class="col">
                      <a class="btn btn-block u-btn-twitter g-font-weight-500 g-font-size-12 text-uppercase g-rounded-left-0 g-rounded-right-3 g-px-25 g-py-13" style="background:#dd5144" href="/signin/google">
                        <i class="mr-2 fa fa-google"></i>
                        Google
                      </a>
                    </div>
                  </div>
                </form>
                <!-- End Join Form -->
              </div>
          </div>
        </div>
      </section>
    
   
      <div id="about-section" class="g-overflow-hidden">
        <div class="container g-pt-40">
          <div class="g-max-width-550 text-center mx-auto g-mb-40">
            <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-23 g-letter-spacing-2 mb-4">Referrals till now</h1>
          </div>
          <div class="g-mb-40">
            <h6 class="text-uppercase g-font-size-12 g-font-weight-600 g-letter-spacing-0_5 g-pos-rel g-z-index-2">Total Referals</h6>

            <div class="js-hr-progress-bar progress g-height-20 rounded-0 g-overflow-visible g-mb-20">
              <div class="js-hr-progress-bar-indicator progress-bar g-pos-rel" role="progressbar" style="width: 99%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                <div class="text-center u-progress__pointer-v1 g-font-size-11 g-color-white g-bg-primary g-rounded-50x">100%</div>
              </div>
            </div>

            <h6 class="text-uppercase g-font-size-12 g-font-weight-600 g-letter-spacing-0_5 g-pos-rel g-z-index-2">In-Progress</h6>

            <div class="js-hr-progress-bar progress g-height-20 rounded-0 g-overflow-visible g-mb-20">
            <div class="js-hr-progress-bar-indicator progress-bar g-pos-rel bg-warning" role="progressbar" style="width: {{$inProgressPercent}}%;" aria-valuenow="{{$inProgressPercent}}" aria-valuemin="0" aria-valuemax="100">
                <div class="text-center u-progress__pointer-v1 g-font-size-11 g-color-white bg-warning g-rounded-50x">{{$inProgressPercent}}%</div>
              </div>
            </div>
            
            <h6 class="text-uppercase g-font-size-12 g-font-weight-600 g-letter-spacing-0_5 g-pos-rel g-z-index-2">Success</h6>

            <div class="js-hr-progress-bar progress g-height-20 rounded-0 g-overflow-visible g-mb-20">
              <div class="js-hr-progress-bar-indicator progress-bar g-pos-rel bg-success" role="progressbar" style="width: {{$successPercent}}%;" aria-valuenow="{{$successPercent}}" aria-valuemin="0" aria-valuemax="100">
                <div class="text-center u-progress__pointer-v1 g-font-size-11 g-color-white bg-success g-rounded-50x">{{$successPercent}}%</div>
              </div>
            </div>
            
          </div>
          
          @if($consultancies->count()>0)
          <div class="g-pt-50 g-pb-70">
            <div class="g-overflow-hidden">
              <div class="g-max-width-550 text-center mx-auto g-mb-20">
                <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-23 g-letter-spacing-2 mb-4">Our Partners</h1>
              </div>
              <div class="row text-center mx-0 g-ml-minus-1 g-mb-minus-1">
                  @foreach($consultancies as $c)
                <div class="col-sm-3 col-md-2 px-0">
                  <div class="g-brd-left g-brd-bottom g-brd-gray-light-v4 g-py-30 g-px-15">
                  <a href="/partner/{{$c->alias}}">
                      @if($c->logo)
                      <img class="img-fluid g-width-100 g-opacity-0_8--hover g-transition-0_2" src="/partner_logos/{{$c->logo}}" alt="Image Description">
                      @else
                      <img class="img-fluid g-width-100 g-opacity-0_8--hover g-transition-0_2" src="/main-assets/assets/img-temp/200x100/img1.png" alt="Image Description">                    
                      @endif
                    </a>
                  </div>
                </div>
                @endforeach
              
              </div>
            </div>
            <!-- Vision & Mission -->
            {{-- <div class="g-pos-rel">
              <div class="row justify-content-between">
                <div class="col-lg-5 g-mb-30">
                  <div class="g-mt-20 mb-5">
                    <h2 class="mb-4">What is Get Linked?</h2>
                    <p class="g-line-height-2">Get Linked Solutions is a referral application, both mobile and web with its sole purpose to bring businesses closer to their customer through the power of internet and network. There has been a lot of commission up for grabs in a modern business model with society being ever more sophisticated and tailored to the advantage of the customer. Get Linked Solutions is a platform which offers the referral commission firsthand forming a win-win situation to the business, which spends a large portion of their budget on marketing and the customers who always trust the business they have been recommended by their close somebody. 
                    </p>
                  </div>

                  <!-- Icon Block -->
                  <div class="media mb-4">
                    <div class="d-flex mr-4">
                      <span class="u-icon-v3 g-width-50 g-height-50 g-color-bluegray g-bg-bluegray-opacity-0_1 g-font-size-16 rounded-circle">
                        <i class="icon-communication-114 u-line-icon-pro"></i>
                      </span>
                    </div>
                    <div class="media-body">
                      <span class="g-font-weight-700">31,500+</span>
                      <p class="g-text">Happy Partners.</p>
                    </div>
                  </div>
                  <!-- End Icon Block -->

                
                  <!-- Icon Block -->
                  <div class="media mb-4">
                    <div class="d-flex mr-4">
                      <span class="u-icon-v3 g-width-50 g-height-50 g-color-red g-bg-red-opacity-0_1 g-font-size-16 rounded-circle">
                        <i class="icon-communication-116 u-line-icon-pro"></i>
                      </span>
                    </div>
                    <div class="media-body">
                      <span class="g-font-weight-700">4+</span>
                      <p class="g-text">Partner Consultancies.</p>
                    </div>
                  </div>
                  <!-- End Icon Block -->
                </div>
              </div>

              <div class="col-lg-6 g-pos-abs--lg g-top-0--lg g-right-0--lg g-mb-30">
                  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                  
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                      </ol>
                      <div class="carousel-inner">
                        <div class="carousel-item">
                          <img class="d-block w-100" src="/assets/img/1.png" alt="First slide">
                          <div class="text-center d-md-block">
                            <h5 class="font-weight-light" style="color: #F59E46;">Sign Up</h5>
                          </div>
                        </div>
                        <div class="carousel-item active">
                          <img class="d-block w-100" src="/assets/img/3.png" alt="Second slide">
                          <div class="text-center d-md-block">
                            <h5 class="ont-weight-light" style="color: #4FA5BC;">Refer</h5>
                          </div>
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="/assets/img/2.png" alt="Third slide">
                          <div class="text-center d-md-block">
                            <h5 class="ont-weight-light">Earn</h5>
                          </div>
                        </div>
      
                      </div>
                      
                    </div>
              </div>
            </div> --}}
            <!-- End Vision & Mission -->
          </div>
          @endif
        </div>
      </div>
      {{-- <h1 class="text-uppercase text-center g-color-main-light-v1 g-font-weight-600 g-font-size-23 g-letter-spacing-2 mb-4">Our Partner Consultancies</h1>
      
    
      <div id="carousel5" class="js-carousel js-carousel_single-item-thumbs5__thumbs text-center mx-auto g-max-width-645 g-mb-35" data-infinite="true" data-slides-show="3" data-center-mode="true" data-nav-for="#carousel6" data-responsive='[{
        "breakpoint": 768,
        "settings": {
          "slidesToShow": 3
        }
      }, {
        "breakpoint": 576,
        "settings": {
          "slidesToShow": 1
        }
      }]'>
      @foreach($consultancies as $c)
      <div class="js-thumb g-px-15--sm">
        @if($c->logo)
        <img class="rounded-circle" style="height:10rem; width:10rem;" src="/partner_logos/{{$c->logo}}" alt="Image Description">
        @else
        <img class="rounded-circle" style="height:10rem; width:10rem;" src="/main-assets/assets/img-temp/100x100/img6.jpg" alt="Image Description">
        @endif
      </div>
      @endforeach
    </div>
 <div id="carousel6" class="js-carousel js-carousel_single-item-thumbs5__slides text-center mx-auto g-max-width-645 g-pb-50" data-infinite="true" data-fade="true" data-animation="linear" data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-0" data-nav-for="#carousel5">
      @foreach($consultancies as $c) 
    <div class="js-slide">
      <h4 class="h4 text-uppercase g-font-weight-600er g-mb-15">
        {{$c->name}}
      </h4>
      <blockquote class="lead g-mb-0">{{$c->description}}</blockquote>
    </div>
      @endforeach

  </div> --}}

  <!-- Contact -->
  <footer id="contact-section" class="g-pos-rel">
    <!-- Content -->
    {{-- <div class="g-bg-size-cover g-bg-img-hero u-bg-overlay g-bg-black-opacity-0_7--after g-pt-120 g-pb-70" style="background-image: url(/assets/img/banner-images/contact.jpg);">
      <div class="container u-bg-overlay__inner">
        <div class="row align-items-center">
          <div class="col-md-4 g-mb-50">
            <h3 class="h4 g-color-white mb-4">Contact Info</h3>

            <!-- Icon Block -->
            <div class="media align-items-center mb-4">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-hotel-restaurant-235 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">5B Streat, City 50987 New Town US</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="media align-items-center mb-4">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-communication-033 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">+32 (0) 333 444 555</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="media align-items-center g-mb-60">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-communication-062 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">htmlstream@support.com</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Social Icons -->
            <h3 class="h4 g-color-white">Social Networks</h3>

            <ul class="list-inline mb-0">
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://www.facebook.com/htmlstream">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://twitter.com/htmlstream">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://github.com/htmlstream">
                  <i class="fa fa-github"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://dribbble.com/htmlstream">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
            <!-- End Social Icons -->
          </div>

          <div class="col-md-8 g-mb-50">
            <div class="g-brd-around g-brd-white-opacity-0_6 g-px-45 g-py-60">
              <div class="row">
                <div class="col-md-4 g-mb-30">
                  <h2 class="h1 g-color-white">Contact Us</h2>
                </div>

                <div class="col-md-8 g-mb-30">
                  <!-- Contact Form -->
                  <form method="POST" action="/contactus" >
                    {{csrf_field()}}
                    <div class="mb-3">
                      <input required name="name" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 rounded-0 g-py-13 g-px-0 mb-2" type="text" placeholder="Name">
                    </div>

                    <div class="mb-3">
                      <input required name="email" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 rounded-0 g-py-13 g-px-0 mb-2" type="email" placeholder="Email">
                    </div>

                    <div class="mb-4">
                      <textarea required name="message" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 g-resize-none rounded-0 g-py-13 g-px-0 mb-5" rows="5" placeholder="Message"></textarea>
                    </div>

                    <button class="btn u-btn-primary g-bg-secondary g-color-primary g-color-white--hover g-bg-primary--hover g-font-weight-600 g-font-size-12 g-rounded-30 g-py-15 g-px-35" type="submit" role="button">Send Message</button>
                  </form>
                  
                  <!-- End Contact Form -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Content -->
    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="position: fixed;right: 2rem; bottom: 2px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <h4 class="h5">
                        <i class="fa fa-check-circle-o"></i>
                        Thank you for your message!
                      </h4>
                    {{Session::get('success')}}
                  </div>
    @endif
    @if (count($errors)>0 )
    <div class="alert alert-danger alert-dismissible fade show" role="alert"  style="position: fixed;right: 2rem; bottom: 2px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="h5">
          <i class="fa fa-minus-circle"></i>
          Oh snap!
        </h4>
            @foreach ($errors->all() as $error)
              <p>{{ $error }}</p>
            @endforeach
      
    </div>
    @endif --}}
    <!-- Go To Top -->
    <a class="js-go-to text-center g-color-main g-color-primary--hover g-left-50x g-ml-minus-100" href="#!"
       data-type="absolute"
       data-position='{
         "bottom": 65
       }'
       data-offset-top="400"
       data-compensation="#js-header"
       data-show-effect="fadeInUp">
      <svg version="1.1" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="208px" height="50px">
        <path fill-rule="evenodd" clip-rule="evenodd" fill="#fff" d="M111.042,0h-0.085C81.962,0.042,50.96,42.999,6,42.999c-6,0-6,0-6,0v1h214v-1v-0.015C169.917,42.349,139.492,0.042,111.042,0z"/>
      </svg>
      <i class="g-font-size-20 g-pos-abs g-bottom-20 g-left-50x g-ml-2 fa fa-angle-double-up"></i>
    </a>
    <!-- End Go To Top -->

    <!-- Copyright -->
    <div class="container text-center g-py-30">
      <p class="g-font-size-13 mb-0">&#169; 2019 Get Linked. Powered by <strong><a href="http://incubeweb.com/" target="_blank">Incube</a></strong></p>
    </div>
    <!-- End Copyright -->
  </footer>
  <!-- End Contact -->
</main>

<!-- JS Global Compulsory -->
<script src="/main-assets/assets/vendor/jquery/jquery.min.js"></script>
<script src="/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/main-assets/assets/vendor/popper.min.js"></script>
<script src="/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/main-assets/assets/vendor/appear.js"></script>
<script src="/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>


<!-- JS Unify -->

<script src="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

<script src="/main-assets/assets/js/hs.core.js"></script>
<script src="/main-assets/assets/js/components/hs.header.js"></script>
<script src="/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
<script src="/main-assets/assets/js/components/hs.scroll-nav.js"></script>
<script src="/main-assets/assets/js/components/hs.counter.js"></script>
<script src="/main-assets/assets/js/components/hs.carousel.js"></script>
<script src="/main-assets/assets/js/components/hs.cubeportfolio.js"></script>
<script src="/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/main-assets/assets/js/components/hs.go-to.js"></script>
<script src="/main-assets/assets/js/components/hs.scrollbar.js"></script>
<script src="/main-assets/assets/js/components/hs.select.js"></script>
<script  src="/main-assets/assets/js/components/hs.progress-bar.js"></script>

<!-- JS Customization -->
<script src="/main-assets/assets/js/custom.js"></script>
<script>
    $('.carousel').carousel({
      interval: 3000
    })
</script>
<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of carousel
    $('#carousel5').on('click', '.js-thumb', function (e) {
      e.stopPropagation();

      var i = $(this).data('slick-index');

      if ($('#carousel5').slick('slickCurrentSlide') !== i) {
        $('#carousel5').slick('slickGoTo', i);
      }
    });

    // initialization of carousel
    $.HSCore.components.HSCarousel.init('.js-carousel');

    // initialization of header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // initialization of go to section
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of counters
    var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');

    // initialization of popups
    $.HSCore.components.HSPopup.init('.js-fancybox');
    var horizontalProgressBars = $.HSCore.components.HSProgressBar.init('.js-hr-progress-bar', {
      direction: 'horizontal',
      indicatorSelector: '.js-hr-progress-bar-indicator'
    });
  });

  $(window).on('load', function() {
    // initialization of HSScrollNav
    $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
      duration: 700
    });

    // initialization of cubeportfolio
    $.HSCore.components.HSCubeportfolio.init('.cbp');
  });

</script>
</body>
</html>