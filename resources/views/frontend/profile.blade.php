@extends('layouts.app')

@section('body')

<style>
.profile_picture{
    border-radius: 50%;
    height: 10rem;
    width: 10rem;
    margin: 1rem 4rem;
}
.profile_container{
    background: #f1f1f1;
    padding: 2rem;
}
.view_referal_photo{
    height: 8rem;
    margin: 1rem 2rem;
}
.icon-box{
    color: #1fca2e;
    border-radius: 50%;
    z-index: 9;
    border: 5px solid #fff;
    padding: 15px;
}
.disabled {
  cursor: not-allowed;
}

.sp-list-group-item{
    color:#28a745;
}
.sp-list-group-item.active {
    z-index: 2;
    color: #fff !important;
    background-color: #2385aa;
    border-color: #2385aa;
}

</style>
<hr style="margin:0rem">
<section class="g-mb-100 g-mt-20" ng-app="profileapp" ng-cloak ng-controller="ProfileController" ng-init="init({{$categories}});getReferals();">
    <div class="container">
      <div class="row">
        <!-- Profile Sidebar -->
        <div class="col-lg-3 g-mb-50 g-mb-0--lg">
          <!-- User Image -->   
          <div class="u-block-hover g-pos-rel">
            <figure>
                @if(Auth::user()->photo == null)
              <img class="img-fluid u-block-hover__main--zoom-v1 profile_picture" id="profilePicture" src="/main-assets/assets/img-temp/400x450/img5.jpg" alt="Image Description">
                @else
            <img class="img-fluid u-block-hover__main--zoom-v1 profile_picture" id="profilePicture" ng-src="/profile_images/{{Auth::user()->photo}}" alt="Image Description">
                @endif
            </figure>

            <!-- Figure Caption -->
            <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
              <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                <!-- Figure Social Icons -->
                <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                 
                  <li class="list-inline-item align-middle g-mx-7">
                    <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!" id="uploadProfilePictureTrigger">
                      <i class="fa fa-edit"></i>
                      
                    </a>
                  </li>
                  <input type="file" hidden fileread="profilephoto" ng-model="profilephoto" id="uploadProfilePicture" onchange="angular.element(this).scope().uploadProfilePicture()"/>

                </ul>
                <p class="text-center">
                    <img hidden id="uploadProfilePictureLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                </p> 
                <!-- End Figure Social Icons -->
              </div>
            </figcaption>
            <!-- End Figure Caption -->
          </div>
          <!-- User Image -->

          <!-- Sidebar Navigation -->
          <div class="list-group list-group-border-0 g-mb-40">
          

            <!-- Profile -->
            {{-- <a id="editProfileTabLink" href="#editProfileTab" data-toggle="tab" role="tab" class="list-group-item list-group-item-action justify-content-between">
              <span><i class="icon-cursor g-pos-rel g-top-1 g-mr-8"></i>Edit Profile</span>
            </a> --}}
            <!-- End Profile -->

            <!-- Users Contacts -->
            <a id="myReferalTabLink" ng-click="getReferals()" href="#myReferalsTab" data-toggle="tab" role="tab" class="list-group-item list-group-item-action justify-content-between active">
              <span><i class="icon-notebook g-pos-rel g-top-1 g-mr-8"></i> My Referrals</span>
            </a>
            <!-- End Users Contacts -->

            <a id="myCommissionsTabLink" ng-click="getCommissions()" href="#myCommissionsTab" data-toggle="tab" role="tab" class="list-group-item list-group-item-action justify-content-between">
                <span><i class="icon-notebook g-pos-rel g-top-1 g-mr-8"></i> My Commissions</span>
            </a>
          

            <!-- Reviews -->
            <a id="referNowTabLink" href="#referNowTab" data-toggle="tab" role="tab" style="color:#28a745" class="list-group-item sp-list-group-item list-group-item-action justify-content-between">
              <strong><span><i class="icon-heart g-pos-rel g-top-1 g-mr-8"></i> Refer Now !!!</span></strong>
            </a>
            <!-- End Reviews -->

          
          </div>
          <!-- End Sidebar Navigation -->

        </div>
        <!-- End Profile Sidebar -->

        <!-- Profile Content -->
        <div class="col-lg-9 tab-content">
            <div id="myReferalsTab" class="tab-pane show active" role="tabpanel">
                <button class="btn btn-primary pull-right" ng-click="referPerson()">Refer Now !!!</button>
               
                <h4><b>My Referrals:</b></h4>
                <br>
                <p class="text-center">
                    <img id="myReferalsLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 5rem; width:5rem;"/>
                </p>
                <div hidden id="myReferalsBeforeLoading">
                    <p style="font-weight:600;color:brown;" ng-hide="referals.length>0">You currently do not have referrals.</p>

                    <div class="row">
                        <div class="col-md-4 g-mb-30" ng-repeat="refers in referals">
                            <!-- Figure -->
                            <figure class="u-info-v7-1 g-brd-around g-brd-gray-light-v4 g-bg-white g-rounded-4 text-center">
                            <div class="g-py-20 g-px-5">
                                <!-- Figure Image -->
                                
                                <img class="g-width-100 g-height-100 rounded-circle g-mb-20" ng-if="refers.photo==null" src="/main-assets/assets/img-temp/125x125/img1.jpg" alt="Image Description">
                                <img class="g-width-100 g-height-100 rounded-circle g-mb-20" ng-if="refers.photo!=null" ng-src="/referal_photos/@{{refers.photo}}" alt="@{{refers.photo}}">
                                
                                <!-- Figure Image -->
                        
                                <!-- Figure Info -->
                            <h4 class="h5 mb-4">@{{refers.name}}</h4>
                                <div class="d-block">
                                    <span class="u-info-v7-1__item-child-v2 g-rounded-25 g-py-5 g-px-20 g-mr-3  g-color-white @{{getStatusColor(refers.status)}}" >
                                        
                                        @{{refers.status}}
                                    </span>
                                </div>
                                <!-- End Figure Info -->
                            </div>
                        
                            <hr class="g-brd-gray-light-v4 g-my-0">
                        
                            <!-- Figure List -->
                            <ul class="row list-inline g-py-5 g-ma-0">
                                <li class="col g-brd-right g-brd-gray-light-v4">
                                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-cyan--hover" href="#!" ng-click="viewReferal(refers.id)">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </li>
                                <li class="col g-brd-right g-brd-gray-light-v4">
                                <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-yellow--hover @{{refers.status!='unfinished'?'disabled':''}}" href="#!" ng-click="editReferal(refers.id,refers.status!='unfinished'?false:true)">
                                    <i class="fa fa-edit"></i>
                                    </a>
                                </li>
                                <li class="col">
                                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-red--hover" href="#!" data-toggle="tooltip"  ng-click="deleteReferalConfirmation(refers.id)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- End Figure List -->
                            </figure>
                            <!-- End Figure -->
                        </div>
                        
                    </div>
                    <div id="referalPagination">
                    </div>
                </div>
                
            </div>
            <div id="myCommissionsTab" class="tab-pane fade" role="tabpanel">
                <span class="pull-right" style="font-size: 18px;">Total: Rs. @{{paidReferals!=null?paidReferals.length*10000:0}}</span>
               
                <h4><b>Commission Earned:</b></h4>
                <br>
                <p class="text-center">
                    <img id="myCommissionsLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 5rem; width:5rem;"/>
                </p>
                <div hidden  id="myCommissionsBeforeLoading">
                    <p style="font-weight:600;color:brown;" ng-hide="paidReferals.length>0">You currently havent earned commission. Refer more to increase chance of earning.</p>
                    
                    <div class="row" >
                        <div class="col-md-4 g-mb-30" ng-repeat="refers in paidReferals">
                            <!-- Figure -->
                            <figure class="u-info-v7-1 g-brd-around g-brd-gray-light-v4 g-bg-white g-rounded-4 text-center">
                            <div class="g-py-20 g-px-5">
                                <!-- Figure Image -->
                                
                                <img class="g-width-100 g-height-100 rounded-circle g-mb-20" ng-if="refers.photo==null" src="/main-assets/assets/img-temp/125x125/img1.jpg" alt="Image Description">
                                <img class="g-width-100 g-height-100 rounded-circle g-mb-20" ng-if="refers.photo!=null" ng-src="/referal_photos/@{{refers.photo}}" alt="@{{refers.photo}}">
                            
                                <!-- Figure Image -->
                        
                                <!-- Figure Info -->
                            <h4 class="h5 mb-4">@{{refers.name}}</h4>
                                <div class="d-block">
                                    <span class="u-info-v7-1__item-child-v2 g-rounded-25 g-py-5 g-px-20 g-mr-3  g-color-white @{{getStatusColor(refers.status)}}" >
                                        
                                        @{{refers.status}}
                                    </span>
                                </div>
                                <!-- End Figure Info -->
                            </div>
                        
                            <hr class="g-brd-gray-light-v4 g-my-0">
                        
                            <!-- Figure List -->
                            <ul class="row list-inline g-py-5 g-ma-0">
                                <li class="col g-brd-right g-brd-gray-light-v4">
                                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-cyan--hover" href="#!" ng-click="viewReferal(refers.id)">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </li>
                                <li class="col g-brd-right g-brd-gray-light-v4">
                                <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-yellow--hover @{{refers.status!='unfinished'?'disabled':''}}" href="#!" ng-click="editReferal(refers.id,refers.status!='unfinished'?false:true)">
                                    <i class="fa fa-edit"></i>
                                    </a>
                                </li>
                                <li class="col">
                                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-bg-transparent g-color-red--hover" href="#!" data-toggle="tooltip"  ng-click="deleteReferalConfirmation(refers.id)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- End Figure List -->
                            </figure>
                            <!-- End Figure -->
                        </div>
                        
                    </div>
                    <div id="commissionPagination">
                    </div>
                </div>
            </div>
            <div id="referNowTab" class="tab-pane fade" role="tabpanel">
                <h4><b>Referral Application:</b></h4>
                <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                    <!-- Text Input -->
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-mb-10" for="hf-email">Full Name</label>
                    
                        <div class="col-sm-9">
                            <input  class="form-control u-form-control rounded-0" ng-model="referalApplication.name" type="text" placeholder="Enter full name of a person">
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                        
                        </div>
                      
                    </div>
                    <!-- End Text Input -->
                      <!-- End Text Input -->
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Contact Number</label>
                    
                        <div class="col-sm-9">
                            <input class="form-control u-form-control rounded-0" ng-model="referalApplication.phone" name="phone" type="text" placeholder="Enter contact number">
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                        </div>
                        
                    </div>
                    
                    <!-- Text Input -->
                      
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Photo</label>
                    
                        <div class="col-sm-9">

                                <div class="input-group-append">
                           
                                    <input type="file" name="photo" fileread="referalApplication.photo" ng-model="referalApplication.photo">
                                </div>
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                        </div>
                        
                    </div>
                     <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Email Address </label>
                    
                        <div class="col-sm-9">
                            <input  class="form-control u-form-control rounded-0" ng-model="referalApplication.email" name="email" type="email" placeholder="Enter email address">
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                        </div>
                        
                    </div>
                    <div class="form-group row g-mb-30">
                    <label class="col-sm-3 g-mb-10" for="inputGroup2_1">Refer To </label>
                        <div class="col-sm-9">
                            <select class="custom-select mb-3" name="visa_type" ng-model="selectedCategory">
                                   
                            <option ng-repeat="c in categories" value="@{{c.id}}">@{{c.title}}</option>
                                  
                            </select>
                           
                        </div>
                    </div>
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-mb-10">Refer To</label><br>
                        
                    
                        <div class="col-sm-9">
                            {{-- <select class="custom-select mb-3" name="consultancy" ng-model="referalApplication.consultancy" id="inlineFormCustomSelectPref">
                                <option selected="">Select Your Consultancy..</option>
                                @foreach($consultancies as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                              </select> --}}
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                            
                            <div class="btn-group justified-content"  ng-show="selectedCategory!=''">
                                {{-- @foreach($categories as $c) --}}
                                <div ng-repeat="p in partners | filter: {category_id:selectedCategory}">
                                    <label class="u-check" title="@{{p.name}}">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" value="@{{p.id}}" ng-model="np.np" name="partner" type="radio" >
                                        <span class="btn btn-md btn-block g-font-size-13 u-btn-outline-lightgray g-color-white--checked g-bg-primary--checked rounded-0">
                                            <img src="/partner_logos/@{{p.logo}}" style="height:2rem;" alt="logo">
                                            <br>
                                            @{{p.name.length>16?p.name.substr(0,14)+'..':p.name}}
                                            <br>  
                                            {{-- <div ng-show="p.show_rate=='show'">
                                                @{{p.commission_rate!=null?'Rs. '+p.commission_rate : '-'}}
                                        
                                            </div> --}}
                                        </span>
                                       
                                    <span></span>
                                
                                    </label>
                                </div>
                                {{-- @endforeach --}}
                             
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group row g-mb-25"  >
                        <label class="col-sm-3 col-form-label g-mb-10">Refer To<br>(commission)  </label><br>
                        
                    
                        <div class="col-sm-9">
                            {{-- <select class="custom-select mb-3" name="consultancy" ng-model="referalApplication.consultancy" id="inlineFormCustomSelectPref">
                                <option selected="">Select Your Consultancy..</option>
                                @foreach($consultancies as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                              </select> --}}
                            {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                            
                            <div class="btn-group justified-content"  >
                                {{-- @foreach($categories as $c) --}}
                                <div ng-repeat="p in branches | filter: {partner_id:np.np}">
                                    <label class="u-check" title="@{{p.name}}">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" value="@{{p.id}}" ng-model="referalApplication.consultancy" name="branch" type="radio" @{{$index==0?'checked=""':''}} >
                                        <span class="btn btn-md btn-block g-font-size-13 u-btn-outline-lightgray g-color-white--checked g-bg-primary--checked rounded-0">
                                       
                                            {{-- <img src="/partner_logos/@{{p.logo}}" style="height:2rem;" alt="logo"> --}}
                                            <br>
                                            @{{p.name.length>16?p.name.substr(0,14)+'..':p.name}}
                                            <br>  
                                            <div ng-show="p.show_rate=='show'">
                                                @{{p.commission_rate!=null?'Rs. '+p.commission_rate : '-'}}
                                        
                                            </div>
                                        </span>
                                       
                                    <span></span>
                                
                                    </label>
                                </div>
                                    
                                {{-- @endforeach --}}
                             
                            </div>
                        </div>
                        
                    </div>
                    <button type="button" ng-click="referalSave()" class="btn btn-warning" style="color:white" id="saveReferal">Save changes <i class="fa fa-check-circle g-mr-5"></i></button>
                    <button tyle="button" ng-click="referalSubmit()" class="btn btn-success" id="submitReferal">Refer Now <i class="fa fa-check-circle g-mr-5"></i></button>
                    
                </form>
                        <!-- End General Forms -->
              
            </div>

            <div id="settingsTab" class="tab-pane fade" role="tabpanel">
                <h4><b>Settings</b></h4>
                
                

                <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                    <!-- Current Password -->
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Current password</label>
                        <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                            <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="Current password">
                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Current Password -->

                    <!-- New Password -->
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">New password</label>
                        <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                            <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="New password">
                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- End New Password -->

                    <!-- Verify Password -->
                    <div class="form-group row g-mb-25">
                        <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Verify password</label>
                        <div class="col-sm-9">
                            <div class="input-group g-brd-primary--focus">
                            <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="Verify password">
                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                <i class="icon-lock"></i>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Verify Password -->

                    <hr class="g-brd-gray-light-v4 g-my-25">

                    <div class="text-sm-right">
                    <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#!">Cancel</a>
                    <a class="btn u-btn-primary rounded-0 g-py-12 g-px-25" href="#!">Save Changes</a>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Profile Content -->
      </div>
    </div>
    
    <div id="viewReferal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; width:70%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-12">
                    <img class="view_referal_photo rounded-circle g-mb-20" ng-if="selectedReferal.photo==null" src="/main-assets/assets/img-temp/125x125/img1.jpg" alt="Image Description">
                    <img class="view_referal_photo rounded-circle g-mb-20" ng-if="selectedReferal.photo!=null" src="/referal_photos/@{{selectedReferal.photo}}" alt="@{{refers.photo}}">
                </div>
                <div class="col-lg-9 col-sm-12">
                    <ul class="list-unstyled g-mb-30">
                        <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-5">
                            <div class="g-pr-10">
                            <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Name:</strong>
                            <span class="align-top">@{{selectedReferal.name}}</span>
                            </div>
                            <span>
                                <i class="fa fa-edit g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                            </span>
                        </li>
                        {{-- <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-5">
                            <div class="g-pr-10">
                            <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Email address:</strong>
                            <span class="align-top">@{{selectedReferal.email}}</span>
                            </div>
                            <span>
                                <i class="fa fa-edit g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                            </span>
                        </li> --}}
                        {{-- <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-5">
                            <div class="g-pr-10">
                            <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Phone number:</strong>
                            <span class="align-top">@{{selectedReferal.phone}}</span>
                            </div>
                            <span>
                                <i class="fa fa-edit g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                            </span>
                        </li> --}}
                         <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-5">
                            <div class="g-pr-10">
                            <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Visa Type:</strong>
                            <span class="align-top">@{{selectedReferal.visa_type}}</span>
                            </div>
                            <span>
                                <i class="fa fa-edit g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                            </span>
                        </li>
                        <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-5">
                            <div class="g-pr-10">
                            <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Refered To:</strong>
                            <span class="align-top">@{{selectedReferal.consultancy_name!=null?selectedReferal.consultancy_name:''}}</span>
                            </div>
                            <span>
                                <i class="fa fa-edit g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                            </span>
                        </li>
                    </ul>
                    
                </div>
            </div>
       </div>
        
    </div>

    <div id="editReferal" class="text-left g-bg-white g-pa-10" style="display: none; width:70%; max-height:95%; overflow-y:auto">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container" style="margin-top:2rem">
            <form class="g-brd-around g-brd-gray-light-v4 g-pa-10 g-mb-10">
                
                    <!-- Text Input -->
                <div class="form-group row g-mb-15">
                    <label class="col-sm-3 col-form-label g-mb-10" for="hf-email">Full Name</label>
                
                    <div class="col-sm-9">
                        <input  class="form-control u-form-control rounded-0" ng-model="selectedReferal.name" type="text" placeholder="Enter full name of a person">
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    
                    </div>
                    
                </div>
                <!-- End Text Input -->
                <!-- Text Input -->
                <div class="form-group row g-mb-15">
                    <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Email Address</label>
                
                    <div class="col-sm-9">
                        <input  class="form-control u-form-control rounded-0" ng-model="selectedReferal.email" name="email" type="email" placeholder="Enter email address">
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    </div>
                    
                </div>

                    <!-- End Text Input -->
                <div class="form-group row g-mb-15">
                    <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Contact Number</label>
                
                    <div class="col-sm-9">
                        <input class="form-control u-form-control rounded-0" ng-model="selectedReferal.phone" name="phone" type="text" placeholder="Enter contact number">
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    </div>
                    
                </div>
                
                
                <div class="form-group row g-mb-15">
                    <label class="col-sm-3 col-form-label g-mb-10"  for="hf-email">Photo</label>
                
                    <div class="col-sm-9">
                    <img class="view_referal_photo rounded-circle g-mb-20" ng-if="selectedReferal.photo!=null" src="/referal_photos/@{{selectedReferal.photo}}" alt="Photo" id="editReferalPhoto">
                            
                            <div class="input-group-append">
                        
                                <input type="file" name="photo" id="editReferalPhotoUploader" fileread="selectedReferal.photo" ng-model="selectedReferal.photo" >
                            </div>
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    </div>
                    
                </div>
               
                <div class="form-group row g-mb-20">
                    <label class="col-sm-3 col-form-label g-mb-10">Refer To  </label>
                    <div class="col-sm-9">
                        <select class="custom-select mb-3" name="visa_type" ng-model="selectedReferal.category_id">
                               
                        <option ng-repeat="c in categories" value="@{{c.id}}">@{{c.title}}</option>
                              
                        </select>
                    </div>
                </div>
                <div class="form-group row g-mb-15" ng-if="selectedReferal.category_id!=undefined">
                    <label class="col-sm-3 col-form-label g-mb-10">Refer To @{{selectedReferal.consultancy}}</label>
                
                    <div class="col-sm-9">
                        {{-- <select class="custom-select mb-3" name="consultancy" ng-model="selectedReferal.consultancy" id="inlineFormCustomSelectPref">
                            <option selected="">Select Your Consultancy..</option>
                            @foreach($categories as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                            </select> --}}
                            <div ng-repeat="p in partners | filter: {category_id:selectedReferal.category_id}">
                                <label class="u-check" title="@{{p.name}}">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" value="@{{p.id}}" ng-model="selectedReferal.np" name="partner2" type="radio" @{{$index==0?'checked=""':''}} >
                                    <span class="btn btn-md btn-block g-font-size-13 u-btn-outline-lightgray g-color-white--checked g-bg-primary--checked rounded-0">@{{p.name.length>16?p.name.substr(0,14)+'..':p.name}}
                                        <br>  
                                          
                                        <div ng-if="p.show_rate=='show'">
                                        @{{p.commission_rate!=null?'Rs. '+p.commission_rate : '-'}}
                                        </div>
                                    </span>
                                    
                            
                                </label>
                            </div>
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    </div>
                    
                </div>
                <div class="form-group row g-mb-15" ng-if="selectedReferal.np!=undefined && selectedReferal.np!=''">
                <label class="col-sm-3 col-form-label g-mb-10">Refer To</label>
                
                    <div class="col-sm-9">
                        {{-- <select class="custom-select mb-3" name="consultancy" ng-model="selectedReferal.consultancy" id="inlineFormCustomSelectPref">
                            <option selected="">Select Your Consultancy..</option>
                            @foreach($categories as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                            </select> --}}
                            <div ng-repeat="p in branches | filter: {partner_id:selectedReferal.np}">
                                <label class="u-check" title="@{{p.name}}">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" value="@{{p.id}}" ng-model="selectedReferal.consultancy" name="consultancy" type="radio" @{{$index==0?'checked=""':''}} >
                                    <span class="btn btn-md btn-block g-font-size-13 u-btn-outline-lightgray g-color-white--checked g-bg-primary--checked rounded-0">@{{p.name.length>16?p.name.substr(0,14)+'..':p.name}}
                                        <br>  
                                          
                                        <div ng-if="p.show_rate=='show'">
                                        @{{p.commission_rate!=null?'Rs. '+p.commission_rate : '-'}}
                                        </div>
                                    </span>
                                    
                            
                                </label>
                            </div>
                        {{-- <small class="form-text text-muted g-font-size-default g-mt-10">full name of person.</small> --}}
                    </div>
                    
                </div>
                <button type="button" ng-click="referalSaveEdit()" class="btn btn-warning" style="color:white" id="saveReferalEdit">Save changes <i class="fa fa-check-circle g-mr-5"></i></button>
                <button tyle="button" ng-click="referalSubmitEdit()" class="btn btn-success" id="submitReferalEdit">Refer Now <i class="fa fa-check-circle g-mr-5"></i></button>
                
            </form>
       </div>
        
    </div>

    <div id="deleteReferal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="text-center">
            <h4><strong>Delete Confirmation</strong></h4>
                <p>Are you sure you want to delete this referal?</p>
                <button class="btn btn-danger" id="deleteReferalBtn" ng-click="deleteReferal()">Delete <i class="fa fa-check-circle g-mr-5"></i></button>
                <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Cancel</button>
            </div>
       </div>
        
    </div>

    <div id="bankDetailsConfirm" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="text-center">
            <h4><strong>You have earned a commission!!!</strong></h4>
                <p>You have earned commission but your bank details are not provided. Please set your bank details in your <a href="/user/editprofile">Edit Profile section</a> so that we can deposit it in your account. </p>
                <a class="btn btn-success" href="/user/editprofile" >Set Now <i class="fa fa-check-circle g-mr-5"></i></a>
                <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Later</button>
            </div>
       </div>
        
    </div>

    <div id="errorBox" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        
        <div class="container">
            <div class="text-center">
            <h4><strong>Something went wrong.</strong></h4>
            <p class="description" style="color:red;" id="errorMessage"></p>
            <p class="description" style="color:red;" id="errorDetails"></p>
                <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
    <div id="successModal" class="text-center g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        <div class="icon-box">
            <i class="fa fa-check" style="font-size:57px"></i>
        </div>
        <div class="container">
            <div class="text-center">
            <h4><strong id="successMessage">Success!!!</strong></h4>
               <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
        
    </div>
</section>

@stop

@section('js')
    <script src="/profilejs/profile.js"></script>

    @if(isset($bankDetailsNotSet) && $bankDetailsNotSet)
        <script>
            $(document).ready(function(){
                var modal = new Custombox.modal({
            content: {
              effect: 'fadein',
              target: '#bankDetailsConfirm'
            }
        });
          
        // Open
        modal.open();
            });
        </script>
    @endif
    <script>
        $(document).ready(function(){
          $('#uploadProfilePictureTrigger').click(function(){
            $('#uploadProfilePicture').trigger('click');
         
            });
            function readIMG(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#editReferalPhoto').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#editReferalPhotoUploader").change(function(){
                readIMG(this);
            });
        });
       
      </script>
@stop