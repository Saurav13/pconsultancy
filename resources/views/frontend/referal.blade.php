@extends('layouts.app')

@section('body')
<style>
    .referal_image{
        height: 12rem;
        width: 12rem;
        border-radius: 50%;
    }
    .referedBy_image{
        height: 6rem;
        width: 6rem;
        border-radius: 50%;
        margin-right:1rem
    }
    .referal_application{
      border:2px solid #21a79b;
      border-radius:20px;
      padding:2rem;
    }
    .pull-down{
            position: absolute;
            bottom: -16px;
            left: 0;
            right: 0;
        }
</style>
<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url(../../assets/img/banner-images/referalbanner.jpg);">
    <div class="container g-bg-cover__inner">
      <header class="g-mb-20">
        <h2 class="h1 g-font-weight-300 text-uppercase g-color-white">{{$referal->name}}
         
        </h2>
      </header>
  
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-7">
          <a class="u-link-v5 g-color-white g-color-primary--hover" href="/consultancy/cprofile">Dashboard</a>
          <i class="fa fa-angle-right g-ml-7"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>{{$referal->name}}</span>
        </li>
      </ul>
    </div>
  </section>
  <section class="container g-pt-30 g-pb-40" ng-cloak ng-app="referalapp" ng-controller="ReferalController" ng-init="referalInit({{$referal}})">
    <div class="row referal_application">
        <!-- Figure Image -->
        <div class="col-md-8 offset-md-2 col-lg-3 offset-lg-0 g-mb-30">
            <img class="referal_image" ng-if="referal.photo==null" src="/main-assets/assets/img-temp/400x450/img2.jpg" alt="Image Description">
            <img class="referal_image" ng-if="referal.photo!=null" src="/referal_photos/@{{referal.photo}}" alt="Image Description">
          <br><br>
          <p><strong>Status: </strong>&nbsp;<span class="u-label g-rounded-20 g-px-15 g-mr-10 g-mb-15 @{{getStatusColor(referal.status)}}">@{{referal.status}}</span> </p>
          <p style="margin-bottom:2px;"> <button class="btn btn-warning form-control" style="color:white" ng-show="referal.status=='seen' || referal.status=='rejected' " ng-click="toggleStatusConfirm('in-progress')"> <i class="fa fa-check-circle g-mr-5"></i> Mark as 'In-Progress' </button></p>
          <p style="margin-bottom:2px;"> <button class="btn btn-success form-control" style="color:white" ng-show="referal.status=='seen' || referal.status=='in-progress' || referal.status=='rejected' " ng-click="toggleStatusConfirm('success')"> <i class="fa fa-check-circle g-mr-5"></i> Mark as 'Success' </button></p>
          
          <p> <button class="btn btn-danger form-control" ng-show="referal.status=='seen' || referal.status=='in-progress' " ng-click="toggleStatusConfirm('rejected')" style="color:white"><i class="fa fa-times-circle g-mr-5"></i>Mark as 'Rejected'</button></p>
          <p> <button class="btn btn-success form-control pull-down g-bg-teal" ng-show="referal.status=='success' " ng-click="payCommission()" style="color:white"><i class="fa fa-check-circle g-mr-5"></i>Pay Commission</button></p>
          <p> <button class="btn btn-success form-control pull-down" style="color:white;" ng-show="referal.status=='paid'" ><i class="fa fa-check-circle g-mr-5"></i>Payment Successful</button></p>
          <p> <button class="btn btn-danger form-control pull-down" style="color:white;" ng-show="referal.paid_status=='rebound'" ng-click="payCommission()"><i class="fa fa-check-circle g-mr-5"></i>Payment Unsuccessful</button></p>
          <p> <button class="btn btn-warning form-control pull-down" style="color:white;" ng-show="referal.paid_status=='unverified'" ng-click="payCommission()"><i class="fa fa-check-circle g-mr-5"></i>Payment Under Verification</button></p>
            
        </div>
        <!-- End Figure Image -->

        <!-- Figure Body -->
        <div class="col-lg-8">
          <div class="d-flex justify-content-between g-mb-10">
            <div class="g-mb-20">
              <h4>Referral Application</h4>
              <h5 class="h5 g-mb-5">Name: @{{referal.name}}</h5>
            </div>
          </div>
          <hr class="g-brd-gray-light-v4 g-my-15">
                    
          <div class="g-py-10">
          <!-- Figure Contacts -->
          <ul class="list-unstyled g-color-gray-dark-v5 g-font-size-13 g-mb-0">
              <li class="g-mb-10">
              <i class="g-pos-rel g-mt-1 mr-2 icon-communication-062 u-line-icon-pro"></i>
              <span>Email: </span><a href="#!">@{{referal.email}}</a>
              </li>
              <li>
              <i class="g-pos-rel g-top-1 mr-2 icon-electronics-005 u-line-icon-pro"></i>
              <span>Contact: </span> <em class="g-font-style-normal">@{{referal.phone}}</em>
              </li>
          </ul>
          <!-- End Figure Contacts -->
          </div>
  
          <hr class="g-brd-gray-light-v4 g-my-15">

          <!-- Figure Info -->
          <div class="g-mb-50">
            <p><strong>Visa Type:</strong></p>
           <p>@{{referal.visa_type}}</p>
          </div>
          <!-- End Info -->
          <hr class="g-brd-gray-light-v4 g-my-15">
          <h5>Refered by:</h5>
            <div class="d-flex justify-content-start">
                <img class="referedBy_image" ng-if="referal.user.photo==null" src="/main-assets/assets/img-temp/100x100/img11.jpg" alt="Image Description">
                <img class="referedBy_image" ng-if="referal.user.photo!=null" src="/profile_images/@{{referal.user.photo}}" alt="Image Description">
              <div class="d-block">
                    <!-- Figure Info -->
                    <div class="g-mb-5">
                        <br>
                    <h3 class="h6 g-font-weight-600 mb-1">
                        <a class="g-color-black g-color-primary--hover" href="#!">@{{referal.user.name}}</a>
                    </h3>
                    <em class="g-color-gray-dark-v4 g-font-style-normal g-font-size-12">Date: </em>
                    <a class="g-font-size-12" href="#!">@{{refers.submitted_at.replace(' ','T') | date: 'MMM d, y h:mm a'}} </a>
                    </div>
                    <!-- End Figure Info -->
                </div>
            </div>
        </div>
        <!-- End Figure Body -->
    </div>
    <div id="in-progressReferal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
      <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
          <div class="text-center">
          <h4><strong>Mark as In-Progress?</strong></h4>
              <p>The person who refered this will notice the status changed.</p>
              
              <button class="btn btn-warning" style="color:white;" id="in-progressReferalBtn" ng-click="toggleReferalStatus('in-progress')">Confirm <i class="fa fa-check-circle g-mr-5"></i></button>
              <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Cancel</button>
          </div>
      </div>
      
    </div>
    <div id="rejectedReferal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
      <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
          <div class="text-center">
          <h4><strong>Mark as Rejected?</strong></h4>
              <p>The person who refered this will notice the status changed.</p>
              
              <button class="btn btn-danger" id="rejectedReferalBtn" ng-click="toggleReferalStatus('rejected')">Confirm <i class="fa fa-check-circle g-mr-5"></i></button>
              <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Cancel</button>
          </div>
      </div>
    </div>
    <div id="successReferal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
      <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
          <div class="text-center">
          <h4><strong>Mark as Success?</strong></h4>
              <p>The person who refered this will notice the status changed.</p>
              <p>Once the referal is marked as success, you have to pay the commission.</p>
              
              
              <button class="btn btn-success" id="successReferalBtn" ng-click="toggleReferalStatus('success')">Confirm <i class="fa fa-check-circle g-mr-5"></i></button>
              <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Cancel</button>
          </div>
      </div>
    
    </div>
    <div id="payCommission" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
      <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
          <div class="text-center" style="font-weight:600">
            <h4><strong>Payment Info</strong></h4>
              <p>Please deposit your commission amount in our account in Himalayan Bank Pvt Ltd.</p>
              <p>Account Name:  Get Linked</p>
              <p>Account Number:  99089971526399</p>
              
              <p>Once you have deposited the amount. Upload the image of receipt.</p>
              <br>
              <p ng-show="referal.paid_status=='unverified'" style="color:brown;">Your Receipt is awaiting verification.</p>
              <p ng-show="referal.paid_status=='rebound'" style="color:red;">Verification Failed. Please input valid receipt image.</p>
              
              <form ng-submit="submitReceipt()">  
                <input type="file"  fileread="receipt_image" ng-model="receipt_image" />
                <button class="btn btn-success" id="referalSubmitBtn" type="submit">Submit <i class="fa fa-check-circle g-mr-5"></i></button>
              </form>
              <p>Payment process will be complete after we verify your receipt.</p>
                
          </div>
          
      </div>
      
    </div>

    <br><br><br><br>
    <br><br><br><br>
    <br><br><br><br>
    
    <div id="errorBox" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
      <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
          <div class="text-center">
          <h4><strong>Something went wrong.</strong></h4>
          <p class="description" style="color:red;" id="errorMessage"></p>
          <p class="description" style="color:red;" id="errorDetails"></p>
              <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
          </div>
      </div>
        
    </div>
    <div id="successModal" class="text-center g-bg-white g-overflow-y-auto g-pa-20" style="display: none; min-width:40%">
        <button type="button" class="close" style="cursor:pointer;" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>
        <div class="icon-box">
            <i class="fa fa-check" style="font-size:57px"></i>
        </div>
        <div class="container">
            <div class="text-center">
            <h4><strong id="successMessage">Success!!!</strong></h4>
              <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
      </div>
        
    </div>
  </section>
@endsection

@section('js')
<script src="/consultancyjs/referal.js"></script>

@endsection