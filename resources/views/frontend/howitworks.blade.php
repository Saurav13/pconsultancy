<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Get Linked | How it Works?</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/logo_ico.png">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans%7CMerriweather%7COpen+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-line-pro/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/animate.css">
  <link rel="stylesheet" href="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/main-assets/assets/css/unify-globals.css">
    <!-- CSS Template -->
    <link rel="stylesheet" href="/assets/css/styles.op-consulting.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/layers.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/navigation.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/main-assets/assets/css/custom.css">
  </head>

  <body id="home-section">
    <main>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance u-header--change-logo"
              data-header-fix-moment="0">
        <div class="u-header__section g-bg-black-opacity-0_2 g-transition-0_3 g-py-7 g-py-10--md"
             data-header-fix-moment-exclude="g-bg-black-opacity-0_5 g-py-10--md"
             data-header-fix-moment-classes="u-shadow-v19 g-bg-white g-py-10--md">
          <nav class="navbar navbar-expand-lg g-py-0">
            <div class="container g-pos-rel">
              <!-- Logo -->
              <a href="/" style="padding-top:1.2rem;" class="js-go-to navbar-brand u-header__logo"
                 data-type="static">
                <img class="u-header__logo-img u-header__logo-img--main g-width-90" src="/assets/img/logo.png" style="height:2rem;" alt="Image description">
                <img class="u-header__logo-img g-width-90" src="/assets/img/logo-dark.png" alt="Image description" style="height:2rem;">
              </a>
              <!-- End Logo -->

              <!-- Navigation -->
              <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar">
                <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-letter-spacing-1 g-font-size-12 g-pt-20 g-pt-0--lg ml-auto">
                  <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg">
                    <a href="/" class="nav-link p-0">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                    <a href="#about-section" class="nav-link p-0">How it works</a>
                  </li>
                  
                  <li class="nav-item g-ml-15--lg">
                      <a href="/become-a-partner" class="nav-link p-0">Become a partner</a>
                    </li>
                  {{-- <li class="nav-item g-ml-15--lg">
                    <a href="/main-assets/index.html" class="nav-link p-0">Learn more</a>
                  </li> --}}
                  @if(!Auth::guard('consultancy_user')->check() && !Auth::check())
                      <li class="nav-item g-ml-15--lg"><a class="nav-link p-0" href="{{ route('login') }}">Sign In</a></li>
                      
                  @elseif(Auth::check())
                      <li class="nav-item g-ml-15--lg dropdown">
                          <a href="#" class="dropdown-toggle nav-link p-0" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <ul class="dropdown-menu list-unstyled g-text-transform-none g-brd-top g-brd-top-2 g-min-width-200 g-mt-7 g-mt-7--lg--scrolling animated">
                            <li class="dropdown-item">
                              <a class="nav-link g-px-0" style="color:#555555" href="/profile">My Profile</a>
                            </li> 
                            <li class="dropdown-item">
                                  <a class="nav-link g-px-0" style="color:#555555"  href="{{ route('logout') }}"
                                      onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                      Logout
                                  </a>

                                  <form id="logout-form" action="{{route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                  </form>
                              </li>
                          </ul>
                      </li>
                  @endif
                  @if(Auth::check() || Auth::guard('consultancy_user')->check() )

                  <li class="nav-item g-ml-15--lg">
                      <a href="/notifications"><i class="fa fa-bell" style="font-size: 15px;"></i></a>
                      @if(Auth::check())
                        @if(Auth::user()->notifications->count()>0);
                          <span style="    position: absolute; top: -24px; right: 7px;color: red; font-size: 37px;">.</span>
                        @endif
                      @endif
                      @if(Auth::guard('consultancy_user')->check())
                        @if(Auth::guard('consultancy_user')->user()->branch->notifications->count()>0);
                          <span style="    position: absolute; top: -24px; right: 7px;color: red; font-size: 37px;">.</span>
                        @endif
                      @endif
                  </li>
                  @endif
                </ul>
                @if(!Auth::guard('consultancy_user')->check() && !Auth::check())
                <div class="d-inline-block g-pos-rel g-valign-middle g-ml-10 g-pl-0--lg g-mt-10">
                    <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="{{ route('consultancy_login') }}" ><b>Partner Area</b></a>
                  </div>
                  @elseif(Auth::guard('consultancy_user')->check())
                  <div class="nav-item g-ml-15--lg dropdown">
                      <a id="nav-link--cousuntancydrop" class="dropdown-toggle g-font-size-12 text-uppercase nav-link g-py-7 g-px-0" data-toggle="dropdown" href="#!" aria-haspopup="true" aria-expanded="false" >
                          {{Auth::guard('consultancy_user')->user()->name }} <span class="caret"></span>
                      </a>

                      <ul class="dropdown-menu list-unstyled g-text-transform-none g-brd-top g-brd-top-2 g-min-width-200 g-mt-7 g-mt-7--lg--scrolling animated" >
                        <li class="dropdown-item">
                          <a class="nav-link g-px-0" style="color:#555555" href="/consultancy/cprofile">Dashboard</a>
                        </li>  
                        <li class="dropdown-item">
                              <a class="nav-link g-px-0" style="color:#555555" href="{{ route('consultancy_logout') }}"
                                  onclick="event.preventDefault();
                                          document.getElementById('logout-form-consultancy').submit();">
                                  Logout
                              </a>

                              <form id="logout-form-consultancy" action="{{ route('consultancy_logout')}}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                      </ul>
                    </div>
                  
                  @endif
                </div>
              <!-- End Navigation -->

              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
            </div>
          </nav>
        </div>
      </header>
      <!-- End Header -->

      <div id="about-section" class="g-overflow-hidden">
          <div class="container g-pt-100">
            <!-- Heading -->
              <div class="g-max-width-550 text-center mx-auto g-mb-40">
                  <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-13 g-letter-spacing-2 mb-4">How it Works?</h1>
                  <p class="g-font-size-16">In a simple 3 steps.</p>
              </div>

             
              <div class="row">
                  <div class="col-lg-12 g-mb-30">
                      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                      
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                      
                              <img class="d-block" style="height:15rem; margin:0 41%" src="/assets/img/1.png" alt="First slide">
                              <div class="text-center d-md-block">
                                <h5 class="font-weight-light" style="color: #F59E46;">Sign Up</h5>
                              </div>]
                            </div>
                            <div class="carousel-item">
                              <img class="d-block" style="height:15rem; margin:0 41%" src="/assets/img/3.png" alt="Second slide">
                              <div class="text-center d-md-block">
                                <h5 class="font-weight-light" style="color: #4FA5BC;">Refer</h5>
                              </div>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block" style="height:15rem; margin:0 41%" src="/assets/img/2.png" alt="Third slide">
                              <div class="text-center d-md-block">
                                <h5 class="font-weight-light">Earn</h5>
                              </div>
                            </div>
          
                          </div>
                          
                        </div>
                  </div>
                  <div class="col-lg-12 g-mb-30">
                      <!-- Icon Blocks -->
                      <div class="u-shadow-v19 g-bg-white g-pa-30">
                      <div class="media g-mb-15">
                          <div class="d-flex mr-4">
                          <span class="g-color-gray-dark-v4 g-font-size-26">
                              <img src="/assets/img/1.png" width="50px" height="50px">
                          </span>
                          </div>
                          <div class="media-body">
                          <h3 class="h5 g-color-black mb-20">Sign Up</h3>
                          <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                          <p class="g-color-gray-dark-v4 g-mb-0">Signing up with Get Linked is easy. Just put you basic information or Sign up using Facebook or Gmail. Verify your mobile number and Voila !! you just “Got Linked”. You can now start making your own income.
                              </p>
                          </div>
                      </div>
                      </div>
                      <!-- End Icon Blocks -->
                  </div>
                  
                  <div class="col-lg-12 g-mb-30">
                      <!-- Icon Blocks -->
                      <div class="u-shadow-v19 g-bg-white g-pa-30">
                      <div class="media g-mb-15">
                          <div class="d-flex mr-4">
                          <span class="g-color-gray-dark-v4 g-font-size-26">
                            <img src="/assets/img/3.png" width="50px" height="50px">
                          </span>
                          </div>
                          <div class="media-body">
                          <h3 class="h5 g-color-black mb-20">Refer</h3>
                          <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                          <p class="g-color-gray-dark-v4 g-mb-0">This is the most vital part of the process. Everyone has a strong network of family and friends. You can now use this to earn spare cash. You ask your family and friends, if anyone is interested to study abroad and would like a consultation.  If yes, you have secured your lead (potential customer) and you Get  him/her Linked by taking her basic information and selecting one of our highly reputed Partnered Consultancies. The information will then be sent through the Get Linked App to the selected Consultancy’s system as a lead. Easy as that.</p>
                          </div>
                      </div>
                      </div>
                  </div>
                  <div class="col-lg-12 g-mb-30">
                      <!-- Icon Blocks -->
                      <div class="u-shadow-v19 g-bg-white g-pa-30">
                      <div class="media g-mb-15">
                          <div class="d-flex mr-4">
                          <span>
                            <img src="/assets/img/2.png" width="80px" height="80px" style="position : absolute; left:25px">
                          </span>
                          </div>
                          <div class="media-body ml-5">
                          <h3 class="h5 g-color-black mb-20">Earn</h3>
                          <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                          <p class="g-color-gray-dark-v4 g-mb-0">If the referral decides to use the service of the consultancy, you Earn the commission directly deposited to your bank account. Then you move on to find another referral.</p>
                          </div>
                      </div>
                      </div>
                      <!-- End Icon Blocks -->
                  </div>
                      <!-- End Icon Blocks -->
              </div>
          </div>
        </div>

      <!-- Testimonials -->
      <div class="container g-pt-70 g-pb-100">
        <div class="g-max-width-550 text-center mx-auto g-mb-40">
            <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-13 g-letter-spacing-2 mb-4">Success Stories</h1>
           
        </div>
        <div id="carouselCus2" class="js-carousel u-carousel-indicators-v32 g-pos-stc g-width-400 text-center mx-auto g-mb-20"
         data-infinite="true"
         data-center-mode="true"
         data-slides-show="3"
         data-is-thumbs="true"
         data-nav-for="#carouselCus1">
         @foreach($testimonials as $t)
          <div class="js-slide d-flex text-center g-opacity-1 g-cursor-pointer g-px-13 g-py-50">
            @if($t->photo)
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-80 g-height-80 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="{{ route('optimize', ['landing_images',$t->photo,290,290]) }}" alt="Image Description">
            @else            
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-80 g-height-80 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="/assets/img-temp/100x100/img3.jpg" alt="Image Description">
            @endif
          </div>
          @endforeach
        </div>

        <div id="carouselCus1" class="js-carousel g-px-50 g-px-200--lg"
             data-infinite="true"
             data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-45 g-height-45 g-font-size-30 g-color-main-light-v1 g-color-primary--hover"
             data-arrow-left-classes="fa fa-angle-left g-left-0 g-left-40--lg g-mt-minus-50"
             data-arrow-right-classes="fa fa-angle-right g-right-0 g-right-40--lg g-mt-minus-50"
             data-nav-for="#carouselCus2">
        @foreach($testimonials as $t)
          <div class="js-slide text-center">
          <blockquote class="g-color-text g-font-size-default g-font-size-18--md g-mb-60">{{$t->content}}</blockquote>
              <h4 class="g-font-weight-700 g-font-size-15 g-mb-1">{{$t->name}}</h4>
              <span class="d-block g-color-text g-font-size-13">{{$t->profession}}</span>
          </div>
        @endforeach
        </div>
        <!-- End Carousel -->
      </div>
      <!-- End Testimonials -->
  <!-- Contact -->
  <footer id="contact-section" class="g-pos-rel">
    <!-- Content -->
    {{-- <div class="g-bg-size-cover g-bg-img-hero u-bg-overlay g-bg-black-opacity-0_7--after g-pt-120 g-pb-70" style="background-image: url(/assets/img/banner-images/contact.jpg);">
      <div class="container u-bg-overlay__inner">
        <div class="row align-items-center">
          <div class="col-md-4 g-mb-50">
            <h3 class="h4 g-color-white mb-4">Contact Info</h3>

            <!-- Icon Block -->
            <div class="media align-items-center mb-4">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-hotel-restaurant-235 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">5B Streat, City 50987 New Town US</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="media align-items-center mb-4">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-communication-033 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">+32 (0) 333 444 555</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="media align-items-center g-mb-60">
              <div class="d-flex">
                <span class="u-icon-v1 u-icon-size--sm g-color-white mr-2">
                  <i class="icon-communication-062 u-line-icon-pro"></i>
                </span>
              </div>
              <div class="media-body">
                <p class="g-color-white-opacity-0_6 mb-0">htmlstream@support.com</p>
              </div>
            </div>
            <!-- End Icon Block -->

            <!-- Social Icons -->
            <h3 class="h4 g-color-white">Social Networks</h3>

            <ul class="list-inline mb-0">
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://www.facebook.com/htmlstream">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://twitter.com/htmlstream">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://github.com/htmlstream">
                  <i class="fa fa-github"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-2">
                <a class="u-icon-v1 u-icon-size--sm g-color-white g-bg-white-opacity-0_1 g-bg-primary--hover g-font-size-13 rounded-circle" href="https://dribbble.com/htmlstream">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
            <!-- End Social Icons -->
          </div>

          <div class="col-md-8 g-mb-50">
            <div class="g-brd-around g-brd-white-opacity-0_6 g-px-45 g-py-60">
              <div class="row">
                <div class="col-md-4 g-mb-30">
                  <h2 class="h1 g-color-white">Contact Us</h2>
                </div>

                <div class="col-md-8 g-mb-30">
                  <!-- Contact Form -->
                  <form method="POST" action="/contactus" >
                    {{csrf_field()}}
                    <div class="mb-3">
                      <input required name="name" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 rounded-0 g-py-13 g-px-0 mb-2" type="text" placeholder="Name">
                    </div>

                    <div class="mb-3">
                      <input required name="email" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 rounded-0 g-py-13 g-px-0 mb-2" type="email" placeholder="Email">
                    </div>

                    <div class="mb-4">
                      <textarea required name="message" class="form-control g-brd-none g-brd-bottom g-brd-white g-brd-primary--focus g-color-white g-bg-transparent g-placeholder-gray-light-v5 g-resize-none rounded-0 g-py-13 g-px-0 mb-5" rows="5" placeholder="Message"></textarea>
                    </div>

                    <button class="btn u-btn-primary g-bg-secondary g-color-primary g-color-white--hover g-bg-primary--hover g-font-weight-600 g-font-size-12 g-rounded-30 g-py-15 g-px-35" type="submit" role="button">Send Message</button>
                  </form>
                  
                  <!-- End Contact Form -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
    <!-- End Content -->
    {{-- @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="position: fixed;right: 2rem; bottom: 2px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <h4 class="h5">
                        <i class="fa fa-check-circle-o"></i>
                        Thank you for your message!
                      </h4>
                    {{Session::get('success')}}
                  </div>
    @endif
    @if (count($errors)>0 )
    <div class="alert alert-danger alert-dismissible fade show" role="alert"  style="position: fixed;right: 2rem; bottom: 2px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="h5">
          <i class="fa fa-minus-circle"></i>
          Oh snap!
        </h4>
            @foreach ($errors->all() as $error)
              <p>{{ $error }}</p>
            @endforeach
      
    </div>
      @endif --}}
    <!-- Go To Top -->
    <a class="js-go-to text-center g-color-main g-color-primary--hover g-left-50x g-ml-minus-100" href="#!"
       data-type="absolute"
       data-position='{
         "bottom": 65
       }'
       data-offset-top="400"
       data-compensation="#js-header"
       data-show-effect="fadeInUp">
      <svg version="1.1" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="208px" height="50px">
        <path fill-rule="evenodd" clip-rule="evenodd" fill="#fff" d="M111.042,0h-0.085C81.962,0.042,50.96,42.999,6,42.999c-6,0-6,0-6,0v1h214v-1v-0.015C169.917,42.349,139.492,0.042,111.042,0z"/>
      </svg>
      <i class="g-font-size-20 g-pos-abs g-bottom-20 g-left-50x g-ml-2 fa fa-angle-double-up"></i>
    </a>
    <!-- End Go To Top -->

    <!-- Copyright -->
    <div class="container text-center g-py-30">
        <p class="g-font-size-13 mb-0">&#169; 2019 Get Linked. Powered by <strong><a href="http://incubeweb.com/" target="_blank">Incube</a></strong></p>
    </div>
    <!-- End Copyright -->
  </footer>
  <!-- End Contact -->
</main>

<!-- JS Global Compulsory -->
<script src="/main-assets/assets/vendor/jquery/jquery.min.js"></script>
<script src="/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/main-assets/assets/vendor/popper.min.js"></script>
<script src="/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/main-assets/assets/vendor/appear.js"></script>
<script src="/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/main-assets/assets/vendor/fancybox/jquery.fancybox.js"></script>

<!-- JS Unify -->

<script src="/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

<script src="/main-assets/assets/js/hs.core.js"></script>
<script src="/main-assets/assets/js/components/hs.header.js"></script>
<script src="/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
<script src="/main-assets/assets/js/components/hs.scroll-nav.js"></script>
<script src="/main-assets/assets/js/components/hs.counter.js"></script>
<script src="/main-assets/assets/js/components/hs.carousel.js"></script>
<script src="/main-assets/assets/js/components/hs.cubeportfolio.js"></script>
<script src="/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/main-assets/assets/js/components/hs.go-to.js"></script>
<script src="/main-assets/assets/js/components/hs.scrollbar.js"></script>
<script src="/main-assets/assets/js/components/hs.select.js"></script>

<!-- JS Revolution Slider -->
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script src="/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js"></script>

<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

<!-- JS Customization -->
<script src="/main-assets/assets/js/custom.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of carousel
    $.HSCore.components.HSCarousel.init('.js-carousel');

    // initialization of header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // initialization of go to section
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of counters
    var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');

    // initialization of popups
    $.HSCore.components.HSPopup.init('.js-fancybox');
  });

  $(window).on('load', function() {
    // initialization of HSScrollNav
    $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
      duration: 700
    });

    // initialization of cubeportfolio
    $.HSCore.components.HSCubeportfolio.init('.cbp');
  });

  var tpj = jQuery;

  var revapi1014;
  tpj(document).ready(function () {
    if (tpj("#rev_slider_1014_1").revolution == undefined) {
      revslider_showDoubleJqueryError("#rev_slider_1014_1");
    } else {
      revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
        sliderType: "standard",
        jsFileLocation: "revolution/js/",
        sliderLayout: "fullscreen",
        dottedOverlay: "none",
        delay: 9000,
        navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          mouseScrollReverse: "default",
          onHoverStop: "off",
          touch: {
            touchenabled: "on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          },
          arrows: {
            style: "hermes",
            enable: true,
            hide_onmobile: true,
            hide_under: 768,
            hide_onleave: false,
            tmp: '<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div><div class="tp-arr-titleholder">Pocket Consultancy</div></div>',
            left: {
              h_align: "left",
              v_align: "center",
              h_offset: 0,
              v_offset: 0
            },
            right: {
              h_align: "right",
              v_align: "center",
              h_offset: 0,
              v_offset: 0
            }
          }
        },
        responsiveLevels:[1240,1024,778,778],
        gridwidth:[1240,1024,778,480],
        gridheight:[600,500,400,300],
        lazyType: 'smart',
        scrolleffect: {
          fade: "on",
          grayscale: "on",
          on_slidebg: "on",
          on_parallax_layers: "on",
          direction: "top",
          multiplicator_layers: "1.4",
          tilt: "10",
          disable_on_mobile: "off",
        },
        parallax: {
          type: "scroll",
          origo: "slidercenter",
          speed: 400,
          levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
        },
        shadow: 0,
        spinner: "off",
        stopLoop: 'off',
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        fullScreenAutoWidth: "off",
        fullScreenAlignForce: "off",
        fullScreenOffsetContainer: "",
        fullScreenOffset: "0px",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: "off",
          nextSlideOnWindowFocus: "off",
          disableFocusListener: false,
        }
      });
    }

    RsTypewriterAddOn(tpj, revapi1014);
  });
</script>
</body>
</html>