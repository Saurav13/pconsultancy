<p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
    {{ $user->branch->full_name }},
</p>

<p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">
    You are receiving this email because we created a new consultancy account.<br><br><br>
    <strong>Your credentials:</strong><br><br>
    <strong>Email        :</strong> {{ $user->email }} <br>
    <strong>Password     :</strong> {{ $password }}<br>
    
                        
    <div style="text-align:center;margin:20px">
        <a href="{{ route('consultancy_login') }}" style="background-color:#23272b;color:#ffffff;display:inline-block;font-family:brandon-grotesque;text-transform: uppercase;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Login</a>
    </div>
</p>

<p style="font-size: 16px; font-weight: 600; line-height: 24px; color: #333333;">                              
    Thanks,<br>
    Get Linked Team
</p>